<?php if (!defined('PLX_ROOT')) exit;
$plxShow = plxShow::getInstance();
$plxPlugin = $plxShow->plxMotor->plxPlugins->aPlugins['plxMyShop'];
$plxPlugin->donneesModeles['plxPlugin'] = $plxPlugin;
$plxPlugin->validerCommande();
$panier = array('basPageStars','basPage', 'partout');
if($plxPlugin->plxMotor->mode == 'products') {
 $plxPlugin->modele('espacePublic/products');
 $panier = array('basPageStars', 'partout');
} elseif ('1' === $plxPlugin->aProds[$plxPlugin->default_lang][$plxPlugin->productNumber()]['pcat']) {
 $plxPlugin->modele('espacePublic/categorie');
} else {
 $plxPlugin->modele('espacePublic/produit');
}
if (in_array($plxPlugin->getParam('affPanier'), $panier)){
 $plxPlugin->modele('espacePublic/panier');
}