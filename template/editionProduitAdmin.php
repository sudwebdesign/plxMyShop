<?php if (!defined('PLX_ROOT')) exit;

/**
 * Edition du code source d'un produit
 * Est inclus par admin.php
 * @package PLX
 * @author David L, Thomas I.
 **/

$arts_shop_fields = array(
 'title_htmltag',
 'meta_description',
 'meta_keywords',
 'template',
 'image',
 'chapo',
 'pricettc',
 'pcat',
 'poidg',
 'name',
 'url',
 'active',
 'noaddcart',
 'iteminstock',
 'notice_noaddcart',
 'title_htmltag',
 'meta_description',
 'meta_keywords',
 'template'
);

# Hook Plugins plxMyShopAdminProductBegin
eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminProductBegin'));

# On édite le produit
if(!empty($_POST) AND isset($plxPlugin->aProds[$plxPlugin->dLang][$_POST['id']])) {//a verifier si default_lang est ok
 $plxPlugin->editProduct($_POST);
 header('Location: plugin.php?p='.$plxPlugin->plugName.'&amp;prod='.$_POST['id']);
 exit;
} elseif(!empty($_GET['prod'])) { # On affiche le contenu de la page
 $id = plxUtils::strCheck(plxUtils::nullbyteRemove($_GET['prod']));
 if(!isset($plxPlugin->aProds[$plxPlugin->dLang][ $id ])) {//a verifier si default_lang est ok
  plxMsg::Error($plxPlugin->getLang('L_PRODUCT_UNKNOWN_PAGE'));
  header('Location: plugin.php?p='.$plxPlugin->plugName);
  exit;
 }
 # On récupère le contenu
 foreach ($aLangs as $lang) {
  foreach ($arts_shop_fields as $shop_field){
   ${$shop_field}[$lang] = isset($plxPlugin->aProds[$lang][$id][$shop_field])?$plxPlugin->aProds[$lang][$id][$shop_field]:'';
  }
  $content[$lang] = trim($plxPlugin->getFileProduct($id,$lang));
 }
} else { # Sinon, on redirige
 header('Location: products.php');
 exit;
}
# On récupère les templates du produit
$files = plxGlob::getInstance(PLX_ROOT.$plxAdmin->aConf['racine_themes'].$plxAdmin->aConf['style']);
if ($array = $files->query('/^static(-[a-z0-9-_]+)?.php$/')) {
 foreach($array as $k=>$v)
  $aTemplates[$v] = $v;
}
$modProduit = ('1' !== $pcat[ $plxPlugin->dLang ] );
$plxPlugin->idProduit = $id;#Fix productImage() #tep
?>
<style>.title-tag,.inline-form.action-bar h2{display:none;}</style>
<h2 id="page-title" class="title-tag"><?php echo $title = $plxPlugin->getLang($modProduit ? 'L_PRODUCT_TITLE' : 'L_CAT_TITLE'); ?></h2>

<?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminProductTop'));# Hook Plugins plxMyShopAdminProductTop?>

<form action="plugin.php?p=<?php echo $plxPlugin->plugName; ?>" method="post" id="form_product">
 <?php echo plxToken::getTokenPostMethod() ?>
 <div id="tabContainer" data-storetabs="myShopAdmin-<?php echo plxUtils::title2filename($plxPlugin->plxMotor->racine) ?>">
   <?php plxUtils::printInput('prod', $_GET['prod'], 'hidden');?>
   <?php plxUtils::printInput('id', $id, 'hidden');?>
<?php if($plxPlugin->aLangs){#MyMultilingue ?>
   <div class="tabs">
    <ul class="col sml-12">
<?php
    foreach($aLangs as $lang){
     $datab = $titab = $mixab = '';
     if($lang==$_SESSION['default_lang']){# dataLang  : default lang  (site)
      $titab .= ($modProduit?$plxPlugin->getLang('L_PRODUCTS_WEIGHT').' ('.$plxPlugin->getLang('KG').') &amp; '.$plxPlugin->getLang('L_PRODUCTS_STOCK').PHP_EOL:'').L_CONFIG_BASE_DEFAULT_LANG;
      $datab .= ' data-default_lang="'.$lang.'"';
      $mixab .= ' &amp; ';
     }
     if($lang==$plxPlugin->dLang){# data-user_lang
      $titab .= $mixab.L_USER_LANG;
      $datab .= ' data-user_lang="'.$lang.'"';
     }
     echo '     <li id="tabHeader_'.$lang.'"'.($lang==$plxPlugin->dLang?' class="active"':'').' title="'.$titab.'"'.$datab.'>'.strtoupper($lang).'</li>'.PHP_EOL;
    }
?>
    </ul>
   </div>
<?php }#fi mml ?>
   <div class="tabscontent">
<!-- Content en multilingue -->
<?php
foreach($aLangs as $lang) {
 $lgu=($plxPlugin->aLangs AND (isset($_SESSION['default_lang']) AND $lang!=$_SESSION['default_lang']))?$lang.'/':'';#url (view) : old idea ($plxPlugin->dLang != $lang)
 $lng=($plxPlugin->aLangs)?'_'.$lang:'';#post vars

 $link = $plxAdmin->urlRewrite('?'.$lgu.'product'.intval($id).'/'.$url[$lang]);
?>
   <div class="tabpage<?php echo ($lang==$plxPlugin->dLang?' active':''); ?>" id="tabpage<?php echo $lng ?>">
    <div class="in-action-bar return-link plx<?php echo str_replace('.','-',@PLX_VERSION); echo $plxPlugin->aLangs?' multilingue':'';?> selector">
     <h2 class="page-title"><?php echo $title;?><i class="sml-hide">&nbsp;&laquo;<?php echo $name[$lang];?>&raquo;</i></h2>
     <a class="back" href="plugin.php?p=<?php echo $plxPlugin->plugName.($modProduit ? '' : '&amp;mod=cat');?>"><?php
      echo $plxPlugin->lang('L_'  . ($modProduit? 'PRODUCT': 'CAT') . '_BACK_TO_PAGE');
     ?></a><br />
     <input type="submit" value="<?php $plxPlugin->lang($modProduit?'L_PRODUCT_UPDATE':'L_CAT_UPDATE');?>"/>
<?php
      if($active[$lang]){
       $codeTexte = 'L_' . ($modProduit? 'PRODUCT': 'CAT') . '_VIEW_PAGE_ON_SITE';
       $texte = sprintf($plxPlugin->getLang($codeTexte), '<i class="myhide">'.$name[$lang].'</i>');
    ?>
       <br class="med-hide" /><a href="<?php echo $link ?>"><?php echo $texte ?><?php echo($lng?'&nbsp;'.$mmlFlagImgs[$lang]:''); ?></a>
<?php } ?>
    </div>
    <h2 class="no-margin text-right"><sup><i><?php $plxPlugin->lang($modProduit ? 'L_PRODUCT_NAME' : 'L_CAT_NAME')?></i></sup><br /><?php echo $name[$lang] ?></h2>
    <div class="grid">
     <div class="col sml-12 lrg-6">
      <p class="informationsShortcodeProduit"><?php $plxPlugin->lang('L_PRODUCTS_SHORTCODE'); ?>&nbsp;:<br/>
      <span class="code">[<?php echo $plxPlugin->shortcode.'&nbsp;'.$id;?>]</span></p>
     </div>
    </div>
    <div class="grid gridthumb">
     <div class="col sml-12 med-5 label-centered">
      <label for="id_image<?php echo $lng ?>"><?php $plxPlugin->lang('L_PRODUCTS_IMAGE_CHOICE'); echo $lng?'&nbsp;'.$mmlFlagImgs[$lang]:''; ?> <a title="<?php echo $plxPlugin->lang('L_THUMBNAIL_SELECTION') ?>" id="toggler_thumbnail<?php echo $lng ?>" href="javascript:void(0)" onclick="mediasManager.openPopup('id_image<?php echo $lng ?>', true, 'id_image<?php echo $lng ?>')" style="outline:none; text-decoration: none"> +</a></label>
      <?php plxUtils::printInput('image'.$lng,plxUtils::strCheck($image[$lang]),'text','255-255',false,'full-width','','onKeyUp="refreshImg(\'id_image_img'.$lng.'\')"'); ?>
     </div>
     <div class="col sml-12 med-7">
      <div class="image_img" id="id_image<?php echo $lng ?>_img">
       <?php $plxPlugin->productImage(); ?>
      </div>
     </div>
    </div>
  <!-- Fin du selecteur d'image natif de PluXml -->
<?php
if ($modProduit){
 $field = true;
 if($plxPlugin->aLangs){#MultiLingue
  $field = false;
  if($lang==$_SESSION['default_lang']){
   $field = true;
  }
 }
?>

   <?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminProduct'));# Hook Plugins plxMyShopAdminProduct ?>

     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_pricettc<?php echo $lng ?>"><?php $plxPlugin->lang('L_PRODUCTS_PRICE') ;?>&nbsp;<?php echo($lng?$mmlFlagImgs[$lang].'&nbsp;':''); ?>(<?php echo trim($plxPlugin->getParam('devise_'.$lang));?>)&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('pricettc'.$lng,plxUtils::strCheck($pricettc[$lang]),'text','0-255'); ?>
       <?php if($field) {plxUtils::printInput('pricettc4all','','checkbox'); echo '&nbsp;'.L_ALL.'?';} ?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_poidg<?php echo $lng ?>"><?php $plxPlugin->lang('L_PRODUCTS_WEIGHT')?>&nbsp;(<?php $plxPlugin->lang('KG')?>)&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('poidg'.$lng,plxUtils::strCheck($poidg[$lang]),'text','0-255',!$field); ?>
      </div>
      <div class="col sml-12 med-5 label-centered">
       <label for="id_iteminstock<?php echo $lng ?>"><?php $plxPlugin->lang('L_PRODUCTS_ITEM_INSTOCK'); ?>&nbsp;:&nbsp;<a class="hint"><span><?php $plxPlugin->lang('L_PRODUCTS_ITEM_INSTOCK_HINT') ;?><span></a></label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('iteminstock'.$lng,plxUtils::strCheck($iteminstock[$lang]),'text','0-255',!$field); ?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_noaddcart<?php echo $lng ?>"><?php $plxPlugin->lang('L_PRODUCTS_BASKET_BUTTON'); ?>&nbsp;:<?php echo '<img id="cartImg'.$lng.'" class="noaddcartImg" src="'.PLX_PLUGINS.$plxPlugin->plugName.'/images/'.(empty($noaddcart[$lang])?'full':'empty').'.png" />'; ?></label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printSelect('noaddcart'.$lng, array('1'=>L_YES,'0'=>L_NO), intval($noaddcart[$lang]), false,'" onChange="toggleNoaddcart(this.options[this.selectedIndex].value,\''.$lng.'\');'); ?>
       <?php if($field) {plxUtils::printInput('noaddcart4all','','checkbox'); echo '&nbsp;'.L_ALL.'?';}?>
      </div>
     </div>
     <div class="grid<?php echo $noaddcart[$lang]?'':' hide'; ?>" id="config_notice_noaddcart<?php echo $lng ?>">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_notice_noaddcart<?php echo $lng ?>"><?php $plxPlugin->lang('L_PRODUCTS_BASKET_NO_BUTTON') ;?>&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('notice_noaddcart'.$lng,plxUtils::strCheck($notice_noaddcart[$lang]),'text','0-255', false, 'notice_noaddcart'.($noaddcart[$lang]?'" placeholder="'.$plxPlugin->getLang('L_NOTICE_NOADDCART').' ('.$plxPlugin->getLang('L_BY_DEFAULT').')':'')); ?>
      </div>
     </div>
     <hr/>
     <?php $plxPlugin->lang('L_PRODUCTS_CATEGORIES'); echo $lng?'&nbsp;'.$mmlFlagImgs[$lang]:'';?>&nbsp;:<br/>
<?php $listeCategories = explode(",", $plxPlugin->aProds[$lang][$id]["group"]);?>
      <label for="categorie_000<?php echo $lng;?>">
       <input type="checkbox"
         name="listeCategories<?php echo $lng ?>[]"
         value="000"
         id="categorie_000<?php echo $lng;?>"
         <?php echo (!in_array('000', $listeCategories))? '': ' checked="checked"';?>
        />
       <?php echo plxUtils::strCheck($plxPlugin->getLang('L_PRODUCTS_PAGE_HOME')); ?>
      </label>
<?php
 foreach ($plxPlugin->aProds[$lang] as $idCategorie => $p) {
  if ('1' !== $p['pcat']) {
   continue;
  }
?>
      <label for="categorie_<?php echo $idCategorie.$lng;?>">
       <input type="checkbox"
         name="listeCategories<?php echo $lng ?>[]"
         value="<?php echo $idCategorie;?>"
         id="categorie_<?php echo $idCategorie.$lng;?>"
         <?php echo (!in_array($idCategorie, $listeCategories))? '': ' checked="checked"';?>
        />
       <?php echo plxUtils::strCheck($p["name"]); ?>
      </label>
<?php }# hcaerof ?>
     <hr/>
<?php } else {# !$modProduit ?>
     <?php plxUtils::printInput('pricettc'.$lng,plxUtils::strCheck($pricettc[$lang]),'hidden','0-255');?>
     <?php plxUtils::printInput('poidg'.$lng,plxUtils::strCheck($poidg[$lang]),'hidden','50-255');?>
     <?php plxUtils::printInput('noaddcart'.$lng,plxUtils::strCheck($noaddcart[$lang]),'hidden','0-255');?>
     <?php plxUtils::printInput('iteminstock'.$lng,plxUtils::strCheck($iteminstock[$lang]),'hidden','50-255');?>
     <?php plxUtils::printInput('notice_noaddcart'.$lng,plxUtils::strCheck($notice_noaddcart[$lang]),'hidden','50-255');?>

     <?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminCategory'));# Hook Plugins plxMyShopAdminCategory ?>

<?php }#Fi !$modProduit ?>

<?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminProductMiddle'));# Hook Plugins plxMyShopAdminProductMiddle?>

    <div class="grid">
     <div class="col sml-12">
      <label for="id_chapo<?php echo $lng ?>"><?php echo L_HEADLINE_FIELD . ($lng?'&nbsp;'.$mmlFlagImgs[$lang]:''); ?>&nbsp;:</label>
       <?php plxUtils::printArea('chapo'.$lng,plxUtils::strCheck($chapo[$lang]),0,10,false,'full-width');?>
     </div>
    </div>
    <div class="grid">
     <div class="col sml-12">
      <label for="id_content<?php echo $lng ?>"><?php echo L_CONTENT_FIELD . ($lng?'&nbsp;'.$mmlFlagImgs[$lang]:''); ?>&nbsp;:</label>
       <?php plxUtils::printArea('content'.$lng,plxUtils::strCheck($content[$lang]),0,30,false,'full-width');?>
     </div>
    </div>
    <div class="grid">
     <div class="col sml-12 med-5 label-centered">
      <label for="id_template<?php echo $lng ?>"><?php $plxPlugin->lang('L_PRODUCTS_TEMPLATE_FIELD'); echo $lng?'&nbsp;'.$mmlFlagImgs[$lang]:'';?>&nbsp;:</label>
     </div>
     <div class="col sml-12 med-7">
      <?php plxUtils::printSelect('template'.$lng, $aTemplates, $template[$lang]);?>
     </div>
    </div>
<?php
    $pLink = $plxPlugin->getLang('L_FOR_THIS_'.($modProduit?'PRODUCT':'CAT')).'&nbsp;<i>('.$name[$lang].')</i>';
    $pLink = ' '.($active[$lang]?'<a href="'.$link.'">'.$pLink.'</a>':$pLink);
?>
    <div class="grid">
     <div class="col sml-12 med-5 label-centered">
      <label for="id_title_htmltag<?php echo $lng ?>"><?php $plxPlugin->lang('L_CONTENT_OF_TAG');?> "title" (<?php $plxPlugin->lang('L_OPTIONEL'); echo $lng?'&nbsp;'.$mmlFlagImgs[$lang]:'';?>)&nbsp;:
       <br /><?php echo $pLink; ?>
      </label>
     </div>
     <div class="col sml-12 med-7">
      <?php plxUtils::printInput('title_htmltag'.$lng,plxUtils::strCheck($title_htmltag[$lang]),'text','0-255');?>
     </div>
    </div>
    <div class="grid">
     <div class="col sml-12 med-5 label-centered">
      <label for="id_meta_description<?php echo $lng ?>"><?php $plxPlugin->lang('L_CONTENT_OF_TAG');?> meta "description" (<?php $plxPlugin->lang('L_OPTIONEL'); echo $lng?'&nbsp;'.$mmlFlagImgs[$lang]:'';?>)&nbsp;:
       <br /><?php echo $pLink; ?>
      </label>
     </div>
     <div class="col sml-12 med-7">
      <?php plxUtils::printInput('meta_description'.$lng,plxUtils::strCheck($meta_description[$lang]),'text','0-255'); ?>
     </div>
    </div>
    <div class="grid">
     <div class="col sml-12 med-5 label-centered">
      <label for="id_meta_keywords<?php echo $lng ?>"><?php $plxPlugin->lang('L_CONTENT_OF_TAG');?> meta "keywords" (<?php $plxPlugin->lang('L_OPTIONEL'); echo $lng?'&nbsp;'.$mmlFlagImgs[$lang]:'';?>)&nbsp;:
       <br /><?php echo $pLink; ?>
      </label>
     </div>
     <div class="col sml-12 med-7">
      <?php plxUtils::printInput('meta_keywords'.$lng,plxUtils::strCheck($meta_keywords[$lang]),'text','0-255');?>
     </div>
    </div>
   </div>

   <?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminProductBottom'));# Hook Plugins plxMyShopAdminProductBottom : Note if you not inside action-bar : close & reopen paragraph tag ;) ?>

<?php } ?>
<!-- Fin du content en multilingue -->
  </div><!-- fi tabpage id:tabscontent -->

<!--
  </fieldset>
-->
 </div><!-- fi tabContainer -->

<?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminProductEnd'));# Hook Plugins plxMyShopAdminProductEnd?>

</form>
<?php if($plxPlugin->aLangs){#ml ?>
<script type="text/javascript" src="<?php echo PLX_PLUGINS.$plxPlugin->plugName."/js/tabs.js" ?>"></script>
<?php } ?>

<!-- Utilisation du selecteur d'image natif à PluXml -->
<script type="text/javascript" style="display:none">
 function refreshImg(id_img) {
  var dta = document.getElementById(id_img.replace('_img','')).value;
  if(dta.trim()==='') {
   document.getElementById(id_img).innerHTML = '<img src="<?php echo $imgNoUrl ?>" alt="" />';
  } else {
   var link = dta.match(/^(https?:\/\/[^\s]+)/gi) ? dta : '<?php echo $plxAdmin->racine ?>'+dta;
   document.getElementById(id_img).innerHTML = '<img src="'+link+'" alt="" />';
  }
 }

 function toggleNoaddcart(a,e){
  var b = document.getElementById('id_notice_noaddcart'+e);
  var c = document.getElementById('config_notice_noaddcart'+e);
  var d = document.getElementById('cartImg'+e);
  if(a==1){
   b.setAttribute("placeholder","<?php echo $plxPlugin->getLang('L_NOTICE_NOADDCART').' ('.$plxPlugin->getLang('L_BY_DEFAULT').')';?>");
   c.classList.remove("hide");
   d.src = "<?php echo PLX_PLUGINS.$plxPlugin->plugName.'/images/empty.png'; ?>";
  }else{
   b.removeAttribute("placeholder");c.classList.add("hide");
   d.src = "<?php echo PLX_PLUGINS.$plxPlugin->plugName.'/images/full.png'; ?>";
  }
 }
//On modifie la balise title
if(document.title){
 let t = document.getElementById('page-title');
 document.title = ' <?php echo $plxPlugin->plugName ?> - ' + (t.textContent?t.textContent:t.innerText) + ' - ' + document.title;
}
</script>

<?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminProductFoot'));# Hook Plugins plxMyShopAdminProductFoot?>