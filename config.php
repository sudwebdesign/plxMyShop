<?php if(!defined('PLX_ROOT')) exit;
# command_number : Sur une idée de garys02
$command_files_number = $plxPlugin->totalCommandFiles();
# Liste des langues disponibles et prises en charge par le plugin
$aLangs = array($plxPlugin->dLang);# On garde la langue par défaut (du site) si aucune langue sélectionnée dans plxMyMultiLingue (ou non présént ou activé)
$lng = '_'.$plxPlugin->dLang;# myMultiLingue ou non, certaines données sont pour chaques langues
$imgNoUrl = $plxPlugin::$imageNo;
$baseHook = ucfirst(rtrim(basename(__FILE__),'.php'));#Config
$parXml = realpath($plxPlugin->getPlugInfo('parameters.xml'));#Protegé, func perso : outil sauver & suppr.
$hLng = '';#langue de la page home (products/stars)
# Si le plugin plxMyMultiLingue est installé on filtre sur les langues utilisées
$dFlag = strtoupper($plxPlugin->dLang);#STR
if($plxPlugin->aLangs) {
 $aLangs = $plxPlugin->aLangs;#tableau des langues activées
 $mmlFlagImgs = $plxPlugin->mmlFlagImgs();# Images des drapeaux
 $dFlag = $mmlFlagImgs[$plxPlugin->dLang];#IMG
 $hLng = ($plxPlugin->dLang != $_SESSION['default_lang'])?$plxPlugin->dLang.'/':'';#non defaut on ajoute ##/ ds l'url
}
# home link page of MyShop : products/stars
$pLink = $plxPlugin->getLang('L_PRODUCTS_HOME_PAGE').'&nbsp;<i>('.$plxPlugin->getLang('L_GROUP').')</i>';
$hLink = $plxAdmin->urlRewrite('?'.$hLng.'products/stars');
$hLink = ' <a href="'.$hLink.'">'.$pLink.'</a>';

$tabAffPanier = array(
 'basPage' => $plxPlugin->getLang('L_PANIER_POS_BOTTOM'),
 'basPageStars' => $plxPlugin->getLang('L_PANIER_POS_STARS'),
 'pageSeparee' => $plxPlugin->getLang('L_PANIER_POS_SEPARATE'),
 'pageSepareeStars' => $plxPlugin->getLang('L_PANIER_POS_BOTH_STARS'),
 'partout' => $plxPlugin->getLang('L_PANIER_POS_BOTH'),
);

$tabPosDevise = array(
 '0' => $plxPlugin->getLang('L_AFTER'),#after
 '1' => $plxPlugin->getLang('L_BEFORE')#before
);
if(!empty($plxPlugin->getLang('MONEY_POSITION'))){
 $tabPosDevise = array_reverse($tabPosDevise);
}
$timeselection = array();
for($h=0;$h<25;$h++)
 $timeselection[ str_pad($h,2,'0',STR_PAD_LEFT).':00' ] = str_pad($h,2,'0',STR_PAD_LEFT).':00';
$dDateSample = date('Y').'-12-25, '.date('Y').'-12-29_'.(date('Y')+1).'-01-02, '.(date('Y')+1).'-02-22';
$var = array();

# Hook Plugins plxMyShopConfigBegin
eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName . $baseHook . 'Begin'));

if(!empty($_POST)){
 # Controle du token du formulaire
 plxToken::validateFormToken($_POST);
 # Remise a zéro des params (profil admin)
 if(isset($_POST['rezet']) AND $_SESSION['profil'] < PROFIL_MANAGER){
  $_SESSION['info'] = '';
  if(isset($_POST['rezet_ok'])) {
   $nfo = '';
   if(isset($_POST['rezet_back'])){
    $parXm1 = strtr($parXml,array('.xml'=>'_'.date('Y-m-d-H-i-s').'.xml'));#Is 1 (one) not L
    if(copy($parXml, $parXm1))
     $nfo .= $plxPlugin->getLang('L_CONFIG_REZET_BACK_MSG').'<br/>'.$parXm1.'<br/>';
    else
     $nfo .= $plxPlugin->getLang('L_CONFIG_REZET_BACK_MSG_NO').'<br/>';
   }
   # Efface plxMyShop.xml de data/configuration/plugins
   if(unlink($parXml))
    $nfo .= $plxPlugin->getLang('L_CONFIG_REZET_OK_MSG').':<br/>'.$parXml.'<br/>';
   else
    $nfo .= $plxPlugin->getLang('L_CONFIG_REZET_NO_MSG').'<br/>';
  }else
    $nfo .= $plxPlugin->getLang('L_CONFIG_REZET_DO_NOTHING_MSG').'<br/>';
  $_SESSION['info'] .= trim($nfo,'<br/>');
  header('Location: parametres_plugin.php?p='.$plxPlugin->plugName);
  exit;
 }#Fi RAZ conig

 # socolissimo reco
 $plxPlugin->setParam('shipping_by_price', isset($_POST['shipping_by_price'])?1:0, 'numeric');
 $plxPlugin->setParam('shipping_nb_lines', ($_POST['shipping_nb_lines']>=99)?99:intval($_POST['shipping_nb_lines']), 'numeric');
 $plxPlugin->setParam('shipping_colissimo', isset($_POST['shipping_colissimo'])?1:0, 'numeric');
 $plxPlugin->setParam('delivery_date', isset($_POST['delivery_date'])?1:0, 'numeric');
 $plxPlugin->setParam('delivery_firstday', ($_POST['delivery_firstday']>=7)?1:intval($_POST['delivery_firstday']), 'numeric');
 $plxPlugin->setParam('delivery_start_time', $_POST['delivery_start_time'], 'string');
 $plxPlugin->setParam('delivery_end_time', $_POST['delivery_end_time'], 'string');
 $plxPlugin->setParam('delivery_nb_timeslot', ($_POST['delivery_nb_timeslot']>=1440)?1440:intval($_POST['delivery_nb_timeslot']), 'numeric');#1.0.0 en minutes
 $plxPlugin->setParam('delivery_nb_days', intval($_POST['delivery_nb_days']), 'numeric');
 # Verif entre le slot et temps mini pour livraison : ds la logique le tmps mini de livraison doit être >= au slot ;)
 $nb_times = ($_POST['delivery_nb_times'] < $_POST['delivery_nb_timeslot'])?$_POST['delivery_nb_timeslot']:$_POST['delivery_nb_times'];
 $plxPlugin->setParam('delivery_nb_times', intval($nb_times), 'numeric');#v1.0.0 zero day
 $plxPlugin->setParam('delivery_disallowed_dates', $_POST['delivery_disallowed_dates'], 'string');#v0.13.2
 $delivery_day = '';#v0.13.2
 for($d = 0; $d<7; $d++){
  $delivery_day .= (isset($_POST['delivery_day_'.$d])? '1': '0').',';
 }
 $plxPlugin->setParam('delivery_day', rtrim($delivery_day,','), 'string');

 $plxPlugin->setParam('freeshipw', $_POST['freeshipw'], 'string');//free shipping weight
 $plxPlugin->setParam('freeshipp', $_POST['freeshipp'], 'string');//free shipping price
 $plxPlugin->setParam('acurecept', $_POST['acurecept'], 'string');
 for($i=1;$i<=$plxPlugin->getParam('shipping_nb_lines');$i++){
  $num=str_pad($i, 2, '0', STR_PAD_LEFT);
  $plxPlugin->setParam('p'.$num, $_POST['p'.$num], 'string');
  $plxPlugin->setParam('pv'.$num, $_POST['pv'.$num], 'string');
 }
 # end socolissimo reco

 $plxPlugin->setParam('payment_cheque', isset($_POST['payment_cheque'])?1:0, 'numeric');
 $plxPlugin->setParam('payment_cash', isset($_POST['payment_cash'])?1:0, 'numeric');
 $plxPlugin->setParam('order_view', isset($_POST['order_view'])?1:0, 'numeric');

 $plxPlugin->setParam('email', $_POST['email'], 'string');
 $plxPlugin->setParam('email_cc', $_POST['email_cc'], 'string');
 $plxPlugin->setParam('email_bcc', $_POST['email_bcc'], 'string');
 $plxPlugin->setParam('subject'.$lng, $_POST['subject'], 'string');
 $plxPlugin->setParam('newsubject'.$lng, $_POST['newsubject'], 'string');
 $plxPlugin->setParam('thanks_msg'.$lng, $_POST['thanks_msg'], 'string');
 $plxPlugin->setParam('template', $_POST['template'], 'string');
 $plxPlugin->setParam('image_none', $_POST['image_none'], 'string');#1.0.0

 $plxPlugin->setParam('name_cart'.$lng, $_POST['name_cart'], 'string');#1.0.0
 $plxPlugin->setParam('template_home'.$lng, $_POST['template_home'], 'string');#1.0.0
 $plxPlugin->setParam('name_home'.$lng, $_POST['name_home'], 'string');#1.0.0
 $plxPlugin->setParam('image_home'.$lng, $_POST['image_home'], 'string');#1.0.0
 $plxPlugin->setParam('chapo_home'.$lng, $_POST['chapo_home'], 'cdata');#1.0.0
 $plxPlugin->setParam('content_home'.$lng, $_POST['content_home'], 'cdata');#1.0.0
 $plxPlugin->setParam('title_htmltag'.$lng, $_POST['title_htmltag'], 'string');#1.0.0
 $plxPlugin->setParam('meta_description'.$lng, $_POST['meta_description'], 'string');#1.0.0
 $plxPlugin->setParam('meta_keywords'.$lng, $_POST['meta_keywords'], 'string');#1.0.0

 $plxPlugin->setParam('shop_name', $_POST['shop_name'], 'string');
 $plxPlugin->setParam('position_admin', $_POST['position_admin'], 'numeric');#1.0.0
 $plxPlugin->setParam('commercant_name', $_POST['commercant_name'], 'string');
 $plxPlugin->setParam('commercant_street', $_POST['commercant_street'], 'string');
 $plxPlugin->setParam('commercant_city', $_POST['commercant_city'], 'string');
 $plxPlugin->setParam('devise'.$lng, $_POST['devise'], 'string');
 $plxPlugin->setParam('position_devise'.$lng, $_POST['position_devise'], 'numeric');
 $plxPlugin->setParam('commercant_postcode', $_POST['commercant_postcode'], 'string');
 $plxPlugin->setParam('menu_position', $_POST['menu_position'], 'numeric');
 $plxPlugin->setParam('submenu'.$lng, trim($_POST['submenu']), 'string');
 $plxPlugin->setParam('afficheProductsMenu', isset($_POST['afficheProductsMenu'])?1:0, 'numeric');
 $plxPlugin->setParam('afficheLienPanierTop', isset($_POST['afficheLienPanierTop'])?1:0, 'numeric');
 $afficheCategoriesMenu = isset($_POST['afficheCategoriesMenu']) ? '1' : '0';
 $plxPlugin->setParam('afficheCategoriesMenu', $afficheCategoriesMenu, 'numeric');
 $plxPlugin->setParam('affPanier', $_POST['affPanier'], 'string');
 $affichePanierMenu = isset($_POST['affichePanierMenu']) ? '1' : '0';
 $plxPlugin->setParam('affichePanierMenu',$affichePanierMenu, 'numeric');
 $plxPlugin->setParam('localStorage', isset($_POST['localStorage'])?1:0, 'numeric');
 $plxPlugin->setParam('cookie', (isset($_POST['localStorage'])&&isset($_POST['cookie'])?1:0), 'numeric');
 $plxPlugin->setParam('useLangCGVDefault', isset($_POST['useLangCGVDefault'])?1:0, 'numeric');
 $plxPlugin->setParam('libelleCGV_' . $plxPlugin->dLang, $_POST['libelleCGV'], 'string');
 $plxPlugin->setParam('urlCGV', $_POST['urlCGV'], 'string');

 $plxPlugin->setParam('racine_commandes', (!(trim($_POST['racine_commandes']))?'data/commandes/':trim($_POST['racine_commandes'])), 'string');
 $plxPlugin->setParam('racine_products', (!(trim($_POST['racine_products']))?'data/products/':trim($_POST['racine_products'])), 'string');

 if(isset($_POST['command_number_set'])){
  $plxPlugin->commandNumber((!(trim($_POST['command_number']))?$command_files_number:trim($_POST['command_number'])), true);
 }

# Hook Plugins plxMyShopConfigPost
 eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName . $baseHook . 'Post'));

 $plxPlugin->saveParams();
 header('Location: parametres_plugin.php?p='.$plxPlugin->plugName);
 exit;
}
# Charge les valeurs multilingues par défaut avec les fichiers de langue
$tlangs = array();
foreach($aLangs as $tlang) {
 if($tlang==$plxPlugin->dLang){
  $tlangs[$tlang] = $plxPlugin->loadLang(PLX_PLUGINS.$plxPlugin->plugName.'/lang/'.$tlang.'.php');//Fatal error: Call to private method plxPlugin::loadLang() from context '' : (pluxml-5.2)
  $var['subject_'.$tlang] = $plxPlugin->getParam('subject_'.$tlang)=='' ? (!empty($tlangs[$tlang]['L_EMAIL_CUST_SUBJECT'])? $tlangs[$tlang]['L_EMAIL_CUST_SUBJECT']: $plxPlugin->getLang('L_EMAIL_CUST_SUBJECT')): $plxPlugin->getParam('subject_'.$tlang);
  $var['newsubject_'.$tlang] = $plxPlugin->getParam('newsubject_'.$tlang)=='' ? (!empty($tlangs[$tlang]['L_EMAIL_SUBJECT'])? $tlangs[$tlang]['L_EMAIL_SUBJECT']: $plxPlugin->getLang('L_EMAIL_SUBJECT')): $plxPlugin->getParam('newsubject_'.$tlang);
  $var['thanks_msg_'.$tlang] = $plxPlugin->getParam('thanks_msg_'.$tlang)=='' ? (!empty($tlangs[$tlang]['L_EMAIL_SENT_THANKS'])? $tlangs[$tlang]['L_EMAIL_SENT_THANKS']: $plxPlugin->getLang('L_EMAIL_SENT_THANKS')): $plxPlugin->getParam('thanks_msg_'.$tlang);
  $var['devise_'.$tlang] = $plxPlugin->getParam('devise_'.$tlang)=='' ? (!empty($tlangs[$tlang]['MONEY'])?$tlangs[$tlang]['MONEY']: $plxPlugin->getLang('MONEY')): $plxPlugin->getParam('devise_'.$tlang);
  $var['position_devise_'.$tlang] = $plxPlugin->getParam('position_devise_'.$tlang)=='' ? (!empty($tlangs[$tlang]['MONEY_POSITION'])? $tlangs[$tlang]['MONEY_POSITION']: $plxPlugin->getLang('MONEY_POSITION')): $plxPlugin->getParam('position_devise_'.$tlang);
  break;
 }
}
unset($tlangs, $tlang);
$var['subject'] = $var['subject'.$lng];
$var['newsubject'] = $var['newsubject'.$lng];
$var['thanks_msg'] = $var['thanks_msg'.$lng];
# initialisation des variables communes à chaque langue
$var['payment_cheque'] = $plxPlugin->getParam('payment_cheque');
$var['payment_cash'] = $plxPlugin->getParam('payment_cash');
$var['order_view'] = $plxPlugin->getParam('order_view');
# socolissimo reco
$var['shipping_by_price'] = $plxPlugin->getParam('shipping_by_price')=='' ? 0 : $plxPlugin->getParam('shipping_by_price');
$var['shipping_nb_lines'] = $plxPlugin->getParam('shipping_nb_lines')=='' ? 2 : $plxPlugin->getParam('shipping_nb_lines');
$var['shipping_colissimo'] = $plxPlugin->getParam('shipping_colissimo');
# delivery date time
$var['delivery_date'] = $plxPlugin->getParam('delivery_date');
$var['delivery_firstday'] = $plxPlugin->getParam('delivery_firstday')=='' ? 1 : $plxPlugin->getParam('delivery_firstday');
$var['delivery_start_time'] = ('' === $plxPlugin->getParam('delivery_start_time')) ? current($timeselection) : $plxPlugin->getParam('delivery_start_time');
$var['delivery_end_time'] = ('' === $plxPlugin->getParam('delivery_end_time')) ? end($timeselection) : $plxPlugin->getParam('delivery_end_time');
$var['delivery_nb_timeslot'] = $plxPlugin->getParam('delivery_nb_timeslot')=='' ? 2 : $plxPlugin->getParam('delivery_nb_timeslot');
$var['delivery_nb_days'] = intval($plxPlugin->getParam('delivery_nb_days'));
$var['delivery_nb_times'] = intval($plxPlugin->getParam('delivery_nb_times'));
$var['delivery_disallowed_dates'] = trim($plxPlugin->getParam('delivery_disallowed_dates'));#v0.13.2
$var['delivery_day'] = explode(',',($plxPlugin->getParam('delivery_day') == ''? '1,1,1,1,1,1,1': $plxPlugin->getParam('delivery_day')));#v0.13.2

$var['freeshipw'] = $plxPlugin->getParam('freeshipw');
$var['freeshipp'] = $plxPlugin->getParam('freeshipp');
$var['acurecept'] = $plxPlugin->getParam('acurecept');

for($i=1;$i<=$var['shipping_nb_lines'];$i++){
 $num=str_pad($i, 2, '0', STR_PAD_LEFT);
 $var['p'.$num] = $plxPlugin->getParam('p'.$num);
 $var['pv'.$num] = $plxPlugin->getParam('pv'.$num);
}
#end socolissimo reco
$var['email'] = $plxPlugin->getParam('email');
$var['email_cc'] = $plxPlugin->getParam('email_cc');
$var['email_bcc'] = $plxPlugin->getParam('email_bcc');
$var['template'] = $plxPlugin->getParam('template')=='' ? 'static.php' : $plxPlugin->getParam('template');
$var['image_none'] = $plxPlugin->getParam('image_none');
$var['template_home'] = $plxPlugin->getParam('template_home'.$lng)=='' ? 'static.php' : $plxPlugin->getParam('template_home'.$lng);
$var['name_cart'] = $plxPlugin->getParam('name_cart'.$lng);
$var['name_home'] = $plxPlugin->getParam('name_home'.$lng);
$var['image_home'] = $plxPlugin->getParam('image_home'.$lng);
$var['chapo_home'] = $plxPlugin->getParam('chapo_home'.$lng);
$var['content_home'] = $plxPlugin->getParam('content_home'.$lng);
$var['title_htmltag'] = $plxPlugin->getParam('title_htmltag'.$lng);
$var['meta_description'] = $plxPlugin->getParam('meta_description'.$lng);
$var['meta_keywords'] = $plxPlugin->getParam('meta_keywords'.$lng);
$var['shop_name'] = $plxPlugin->getParam('shop_name')=='' ? 'My Shop' : $plxPlugin->getParam('shop_name');
$var['position_admin'] = $plxPlugin->getParam('position_admin')=='' ? 5 : $plxPlugin->getParam('position_admin');
$var['commercant_name'] = $plxPlugin->getParam('commercant_name')=='' ? 'David.L' : $plxPlugin->getParam('commercant_name');
$var['commercant_street'] = $plxPlugin->getParam('commercant_street')=='' ? 'Rue de la plume' : $plxPlugin->getParam('commercant_street');
$var['commercant_postcode'] = $plxPlugin->getParam('commercant_postcode')=='' ? '09600' : $plxPlugin->getParam('commercant_postcode');
$var['commercant_city'] = $plxPlugin->getParam('commercant_city')=='' ? 'Dun' : $plxPlugin->getParam('commercant_city');
$var['devise'] = $plxPlugin->getParam('devise'.$lng)=='' ? $var['devise'.$lng] : $plxPlugin->getParam('devise'.$lng);
$var['position_devise'] = !is_numeric($plxPlugin->getParam('position_devise'.$lng)) ? (!is_numeric($var['position_devise'.$lng])? $var['position_devise'.$lng]: current(array_keys($tabPosDevise))) : $plxPlugin->getParam('position_devise'.$lng);
$var['menu_position'] = $plxPlugin->getParam('menu_position')=='' ? 3 : $plxPlugin->getParam('menu_position');
$var['submenu'] = $plxPlugin->getParam('submenu'.$lng);
$var['afficheProductsMenu'] = $plxPlugin->getParam('afficheProductsMenu');
$var['afficheLienPanierTop'] = $plxPlugin->getParam('afficheLienPanierTop');
$var['afficheCategoriesMenu'] = $plxPlugin->getParam('afficheCategoriesMenu');
$var['affPanier'] = ('' === $plxPlugin->getParam('affPanier')) ? current(array_keys($tabAffPanier)) : $plxPlugin->getParam('affPanier');
$var['affichePanierMenu'] = $plxPlugin->getParam('affichePanierMenu');
$var['localStorage'] = $plxPlugin->getParam('localStorage')!='' ? $plxPlugin->getParam('localStorage') : '1';
$var['cookie'] = $plxPlugin->getParam('cookie')!='' ? $plxPlugin->getParam('cookie') : '1';
$var['useLangCGVDefault'] = $plxPlugin->aLangs ? $plxPlugin->getParam('useLangCGVDefault') : '0';
$var['libelleCGV'] = empty($plxPlugin->getParam('libelleCGV_' . $plxPlugin->dLang)) ? $plxPlugin->getLang('L_COMMANDE_LIBELLE_DEFAUT') : $plxPlugin->getParam('libelleCGV_' . $plxPlugin->dLang);
$var['urlCGV'] = $plxPlugin->getParam('urlCGV');

$var['racine_commandes'] = (!$plxPlugin->getParam('racine_commandes')?'data/commandes/':trim($plxPlugin->getParam('racine_commandes')));
$var['racine_products'] = (!$plxPlugin->getParam('racine_products')?'data/products/':trim($plxPlugin->getParam('racine_products')));

$var['command_number'] = $plxPlugin->commandNumber(0);

# On récupère les templates des pages statiques
$aTemplates = array();
$files = plxGlob::getInstance(PLX_ROOT.$plxAdmin->aConf['racine_themes'].$plxAdmin->aConf['style']);
if ($array = $files->query('/^static(-[a-z0-9-_]+)?.php$/')) {
 foreach($array as $k=>$v)
  $aTemplates[$v] = $v;
}
# Hook Plugins plxMyShopConfigInit
eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName . $baseHook . 'Init'));
?>
<h3 id="pmsTitle" class="page-title hide"><?php $plxPlugin->lang('L_MENU_CONFIG').' '.$plxPlugin->getInfo('title');?></h3>
<script type="text/javascript">//surcharge du titre dans l'admin
 var title = document.getElementById('pmsTitle').innerHTML;
 document.getElementsByClassName('inline-form')[0].firstChild.nextSibling.innerHTML = '<?php echo $plxPlugin->plugName; ?> - '+title;
</script>
<form id="config_plxmyshop" action="parametres_plugin.php?p=<?php echo $plxPlugin->plugName; ?>" method="post">
<?php echo plxToken::getTokenPostMethod() ?>
  <div class="in-action-bar xxl plx<?php echo str_replace('.','-',@PLX_VERSION); echo $plxPlugin->aLangs?' multilingue':'';?>">
   <button type="submit" name="ok" title="<?php $plxPlugin->lang('L_CONFIG_SUBMIT') ?>">☻</button>
   <?php $plxPlugin->adminMenu('configuration');?>
  </div>
<div id="tabContainer" data-storetabs="myShopConfig-<?php echo plxUtils::title2filename($plxAdmin->racine) ?>">
<?php
$aTabs = array(
 'shop',
 'delivery',
 'payments',
 'emails',
 'orders',
 'menu',
 'pages',
 'home',
 'data',#cookies + localStroage (tab memo ?)
);
# Hook Plugins plxMyShopConfigForm (modify aTabs, ...)
eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName . $baseHook . 'Form'));
?>
   <div class="tabs">
    <ul class="col sml-12">
<?php foreach($aTabs AS $i => $tab){ ?>
     <li id="tabHeader_<?php echo $tab.(!$i?'" class="active':'') ?>"><?php $plxPlugin->lang('L_' . strtoupper($tab))?></li>
<?php } ?>
    </ul>
   </div>

  <div class="tabscontent">
   <fieldset class="config">

    <?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName . $baseHook));# Hook Plugins plxMyShopConfig ?>

    <div class="tabpage active" id="tabpage_shop">
     <h2><?php $plxPlugin->lang('L_CONFIG_SHOP_INFO') ?></h2>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_position_admin"><?php $plxPlugin->lang('L_CONFIG_POSITION_ADMIN') ?>&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('position_admin',intval($var['position_admin']),'number','2-120',false,'" min="0" step="1') ?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_shop_name"><?php $plxPlugin->lang('L_CONFIG_SHOP_NAME') ?>&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('shop_name',$var['shop_name'],'text','0-120') ?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_commercant_name"><?php $plxPlugin->lang('L_CONFIG_SHOP_OWNER') ?>&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('commercant_name',$var['commercant_name'],'text','0-120') ?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_commercant_street"><?php $plxPlugin->lang('L_CONFIG_SHOP_STREET') ?>&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('commercant_street',$var['commercant_street'],'text','0-120') ?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_commercant_postcode"><?php $plxPlugin->lang('L_CONFIG_SHOP_ZIP') ?>&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('commercant_postcode',$var['commercant_postcode'],'text','0-120') ?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_commercant_city"><?php $plxPlugin->lang('L_CONFIG_SHOP_TOWN') ?>&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('commercant_city',$var['commercant_city'],'text','0-120') ?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_devise"><?php $plxPlugin->lang('L_CONFIG_SHOP_CURRENCY') ?>&nbsp;:&nbsp;(<?php echo $dFlag ?>)</label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('devise',$var['devise'],'text','5-120') ?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_position_devise"><?php $plxPlugin->lang('L_CONFIG_POSITION_CURRENCY') ?>&nbsp;:&nbsp;(<?php echo $dFlag ?>)</label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printSelect('position_devise', $tabPosDevise, $var['position_devise']) ?>
      </div>
     </div>

     <?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName . $baseHook . 'Shop'));# Hook Plugins plxMyShopConfigShop ?>

    </div><!-- fi tabpage shop -->
    <div class="tabpage" id="tabpage_delivery">
     <h2><?php $plxPlugin->lang('L_CONFIG_DELIVERY_TITLE') ?></h2>
     <div class="grid">
      <div class="col sml-8 lrg-5 label-centered">
       <label for="id_delivery_date"><?php $plxPlugin->lang('L_CONFIG_DELIVERY_DATE');?>&nbsp;:</label>
      </div>
      <div class="col sml-3 sml-push-1 lgr-push-5">
       <label class="switch switch-left-right">
        <input class="switch-input" id="id_delivery_date" name="delivery_date" type="checkbox"<?php echo ((!$var['delivery_date']) ? '' : ' checked="checked"').' onchange="if (this.checked) { document.getElementById(\'blockdelidate\').style.display=\'block\';}else{document.getElementById(\'blockdelidate\').style.display=\'none\';}"';?> />
        <span class="switch-label" data-on="<?php echo L_YES ?>" data-off="<?php echo L_NO ?>"></span>
        <span class="switch-handle"></span>
       </label>
      </div>
     </div>
     <fieldset id="blockdelidate" style="display:<?php echo ($var['delivery_date']==1?'block':'none'); ?>;">
      <legend><?php $plxPlugin->lang('L_CONFIG_DELIVERY_MINDAYS') ?></legend>
      <div class="scrollable-table">
       <table class="full-width">
        <tr>
         <td class="text-right"><?php $plxPlugin->lang('L_CONFIG_FIRSTDAYS') ?>&nbsp;:</td>
         <td class="text-left"><?php
          plxUtils::printselect('delivery_firstday'
          ,explode(' ', ucfirst($plxPlugin->getLang('L_WEEK_PIKADAY_LONG')))
          ,$var['delivery_firstday']);
        ?></td>
        </tr>
        <tr>
         <td class="text-right"><?php $plxPlugin->lang('L_CONFIG_DELIVERY_STARTTIME') ?>&nbsp;:</td>
         <td class="text-left"><?php plxUtils::printSelect('delivery_start_time',$timeselection, $var['delivery_start_time']) ?></td>
        </tr>
        <tr>
         <td class="text-right"><?php $plxPlugin->lang('L_CONFIG_DELIVERY_ENDTIME') ?>&nbsp;:</td>
         <td class="text-left"><?php plxUtils::printSelect('delivery_end_time',$timeselection, $var['delivery_end_time']) ?></td>
        </tr>
        <tr>
         <td class="text-right"><?php $plxPlugin->lang('L_CONFIG_DELIVERY_TIMESLOT') ?>&nbsp;:</td>
         <td class="text-left"><?php plxUtils::printInput('delivery_nb_timeslot',$var['delivery_nb_timeslot'],'number','4-4',false,'" min="1" max="1440') ?><?php $plxPlugin->lang('L_M') ?></td>
        </tr>
        <tr>
         <td class="text-right"><?php $plxPlugin->lang('L_CONFIG_NB_MIN_TIME') ?>&nbsp;:</td>
         <td class="text-left"><?php plxUtils::printInput('delivery_nb_days',strval($var['delivery_nb_days']),'number','4-4',false,'" min="0" max="9999') ?><?php $plxPlugin->lang('L_J') ?></td>
        </tr>
        <tr id="deliverynbtimes" style="display:<?=empty($var['delivery_nb_days'])?'table-row':'none';?>">
         <td class="text-right"><?php $plxPlugin->lang('L_CONFIG_NB_MIN_TIME') ?>&nbsp;:</td>
         <td class="text-left"><?php plxUtils::printInput('delivery_nb_times',strval($var['delivery_nb_times']),'number','4-4',false,'" min="0" max="1440') ?><?php $plxPlugin->lang('L_M') ?></td>
         <script>const nbdays = document.getElementById('id_delivery_nb_days');const nbtimes = document.getElementById('deliverynbtimes');function showtimes(){if(nbdays.value>0)if(nbtimes.style.display!='none')nbtimes.style.display='none';else return;else nbtimes.style.display='';}nbdays.addEventListener('change', function(e){showtimes();});</script>
        </tr>
        <tr>
         <td colspan="2"><?php $plxPlugin->lang('L_CONFIG_DELIVERY_DISALOWED_DATES') ?>&nbsp;:
          <br />
          <?php plxUtils::printInput('delivery_disallowed_dates',$var['delivery_disallowed_dates'],'text','',false, '" placeholder="'.$dDateSample) ?>
          <br />
          <sup><?php $plxPlugin->lang('L_CONFIG_DELIVERY_DISALOW_DATES') ?>&nbsp;:</sup><br />
          <sup><b><?php echo $dDateSample; ?></b></sup>
         </td>
        </tr>
        <tr>
         <td class="text-left" colspan="2">
          <span><?php $plxPlugin->lang('L_DELIVERY_DAY') ?>&nbsp;:</span>
         </td>
        </tr>
<?php for($d=0; $d<7; $d++): ?>
        <tr>
         <td class="text-left">
          <label for="delivery_day_<?php echo $d ?>"><?php $plxPlugin->lang('L_DAY_'.$d);?>&nbsp;:</label>
         </td>
         <td class="text-right">
          <label class="switch switch-left-right">
           <input class="switch-input" id="delivery_day_<?php echo $d ?>" name="delivery_day_<?php echo $d ?>" type="checkbox"<?php echo (!$var['delivery_day'][$d]) ? '' : ' checked="checked"';?> />
           <span class="switch-label" data-on="<?php echo L_YES ?>" data-off="<?php echo L_NO ?>"></span>
           <span class="switch-handle"></span>
          </label>
         </td>
        </tr>
<?php endfor; ?>
       </table>
      </div>
     </fieldset>
     <div class="grid">
      <div class="col sml-8 lrg-5 label-centered">
       <label for="id_shipping_colissimo"><?php $plxPlugin->lang('L_CONFIG_DELIVERY_SHIPPING');?>&nbsp;:</label>
      </div>
      <div class="col sml-3 sml-push-1 lgr-push-5">
       <label class="switch switch-left-right">
        <input class="switch-input" id="id_shipping_colissimo" name="shipping_colissimo" type="checkbox" <?php echo ((!$var['shipping_colissimo']) ? '' : ' checked="checked"').' onchange="if (this.checked) { document.getElementById(\'blocksocoreco\').style.display=\'block\';}else{document.getElementById(\'blocksocoreco\').style.display=\'none\';}"';?> />
        <span class="switch-label" data-on="<?php echo L_YES ?>" data-off="<?php echo L_NO ?>"></span>
        <span class="switch-handle"></span>
       </label>
      </div>
     </div>
     <fieldset id="blocksocoreco" style="display:<?php echo ($var['shipping_colissimo']==1?'block':'none'); ?>;">
      <legend><?php $plxPlugin->lang('L_CONFIG_DELIVERY_CONFIG') ?></legend>
      <div class="scrollable-table">
       <table class="full-width">
        <tr>
         <td class="text-right"><b title="<?php $plxPlugin->lang('L_CONFIG_FREESHIPP') ?>"><?php $plxPlugin->lang('L_CONFIG_FREE') ?></b>&nbsp;(<?php $plxPlugin->lang('L_OPTIONEL');?>)&nbsp;:</td>
         <td><?php plxUtils::printInput('freeshipp',$var['freeshipp'],'text','11-120') ?></td>
         <td><?php echo $var['devise'];?>&nbsp;<sub><?php $plxPlugin->lang('L_AND'); ?>/<?php $plxPlugin->lang('L_OR'); ?></sub></td>
         <td><?php plxUtils::printInput('freeshipw',$var['freeshipw'],'text','11-120') ?></td>
         <td>kg</td>
        </tr>
        <tr>
         <td class="text-right"><?php $plxPlugin->lang('L_CONFIG_PRIX_BASE') ?>&nbsp;:</td>
         <td><?php plxUtils::printInput('acurecept',$var['acurecept'],'text','11-120') ?></td>
         <td class="text-left"><?php echo $var['devise'];?></td>
         <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
         <td colspan="3" class="text-right"><?php $plxPlugin->lang('L_CONFIG_NB_LINES') ?>  <a class="hint"><span><?php $plxPlugin->lang('L_CONFIG_NB_LINES') ?></span></a>&nbsp;:</td>
         <td colspan="2"><?php plxUtils::printInput('shipping_nb_lines',$var['shipping_nb_lines'],'number','2-2',false,'" min="1" max="99') ?></td>
        </tr>
        <tr>
         <td class="text-left" colspan="2">
          <label for="id_shipping_by_price"><b title="<?php echo plxUtils::strCheck($plxPlugin->getLang('L_CONFIG_DELIVERY_BY_PRICE'));?>"><?php $plxPlugin->lang('L_CONFIG_DELIVERY_BY_PRICE');?></b>&nbsp;:</label>
         </td>
         <td colspan="3">
          <label class="switch switch-left-right">
           <input class="switch-input" id="id_shipping_by_price" name="shipping_by_price" type="checkbox"<?php
           echo
            ((!$var['shipping_by_price']) ? '' : ' checked="checked"')
            .' onchange="if (this.checked) {confdelivery(\''.$plxPlugin->getLang('L_CONFIG_DELIVERY_PRICE').$var['devise'].'\')}else{confdelivery(\''.$plxPlugin->getLang('L_CONFIG_DELIVERY_WEIGHT').' '.$plxPlugin->getLang('KG').'\')}"';
           ?> />
           <span class="switch-label" data-on="<?php echo L_YES ?>" data-off="<?php echo L_NO ?>"></span>
           <span class="switch-handle"></span>
           <script type="text/javascript">
           function confdelivery(content) {
            var elems = document.getElementsByTagName('td'), i;
            for (i in elems) {
             if((' ' + elems[i].className + ' ').indexOf(' confdelivery ') > -1) {
              elems[i].innerHTML = content + '&nbsp;:';
             }
            }
           }
           </script>
          </label>
         </td>
        </tr>
<?php
         for($i=1;$i<=$var['shipping_nb_lines'];$i++){
          $num=str_pad($i, 2, '0', STR_PAD_LEFT);
          $cnf=(!$var['shipping_by_price']) ? $plxPlugin->getLang('L_CONFIG_DELIVERY_WEIGHT') .' ' . $plxPlugin->getLang('KG'): $plxPlugin->getLang('L_CONFIG_DELIVERY_PRICE').$var['devise'];
?>
        <tr>
         <td class="text-right confdelivery"><?php echo $cnf; ?>&nbsp;:</td>
         <td><?php plxUtils::printInput('p'.$num,$var['p'.$num],'text','0-120') ?></td>
         <td class="text-center">&lt;=</td>
         <td><?php plxUtils::printInput('pv'.$num,$var['pv'.$num],'text','0-120') ?></td>
         <td class="text-left"><?php echo $var['devise'];?></td>
        </tr>
<?php } ?>
       </table>
      </div>
     </fieldset>

     <?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName . $baseHook . 'Delivery'));# Hook Plugins plxMyShopConfigDelivery ?>

    </div><!-- fi tabpage delivery -->
    <div class="tabpage" id="tabpage_payments">
     <h2><?php $plxPlugin->lang('L_CONFIG_PAYMENTS_TITLE') ?></h2>
     <div class="grid">
      <div class="col sml-8 lrg-5 label-centered">
       <label for="id_payment_cheque"><?php $plxPlugin->lang('L_CONFIG_PAYMENT_CHEQUE');?>&nbsp;:</label>
      </div>
      <div class="col sml-3 sml-push-1 lgr-push-5">
       <label class="switch switch-left-right">
        <input class="switch-input" id="id_payment_cheque" name="payment_cheque" type="checkbox"<?php echo (!$var['payment_cheque']) ? '' : ' checked="checked"';?> />
        <span class="switch-label" data-on="<?php echo L_YES ?>" data-off="<?php echo L_NO ?>"></span>
        <span class="switch-handle"></span>
       </label>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-8 lrg-5 label-centered">
       <label for="id_payment_cash"><?php $plxPlugin->lang('L_CONFIG_PAYMENT_CASH');?>&nbsp;:</label>
      </div>
      <div class="col sml-3 sml-push-1 lgr-push-5">
      <label class="switch switch-left-right">
       <input class="switch-input" id="id_payment_cash" name="payment_cash" type="checkbox"<?php echo (!$var['payment_cash']) ? '' : ' checked="checked"';?> />
       <span class="switch-label" data-on="<?php echo L_YES ?>" data-off="<?php echo L_NO ?>"></span>
       <span class="switch-handle"></span>
      </label>
      </div>
     </div>

     <?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName . $baseHook . 'Payments'));# Hook Plugins plxMyShopConfigPayments ?>

    </div><!-- fi tabpage payments -->
    <div class="tabpage" id="tabpage_emails">
     <h2><?php $plxPlugin->lang('L_CONFIG_EMAIL_ORDER_TITLE') ?></h2>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_email"><?php $plxPlugin->lang('L_EMAIL') ?>&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <input name="email" value="<?php echo $var['email']; ?>" type="text" />
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_email_cc"><?php $plxPlugin->lang('L_EMAIL_CC') ?>&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <input name="email_cc" value="<?php echo $var['email_cc']; ?>" type="text" />
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_email_bcc"><?php $plxPlugin->lang('L_EMAIL_BCC') ?>&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <input name="email_bcc" value="<?php echo $var['email_bcc']; ?>" type="text" />
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_subject"><?php $plxPlugin->lang('L_CONFIG_EMAIL_ORDER_SUBJECT_CUST') ?>&nbsp;(<?php echo $dFlag ?>)&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('subject',$var['subject'],'text','0-120') ?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_newsubject"><?php $plxPlugin->lang('L_CONFIG_EMAIL_ORDER_SUBJECT_SHOP') ?>&nbsp;(<?php echo $dFlag ?>)&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('newsubject',$var['newsubject'],'text','0-120') ?>
      </div>
     </div>

     <?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName . $baseHook . 'Mail'));# Hook Plugins plxMyShopConfigMail ?>

    </div><!-- fi tabpage mail-->
    <div class="tabpage" id="tabpage_orders">
     <h2><?php $plxPlugin->lang('L_CONFIG_VALIDATION_COMMANDE') ?> &amp; <?php $plxPlugin->lang('L_CGV') ?></h2>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_thanks_msg"><?php $plxPlugin->lang('L_CONFIG_EMAIL_ORDER_THANKS') ?>&nbsp;(<?php echo $dFlag ?>)&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('thanks_msg',$var['thanks_msg'],'text','0-120') ?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-8 lrg-5 label-centered">
       <label for="id_order_view"><?php $plxPlugin->lang('L_CONFIG_ORDER_VIEW');?>&nbsp;:</label>
      </div>
      <div class="col sml-3 sml-push-1 lgr-push-5">
      <label class="switch switch-left-right">
       <input class="switch-input" id="id_order_view" name="order_view" type="checkbox"<?php echo (!$var['order_view']) ? '' : ' checked="checked"';?> />
       <span class="switch-label" data-on="<?php echo L_YES ?>" data-off="<?php echo L_NO ?>"></span>
       <span class="switch-handle"></span>
      </label>
      </div>
     </div>
     <?php if($var['urlCGV']){ ?>
     <div class="grid">
      <div class="col sml-8 lrg-5 label-centered">
       <label for="id_useLangCGVDefault"><?php $plxPlugin->lang('L_CONFIG_LANGUE_CGV_SYSTEME');?>&nbsp;:</label>
      </div>
      <div class="col sml-3 sml-push-1 lgr-push-5">
       <label class="switch switch-left-right">
        <input class="switch-input" id="id_useLangCGVDefault" name="useLangCGVDefault" type="checkbox"<?php echo (empty($var['useLangCGVDefault'])) ? '' : ' checked="checked"';?> onchange="if (this.checked) { document.getElementById('libCGV').style.display='none';}else{document.getElementById('libCGV').style.display='block';}" />
        <span class="switch-label" data-on="<?php echo L_YES ?>" data-off="<?php echo L_NO ?>"></span>
        <span class="switch-handle"></span>
       </label>
      </div>
     </div>
     <?php } ?>
     <div class="grid" id="libCGV"<?php echo (empty($var['useLangCGVDefault'])) ? '' : ' style="display:none;"';?>>
      <div class="col sml-12 med-5 label-centered">
       <label><?php $plxPlugin->lang('L_CONFIG_LIBELLE_CGV');?>&nbsp;(<?php echo $dFlag ?>)&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <input name="libelleCGV" value="<?php echo $var['libelleCGV']; ?>" type="text">
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12">
       <p class="success"><?php $plxPlugin->lang('L_CONFIG_INFO_CGV'); echo (defined('PLX_MYMULTILINGUE')? '<br />' . $plxPlugin->getLang('L_CONFIG_INFO_CGV_MML') .' ('.$_SESSION['default_lang'].').': ''); ?><br /><?php if($var['urlCGV'])$plxPlugin->lang('L_CONFIG_INFO_CGV_URL'); ?></p>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label><?php $plxPlugin->lang('L_CONFIG_URL_CGV') ?>&nbsp;:</label>
<?php if($var['urlCGV']) {
       $mmlCGV = $var['urlCGV'];
       if($plxPlugin->aLangs AND $_SESSION['default_lang'] != $plxPlugin->dLang) {
        $mmlCGV = $plxPlugin->lang . $mmlCGV;
        $mmlCGV = strtr($mmlCGV, array($plxPlugin->lang.'?' => '?'.$plxPlugin->lang));
       }
       $mmlCGV = $plxAdmin->urlRewrite($mmlCGV);
?>
      <sup><a href="<?php echo $mmlCGV ?>">☍ <?php echo $mmlCGV ?></a></sup>
<?php } ?>
      </div>
      <div class="col sml-12 med-7">
       <input name="urlCGV" value="<?php echo $var['urlCGV']; ?>" placeholder="?static1" type="text" class="success" />
      </div>
     </div>

     <?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName . $baseHook . 'Cgv'));# Hook Plugins plxMyShopConfigCgv ?>

    </div><!-- fi tabpage cgv -->
    <div class="tabpage" id="tabpage_menu">
     <h2><?php $plxPlugin->lang('L_CONFIG_MENU_TITLE') ?></h2>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_menu_position"><?php $plxPlugin->lang('L_CONFIG_MENU_POSITION') ?>&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('menu_position',$var['menu_position'],'number','5-120') ?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_submenu"><?php $plxPlugin->lang('L_CONFIG_SUBMENU'); ?>&nbsp;:&nbsp;(<?php echo $dFlag ?>)</label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('submenu',$var['submenu'],'text','0-120') ?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-8 lrg-5 label-centered">
       <label for="id_afficheLienPanierTop"><?php $plxPlugin->lang('L_CONFIG_AFFICHER_LIEN_PANIER_TOP');?>&nbsp;<img src="<?php echo PLX_PLUGINS; ?>plxMyShop/icon.png" style="vertical-align:top;"></label>
      </div>
      <div class="col sml-3 sml-push-1 lgr-push-5">
       <label class="switch switch-left-right">
        <input class="switch-input" id="id_afficheLienPanierTop" name="afficheLienPanierTop" type="checkbox" <?php echo $var['afficheLienPanierTop'] ? ' checked="checked"' : '';?> />
        <span class="switch-label" data-on="<?php echo L_YES ?>" data-off="<?php echo L_NO ?>"></span>
        <span class="switch-handle"></span>
       </label>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-8 lrg-5 label-centered">
       <label for="id_afficheProductsMenu"><?php echo sprintf($plxPlugin->getLang('L_CONFIG_AFFICHER_PRODUCTS_MENU'), $hLink);?>&nbsp;:</label>
      </div>
      <div class="col sml-3 sml-push-1 lgr-push-5">
       <label class="switch switch-left-right">
        <input class="switch-input" id="id_afficheProductsMenu" name="afficheProductsMenu" type="checkbox" <?php echo $var['afficheProductsMenu'] ? ' checked="checked"' : '';?> />
        <span class="switch-label" data-on="<?php echo L_YES ?>" data-off="<?php echo L_NO ?>"></span>
        <span class="switch-handle"></span>
       </label>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-8 lrg-5 label-centered">
       <label for="id_afficheCategoriesMenu"><?php $plxPlugin->lang('L_CONFIG_AFFICHER_GROUPS_MENU');?>&nbsp;:</label>
      </div>
      <div class="col sml-3 sml-push-1 lgr-push-5">
       <label class="switch switch-left-right">
        <input class="switch-input" id="id_afficheCategoriesMenu" name="afficheCategoriesMenu" type="checkbox" <?php echo ($var['afficheCategoriesMenu']) ? ' checked="checked"' : '';?> />
        <span class="switch-label" data-on="<?php echo L_YES ?>" data-off="<?php echo L_NO ?>"></span>
        <span class="switch-handle"></span>
       </label>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-8 lrg-5 label-centered">
       <label for="id_affichePanierMenu"><?php $plxPlugin->lang('L_CONFIG_AFFICHER_PANIER_MENU');?>&nbsp;:</label>
      </div>
      <div class="col sml-3 sml-push-1 lgr-push-5">
       <label class="switch switch-left-right">
        <input class="switch-input" id="id_affichePanierMenu" name="affichePanierMenu" type="checkbox" <?php echo ($var['affichePanierMenu']) ? ' checked="checked"' : '';?> />
        <span class="switch-label" data-on="<?php echo L_YES ?>" data-off="<?php echo L_NO ?>"></span>
        <span class="switch-handle"></span>
       </label>
      </div>
     </div>

     <?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName . $baseHook . 'Menu'));# Hook Plugins plxMyShopConfigMenu ?>

    </div><!-- fi tabpage menu -->
    <div class="tabpage" id="tabpage_pages">
     <h2><?php $plxPlugin->lang('L_CONFIG_PAGE') ?></h2>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_affPanier"><?php $plxPlugin->lang('L_CONFIG_BASKET_DISPLAY') ?>&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printSelect('affPanier', $tabAffPanier, $var['affPanier']) ?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_name_cart"><?php $plxPlugin->lang('L_CART_NAME_FIELD');?>&nbsp;(<?php $plxPlugin->lang('L_OPTIONEL');?>)&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('name_cart',plxUtils::strCheck($var['name_cart']),'text','255-255'); ?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_template"><?php $plxPlugin->lang('L_CONFIG_PAGE_TEMPLATE') ?>&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printSelect('template', $aTemplates, $var['template']) ?>
      </div>
     </div>
     <div class="grid gridthumb">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_image_none"><?php $plxPlugin->lang('L_NO_IMAGE_CHOICE') ?>&nbsp;(<?php $plxPlugin->lang('L_OPTIONEL');?>)&nbsp; <a title="<?php echo $plxPlugin->lang('L_THUMBNAIL_SELECTION') ?>" id="toggler_thumbnail" href="javascript:void(0)" onclick="mediasManager.openPopup('id_image_none', true, 'id_image_none')" style="outline:none; text-decoration: none"> +</a></label>
       <?php plxUtils::printInput('image_none',plxUtils::strCheck($var['image_none']),'text','255-255',false,'full-width','','onKeyUp="refreshImg(\'id_image_none_img\')"'); ?>
      </div>
      <div class="col sml-12 med-7">
       <div class="image_img" id="id_image_none_img">
<?php
          $imgUrl = PLX_ROOT.$var['image_none'];
          $imgUrl = is_file($imgUrl)?$imgUrl:$imgNoUrl;
?>
        <img src="<?php echo $imgUrl?>" alt="" />
       </div>
      </div>
     </div>
     <!-- Fin du selecteur d'image natif de PluXml -->

     <?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName . $baseHook . 'Pages'));# Hook Plugins plxMyShopConfigPages ?>

    </div><!-- fi tabpage page -->
    <div class="tabpage" id="tabpage_home">
     <h2><?php $plxPlugin->lang('L_PRODUCTS_HOME_PAGE') ?>&nbsp;(<?php echo $dFlag ?>)</h2>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_name_home"><?php $plxPlugin->lang('L_PRODUCTS_NAME_FIELD');?>&nbsp;(<?php $plxPlugin->lang('L_OPTIONEL');?>)&nbsp;:
        <br><?php echo $hLink ?>
       </label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('name_home',plxUtils::strCheck($var['name_home']),'text','255-255'); ?>
      </div>
     </div>
     <div class="grid gridthumb">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_image_home"><?php $plxPlugin->lang('L_PRODUCTS_IMAGE_CHOICE') ?>&nbsp;(<?php $plxPlugin->lang('L_OPTIONEL');?>)&nbsp; <a title="<?php echo $plxPlugin->lang('L_THUMBNAIL_SELECTION') ?>" id="toggler_thumbnail" href="javascript:void(0)" onclick="mediasManager.openPopup('id_image_home', true, 'id_image_home')" style="outline:none; text-decoration: none"> +</a></label>
       <?php plxUtils::printInput('image_home',plxUtils::strCheck($var['image_home']),'text','255-255',false,'full-width','','onKeyUp="refreshImg(\'id_image_home_img'.'\')"'); ?>
      </div>
      <div class="col sml-12 med-7">
       <div class="image_img" id="id_image_home_img">
<?php
          $imgUrl = PLX_ROOT.$var['image_home'];
          $imgUrl = is_file($imgUrl)?$imgUrl:$imgNoUrl;

$plxPlugin->getLang('L_FOR_THIS_CAT').'&nbsp;<i>('.$plxPlugin->getLang('L_PRODUCTS_HOME_PAGE').')</i>'


?>
        <img src="<?php echo $imgUrl?>" alt="" />
       </div>
      </div>
     </div>
     <!-- Fin du selecteur d'image natif de PluXml -->
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_template_home"><?php $plxPlugin->lang('L_PRODUCTS_TEMPLATE_FIELD');?>&nbsp;:
        <br><?php echo $hLink ?>
       </label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printSelect('template_home', $aTemplates, $var['template_home']);?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_chapo_home"><?php echo L_HEADLINE_FIELD ?>&nbsp;(<?php $plxPlugin->lang('L_OPTIONEL');?>)&nbsp;:
        <br><?php echo $hLink ?>
       </label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printArea('chapo_home',plxUtils::strCheck($var['chapo_home']),0,10,false,'full-width');?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_content_home"><?php echo L_CONTENT_FIELD ?>&nbsp;(<?php $plxPlugin->lang('L_OPTIONEL');?>)&nbsp;:
        <br><?php echo $hLink ?>
       </label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printArea('content_home',plxUtils::strCheck($var['content_home']),0,30,false,'full-width');?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_title_htmltag"><?php $plxPlugin->lang('L_CONTENT_OF_TAG');?> "title"&nbsp;(<?php $plxPlugin->lang('L_OPTIONEL');?>)&nbsp;:
        <br><?php echo $hLink ?>
       </label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('title_htmltag',plxUtils::strCheck($var['title_htmltag']),'text','0-255');?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_meta_description"><?php $plxPlugin->lang('L_CONTENT_OF_TAG');?> meta "description"&nbsp;(<?php $plxPlugin->lang('L_OPTIONEL');?>)&nbsp;:
        <br><?php echo $hLink ?>
       </label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('meta_description', plxUtils::strCheck($var['meta_description'])); ?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_meta_keywords"><?php $plxPlugin->lang('L_CONTENT_OF_TAG');?> meta "keywords"&nbsp;(<?php $plxPlugin->lang('L_OPTIONEL');?>)&nbsp;:
        <br><?php echo $hLink ?>
       </label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('meta_keywords', plxUtils::strCheck($var['meta_keywords'])); ?>
      </div>
     </div>

     <?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName . $baseHook . 'Home'));# Hook Plugins plxMyShopConfigHome ?>

    </div><!-- fi tabpage home -->
    <div class="tabpage" id="tabpage_data">
     <h2><?php $plxPlugin->lang('L_CONFIG_DATA_TITLE') ?></h2><?php $placeholder = '" placeholder="data/%s/'; ?>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_command_number"><?php $plxPlugin->lang('L_CONFIG_ORDERS_NUMBER') ?>&nbsp;:</label>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('command_number',$var['command_number'],'number','5-20', false, '" min="0" step="1') ?>
       <?php plxUtils::printInput('command_number_set','','checkbox') ?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_racine_commandes"><?php $plxPlugin->lang('L_CONFIG_ORDERS_FOLDER') ?>&nbsp;:</label>
       <sup><?php $plxPlugin->lang('L_CONFIG_FOLDERS') ?></sup>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('racine_commandes',$var['racine_commandes'],'text','0-120', false, sprintf($placeholder,'commandes')) ?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-12 med-5 label-centered">
       <label for="id_racine_products"><?php $plxPlugin->lang('L_CONFIG_PRODUCTS_FOLDER') ?>&nbsp;:</label>
       <sup><?php $plxPlugin->lang('L_CONFIG_FOLDERS') ?></sup>
      </div>
      <div class="col sml-12 med-7">
       <?php plxUtils::printInput('racine_products',$var['racine_products'],'text','0-120', false, sprintf($placeholder,'products')) ?>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-8 lrg-5 label-centered">
       <label for="id_localStorage"><?php $plxPlugin->lang('L_CONFIG_LOCALSTORAGE');?>&nbsp;:</label>
      </div>
      <div class="col sml-3 sml-push-1 lgr-push-5">
       <label class="switch switch-left-right">
        <input class="switch-input" id="id_localStorage" name="localStorage" type="checkbox" <?php echo (!$var['localStorage']) ? '' : ' checked="checked"';?> />
        <span class="switch-label" data-on="<?php echo L_YES ?>" data-off="<?php echo L_NO ?>"></span>
        <span class="switch-handle"></span>
       </label>
      </div>
     </div>
     <div class="grid">
      <div class="col sml-8 lrg-5 label-centered">
       <label for="id_cookie"><?php $plxPlugin->lang('L_CONFIG_COOKIE');?>&nbsp;:</label>
      </div>
      <div class="col sml-3 sml-push-1 lgr-push-5">
       <label class="switch switch-left-right">
        <input class="switch-input" id="id_cookie" name="cookie" type="checkbox" <?php echo (!$var['cookie']) ? '' : ' checked="checked"';?> />
        <span class="switch-label" data-on="<?php echo L_YES ?>" data-off="<?php echo L_NO ?>"></span>
        <span class="switch-handle"></span>
       </label>
      </div>
     </div>
<?php if($_SESSION['profil'] < PROFIL_MANAGER AND is_readable($parXml)) { ?>
     <div class="grid alert orange">
      <div class="col sml-8 lrg-5 label-centered">
       <label for="id_rezet_ok"><button type="submit" name="rezet" class="red" title="<?php $plxPlugin->lang('L_CONFIG_REZET_TITLE') ?>"><?php $plxPlugin->lang('L_CONFIG_REZET') ?></button></label>
      </div>
      <div class="col sml-3 sml-push-1 lgr-push-5">
       <input name="rezet_back" id="id_rezet_back" type="checkbox" />&nbsp;<?php $plxPlugin->lang('L_CONFIG_REZET_BACK') ?><br />
       <input name="rezet_ok" id="id_rezet_ok" type="checkbox" />&nbsp;<?php $plxPlugin->lang('L_CONFIG_REZET_OK') ?>
      </div>
     </div>
<?php } ?>

     <?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName . $baseHook . 'Data'));# Hook Plugins plxMyShopConfigData ?>

    </div><!-- fi tabpage data -->

    <?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName . $baseHook . 'FormEnd'));# Hook Plugins plxMyShopConfigFormEnd ?>

   </fieldset>
  </div><!-- tabscontent -->
 </div><!-- fi tabContainer -->
</form>
<!-- Utilisation du selecteur d'image natif à PluXml -->
<script type="text/javascript">
 function refreshImg(id_img) {
  var dta = document.getElementById(id_img.replace('_img','')).value;
  if(dta.trim()==='') {
   document.getElementById(id_img).innerHTML = '';//'<img src="<?php #echo $imgNoUrl ?>" alt="" />';
  } else {
   var link = dta.match(/^(https?:\/\/[^\s]+)/gi) ? dta : '<?php echo $plxAdmin->racine ?>'+dta;
   document.getElementById(id_img).innerHTML = '<img src="'+link+'" alt="" />';
  }
 }
</script>
<script type="text/javascript" src="<?php echo PLX_PLUGINS.$plxPlugin->plugName.'/js/tabs.js?v='.$plxPlugin::V ?>"></script>
<?php
eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName . $baseHook . 'End'));# Hook Plugins plxMyShopConfigEnd
$plxPlugin->tips();