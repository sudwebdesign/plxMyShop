<?php if (!defined('PLX_ROOT')) exit;
/**
 * Plugin plxMyShop
 * Compatible urlRewrite & Multilingue
 * @author David L, Thomas Ingles
 **/
class plxMyShop extends plxPlugin {
 const V = '1.0.4';#$this->getInfo('version') is empty in public mode
 static $imageNo = PLX_PLUGINS.__CLASS__.'/images/none.png';
 # si PLX_DEBUG est vrai + profil superieur au manager (s'active)
 static $PLX_DEBUG_MYSHOP = FALSE;
 public $plugName;
 public $aProds = array(); # Tableau de tous les produits
 public $donneesModeles = array();
 public $plxMotor;
 public $cheminImages;
 public $idProduit;
 public $shortcode = 'boutonPanier';
 public $shortcodeactif = 0;#false
 public $shipOverload = false;
 public $dLang = '';
 # plxMyMultilingue
 static $mml = 0;
 static $mmlC = array(true);
 static $mmlFlags = array();
 public $lang = '';#sous dossier mml fr/
 public $aLangs = false;

 public function tips() {# Contribute
  include (PLX_PLUGINS.__CLASS__.'/lib/tips.inc.php');
 }
 public function onUpdate(){#mise a jour du cache des css &+
  if(!empty($this->getParam('template')) AND empty($this->getParam('template_home_' . $this->dLang))){#up to 1.0.x
   $lng = '_'.$this->dLang;#MyMultiLingue ou non
   $this->commandNumber($this->totalCommandFiles(), true);# Sur une idée de garys02
   $this->setParam('position_admin', 5, 'numeric');
   $this->setParam('delivery_firstday', 1, 'numeric');
   $this->setParam('template_home' . $lng, $this->getParam('template'), 'string');#static.php
   $this->setParam('afficheCategoriesMenu ', intval('non'!=$this->getParam('afficheCategoriesMenu')), 'numeric');
   $this->setParam('affichePanierMenu', intval('non'!=$this->getParam('affichePanierMenu')), 'numeric');
   $this->setParam('affichePanierMenu', intval('non'!=$this->getParam('affichePanierMenu')), 'numeric');
   $this->setParam('submenu' . $lng, trim($this->getParam('submenu')), 'string');
   unset($this->aParams['submenu']);
   $this->setParam('devise' . $lng, trim($this->getParam('devise')), 'string');
   unset($this->aParams['devise']);
   $this->setParam('position_devise' . $lng, intval($this->getParam('position_devise')!='after'), 'numeric');# 0:after 1:before
   unset($this->aParams['position_devise']);
   $this->setParam('subject' . $lng, trim($this->getParam('subject')), 'string');#L_DEFAULT_OBJECT
   unset($this->aParams['subject']);
   $this->setParam('newsubject' . $lng, trim($this->getParam('newsubject')), 'string');#L_EMAIL_SUBJECT
   unset($this->aParams['newsubject']);
   $this->setParam('name_cart' . $lng, trim($this->getParam('name_cart')), 'string');#L_PUBLIC_BASKET
   unset($this->aParams['name_cart']);
   unset($this->aParams['shipping_ups']);#
   unset($this->aParams['shipping_tnt']);#
   #C.G.V, T&C
   if(isset($this->aParams['libelleCGV'])){
    $this->setParam('libelleCGV_' . $this->dLang, trim($this->getParam('libelleCGV')), 'string');
    unset($this->aParams['libelleCGV']);
   }
   $this->setParam('delivery_nb_timeslot', (60 * intval($this->getParam('delivery_nb_timeslot'))), 'numeric');#1.0.0 en minutes
   if(!defined('L_SAVE_SUCCESSFUL')) @define('L_SAVE_SUCCESSFUL', __CLASS__ .' &gt;&gt;&gt; '.self::V.' ✔');
   include_once(PLX_CORE.'/lib/class.plx.msg.php');
   @$this->saveParams();# @ hide little error for public mode
  }#1.0.0
  return array('cssCache' => true);
 }
 /**
  * renvoi un param de $this->plug[] (protected)
  * Scope : config (backup parametre.xml avant RAZ)
  * @param	param : dir * name * filename * parameters.xml * infos.xml
  * @return	string ou false
  * @author	Thomas Ingles
  **/
 public function getPlugInfo($param='') {
  return (!empty($param) AND defined('PLX_ADMIN') AND isset($this->plug[$param]))? $this->plug[$param]:false;
 }

 public function __construct($default_lang){
  if(defined('PLX_MYMULTILINGUE')) {# Si plugin plxMyMultilingue présent
   self::$mmlC[self::$mml] = false;# verif si plxMyMultilingue est chargé avant moi
   $lang = plxMyMultiLingue::_Lang();# récupération de la langue en cours
   if(!empty($lang)) {
    if(isset($_SESSION['default_lang']) AND $_SESSION['default_lang']!=$lang) {
     $this->lang = $lang.DIRECTORY_SEPARATOR;
     #$this->default_lang = $default_lang = $lang;# Langue Dynamique
    }
   }
   $lang = plxMyMultiLingue::_Langs();# récupération du tableau des langues activées
   $this->aLangs = empty($lang) ? $this->aLangs : explode(',', $lang);
  }
  self::$mml++;# instance to check if MyMultilingue is loaded before me (i'm called twice mml 0.9.0)
  # If twice == false == ok, but not : show error msg
  if(isset(self::$mmlC[1]) AND !self::$mmlC[1] AND self::$mmlC[0]){#it's this?
   plxMsg::error($this->getLang('L_LOAD_MULTILINGUE_BEFORE') . ' <i>' . __CLASS__ . '</i>');
  }

  # appel du constructeur de la classe plxPlugin (obligatoire)
  parent::__construct($default_lang);

  if(defined('PLX_AUTHPAGE')) return;

  $this->plugName = __CLASS__;# $this->plug['name'];# or get_class($this);
  # Accès au menu admin réservé au profil administrateur et gestionnaire
  $this->setAdminProfil(PROFIL_ADMIN, PROFIL_MANAGER);
  # droits pour accèder à la page config.php du plugin
  $this->setConfigProfil(PROFIL_ADMIN);
  # Personnalisation du menu admin
  $this->setAdminMenu(
   (empty($this->getParam('shop_name'))? __CLASS__: $this->getParam('shop_name'))
   , intval($this->getParam('position_admin'))
   , $this->getLang('L_ADMIN_MENU_TOOTIP') . ' v' . self::V
  );

  #Si debug de pluxml actif et aux admins, on affiche les erreurs des méls +leurs contenus puis la commande,et on stoppe a la fin au lieu de redirigé
  self::$PLX_DEBUG_MYSHOP = (defined('PLX_DEBUG') AND PLX_DEBUG AND isset($_SESSION['profil']) AND $_SESSION['profil'] < PROFIL_MODERATOR);

# Hook PluXml : core/lib/class.plx.motor.php
  $this->addHook('plxMotorConstruct', 'plxMotorConstruct');#getProducts() + $cheminImages
  $this->addHook('plxMotorPreChauffageBegin', 'plxMotorPreChauffageBegin');
# Hook plxAdminBar >= 2.2.4
  $this->addHook('AdminBarNewContent', 'AdminBarNewContent');# Affiche les liens Créer Produits/Groupes ds plxAdminBar
  $this->addHook('AdminBarAppearance', 'AdminBarAppearance');# Affiche le lien pour gérer la boutique ds plxAdminBar
  if(defined('PLX_ADMIN')) {#Déclaration des hooks pour la zone d'administration
   $this->addHook('AdminTopBottom', 'AdminTopBottom');
   $this->addHook('AdminTopEndHead', 'AdminTopEndHead');
  }
  else{#Déclaration des hooks pour la partie
# Hook plxAdminBar >= 2.2.4
   $this->addHook('AdminBarBegin', 'AdminBarBegin');# Affiche le lien : Modifier ce Produit/Groupe ds plxAdminBar
# Hook PluXml
   $this->addHook('plxMotorParseArticle', 'plxMotorParseArticle');# 4 shortcode in article [boutonPanier ###]
   $this->addHook('plxShowStaticListEnd', 'plxShowStaticListEnd');
   $this->addHook('plxShowConstruct', 'plxShowConstruct');
   $this->addHook('plxShowMeta', 'plxShowMeta');
   $this->addHook('plxShowPageTitle', 'plxShowPageTitle');
   $this->addHook('plxShowStaticContent', 'plxShowStaticContent');
   $this->addHook('SitemapStatics', 'SitemapStatics');
   $this->addHook('ThemeEndBody', 'ThemeEndBody');
   $this->addHook('ThemeEndHead', 'ThemeEndHead');
# Hooks plxMyShop
   $this->addHook('plxMyShopEditProductBegin', 'changeStock');
   $this->addHook('plxMyShopShippingMethod', 'plxMyShopShippingMethod');
   $this->addHook('plxMyShopShowMiniPanier', 'plxMyShopShowMiniPanier');
# Panier si produit(s) dedans ### AND !empty($_SESSION[__CLASS__]['ncart']) ???
   if(!empty($_SESSION[__CLASS__]['prods']) OR !empty($_COOKIE[__CLASS__])){#[$this->dLang] #dLang == '' ici (Est chargé par plxMotorConstruct)
    $this->addHook('plxMyShopPanierFin', 'inlineBasketJs');
    if($this->getParam('delivery_date')){
     $this->addHook('plxMyShopPanierFin', 'inlineDeliverydateJs');
     $this->addHook('ThemeEndHead', 'themeEndHeadDeliverydateJs');
    }
    if($this->getParam('localStorage')){#MyshopCookie
     $this->addHook('plxMyShopPanierCoordsMilieu', 'inlineLocalStorageHtml');
     $this->addHook('plxMyShopPanierFin', 'inlineLocalStorageJs');
    }
    if($this->getParam('cookie')){#MyshopCookie
     $this->addHook('Index', 'Index');
     $this->addHook('IndexEnd', 'IndexEnd');
    }
   }
  }
# Ajout de variables non protégé facilement accessible via $(plxShow->)plxMotor->plxPlugins->aPlugins['plxMyShop']->aConf['racine_XXX'] dans les themes ou dans d'autres plugins.
  $this->aConf['racine_products'] = (!$this->getParam('racine_products')?'data/products/':$this->getParam('racine_products'));
  $this->aConf['racine_commandes'] = (!$this->getParam('racine_commandes')?'data/commandes/':$this->getParam('racine_commandes'));
#  $this->getProducts();#Mntnnt ds plxMotorConstruct hook : &$this (plxMotor) #v1.0.0
  if (!is_dir(PLX_ROOT.$this->aConf['racine_commandes'])){
   mkdir(PLX_ROOT.$this->aConf['racine_commandes'], 0755, true);
  }
  if (!is_file(PLX_ROOT.$this->aConf['racine_commandes'].'index.html')){
   $mescommandeindex = fopen(PLX_ROOT.$this->aConf['racine_commandes'].'index.html', 'w+');
   fclose($mescommandeindex);
  }
# Créer les dossiers de sauvegarde si MyMultilingue
  if($this->aLangs){
   foreach ($this->aLangs as $lang){
    if (!is_dir(PLX_ROOT.$this->aConf['racine_commandes'].$lang.'/')){
     mkdir(PLX_ROOT.$this->aConf['racine_commandes'].$lang.'/', 0755, true);
    }
    if (!is_file(PLX_ROOT.$this->aConf['racine_commandes'].$lang.'/index.html')){
     $mescommandeindex = fopen(PLX_ROOT.$this->aConf['racine_commandes'].$lang.'/index.html', 'w+');
     fclose($mescommandeindex);
    }
   }
  }
# méthodes de paiement
  $tabMethodespaiement = array(
   'cheque' => array(
    'libelle' => $this->getLang('L_PAYMENT_CHEQUE') ,
    'codeOption' => 'payment_cheque',
   ),
   'cash' => array(
    'libelle' => $this->getLang('L_PAYMENT_CASH') ,
    'codeOption' => 'payment_cash',
   ),
  );

  $tabChoixMethodespaiement = array();
  foreach ($tabMethodespaiement as $codeMethodespaiement => $m){
   if ('1' === $this->getParam($m['codeOption'])){
    $tabChoixMethodespaiement[$codeMethodespaiement] = $m;
   }
  }
  $this->donneesModeles['tabChoixMethodespaiement'] = $tabChoixMethodespaiement;
 }

 /**
 * hook admin-bar.php de plxAdminBar(>=2.2.4) qui affiche le lien
 * pour éditer un produit/groupe selon le mode en cours
 **/
 public function AdminBarBegin() {
  $lng = '_'.$this->dLang;#MyMultiLingue
  if($this->plxMotor->mode == 'product') {
    $isCat = $this->aProds[$this->dLang][$this->idProduit]['pcat'];
    $isCat = !$isCat?'L_PRODUCT':'L_GROUP';
echo '<?php '; ?>
    $langA = array_merge($langA, array('product'=>'<?php $this->lang($isCat)?>'));
    $langT = array_merge($langT, array('product'=>'<?php $this->lang($isCat.'_TITLE')?>'));
    $aModes = array_merge($aModes, array('product'));
    $cible='<?= __CLASS__ ?>&amp;prod=<?php echo $this->idProduit ?>';# .$plxAdmin->plxPlugins->aPlugins['<?= __CLASS__ ?>']->idProduit;
    $plxAdmin_url = 'plugin';#.php?p= + $cible === # (core/admin/)plugin.php?p=<?= __CLASS__ ?>&amp;prod=002
?><?php
  }
 }

 /**
 * hook admin-bar.php de plxAdminBar(>=2.2.4) qui
 * affiche les liens pour Créer un produit/groupe
 **/
 public function AdminBarNewContent() {
  if($_SESSION['profil'] <= PROFIL_MANAGER){
?>
          <li id="plx-admin-bar-new-product"><a class="ab-item" href="<?='<?= $BASE_URL ?>'?>plugin.php?<?='<?= $pmmUrl ?>'?>p=<?= __CLASS__ ?>" title="<?php $this->lang('L_NEW_PRODUCT') ?> - <?= __CLASS__ ?>"><?php $this->lang('L_PRODUCTS') ?></a></li>
          <li id="plx-admin-bar-new-group"><a class="ab-item" href="<?='<?= $BASE_URL ?>'?>plugin.php?<?='<?= $pmmUrl ?>'?>p=<?= __CLASS__ ?>&amp;mod=cat" title="<?php $this->lang('L_NEW_CATEGORY') ?> - <?= __CLASS__ ?>"><?php $this->lang('L_GROUPS') ?></a></li>
<?php
  }#FI PROFIL_MANAGER
 }

 /**
 * hook admin-bar.php de plxAdminBar(>=2.2.4) qui
 * affiche les liens pour gérer les Params de la boutique
 **/
 public function AdminBarAppearance() {
  if($_SESSION['profil'] <= PROFIL_MANAGER){
?>
       <li id="plx-admin-bar-parametres-<?= __CLASS__ ?>"><a class="ab-item" href="<?='<?= $BASE_URL ?>'?>parametres_plugin.php?<?='<?= $pmmUrl ?>'?>p=<?= __CLASS__ ?>" title="<?php $this->lang('L_MENU_CONFIG') ?> - <?= __CLASS__ . ' v' . self::V ?>"><?php $this->lang('L_MENU_CONFIG') ?> - <?= __CLASS__ ?></a></li>
<?php
  }#FI PROFIL_MANAGER
 }

 /**
 * hook plxShow->pageTitle($format='',$sep=';')
 * Méthode qui affiche le titre de la page selon le mode
 **/
 public function plxShowPageTitle() {
  $lng = '_'.$this->dLang;#MyMultiLingue
  if($this->plxMotor->mode == 'product') {
   echo '<?php '; ?>
    $aProd = $this->plxMotor->plxPlugins->aPlugins['<?php echo __CLASS__?>']->aProds[ '<?php echo $this->dLang?>' ][ '<?php echo $this->idProduit?>' ];#langue de session (a ajouté & ailleurs i think)
    $title_htmltag =  $aProd['title_htmltag'];
    $title = $title_htmltag !='' ? $title_htmltag : $aProd['name'];
    $subtitle = $this->plxMotor->aConf['title'];

    $fmt = '';
    if(preg_match('/'.$this->plxMotor->mode.'\s*=\s*(.*?)\s*('.$sep.'|$)/i',$format,$capture)) {
     $fmt = trim($capture[1]);
    }

    $format = $fmt=='' ? '#title - #subtitle' : $fmt;
    $txt = str_replace('#title', trim($title), $format);
    $txt = str_replace('#subtitle', trim($subtitle), $txt);
    echo plxUtils::strCheck(trim($txt, ' - '));
    return true;#stop hooked func
?><?php
  }
  elseif($this->plxMotor->mode == 'products') {#home page products/stars
   echo ($this->getParam('name_home'.$lng)? $this->getParam('name_home'.$lng): $this->getLang('L_PRODUCTS_HOME_PAGE')).' - ';
  }
  elseif($this->plxMotor->mode == 'boutique') {#panier
   echo ($this->getParam('name_cart'.$lng)? $this->getParam('name_cart'.$lng): $this->getLang('L_PUBLIC_BASKET')).' - ';
  }
 }

 /**
 * hook plxMotor->meta($meta='')
 * Méthode qui affiche le meta passé en paramètre
 **/
 public function plxShowMeta() {
  if($this->plxMotor->mode == 'products' OR $this->plxMotor->mode == 'product') {
   $lng = '_'.$this->dLang;#MyMultiLingue
   echo '<?php '; ?>
    if($this->plxMotor->mode == 'products'){# plxMyShop Home Page
     if(!empty($metashow = $this->plxMotor->plxPlugins->aPlugins['<?php echo __CLASS__?>']->getParam('meta_'.$meta.'<?php echo $lng ?>'))){
      echo '	<meta name="'.$meta.'" content="'.plxUtils::strCheck($metashow).'" />'.PHP_EOL;
      return true;#stop hooked func
     }
    }else{
     $aProd = $this->plxMotor->plxPlugins->aPlugins['<?php echo __CLASS__?>']->aProds[ '<?php echo $this->dLang?>' ][ '<?php echo $this->idProduit?>' ];
     if(!empty($aProd['meta_'.$meta])){
      echo '	<meta name="'.$meta.'" content="'.plxUtils::strCheck($aProd['meta_'.$meta]).'" />'.PHP_EOL;
      return true;#stop hooked func
     }
#     elseif(!empty($this->plxMotor->aConf['meta_'.$meta]))
#      echo '	<meta name="'.$meta.'" content="'.plxUtils::strCheck($this->plxMotor->aConf['meta_'.$meta]).'" />'.PHP_EOL;
    }
#    return true;#stop hooked func
?><?php
  }
 }

 /**
 * Méthode d'ajout des <link rel="alternate"... sur les pages multilingues de toutes les langues disponible(s)
 *
 **/
 public function ThemeEndHead() {
  if($this->aLangs) {
   switch($this->plxMotor->mode) {
    case 'boutique':
    case 'products':
    case 'product':
     $url = $this->plxMotor->get;#defaut
     foreach($this->aLangs as $k=>$v) {
      #if($this->dLang == $v) continue;# Cache la langue consultée. Non, doit y être : moz.com/learn/seo/hreflang-tag
      $lng = ($_SESSION['default_lang']!=$v)?$v.'/':'';
      if($this->idProduit
       AND isset($this->aProds[$v][$this->idProduit])
       AND $this->aProds[$v][$this->idProduit]['active']
       #AND $this->aProds[$v][$this->idProduit]['readable']# ?
      )
       $url = 'product' . intval($this->idProduit) . '/' . $this->aProds[$v][$this->idProduit]['url'];
      else if($this->plxMotor->mode == 'product')
       continue;
      ?>
	<link rel="alternate" hreflang="<?=$v?>" href="<?='<?=$plxMotor->urlRewrite("?'.$lng.$url.'")?>'?>" />
<?php
     }
     break;
    default:break;
   }
  }
 }
 /**
 * Méthode d'ajout du lien mon panier sur les pages
 *
 **/
 public function plxShowLienPanierTop(){
  if ($this->getParam('afficheLienPanierTop')) {
   $lng = '_'.$this->dLang;#MyMultiLingue
   $name = ($this->getParam('name_cart'.$lng)? $this->getParam('name_cart'.$lng): $this->getLang('L_PUBLIC_BASKET'));
   $lienPanier = isset($this->donneesModeles['lienPanier'])? $this->donneesModeles['lienPanier']: $this->plxMotor->urlRewrite('?boutique/panier#panier');
?>
  <div class="basket_link_image">
   <a href="<?php echo htmlspecialchars($lienPanier);?>" id="notiShoppingCart">
   <span id="notiNumShoppingCart"></span><img src="<?php echo PLX_PLUGINS.__CLASS__; ?>/icon.png">&nbsp;<?php echo $name ?></a>
  </div>
<?php
  }
 }
 /**
 * Méthode qui charge le code css nécessaire à la gestion de onglet dans l'écran de configuration du plugin
 *
 * @return    stdio
 * @author    Stephane F, Thomas I.
 **/
 public function AdminTopEndHead() {
  $pg = rtrim(basename($_SERVER['SCRIPT_NAME']), '.php');
  if (strpos($pg,'plugin')!==FALSE && isset($_GET['p']) && $_GET['p']==__CLASS__) {
   switch($pg){
    case 'parametres_plugin':
     echo '<link rel="stylesheet" type="text/css" href="'.PLX_PLUGINS.__CLASS__.'/css/tabs.css?v='.self::V.'" />'.PHP_EOL;
    case 'plugin':
     if($this->aLangs)
      echo '<link rel="stylesheet" type="text/css" href="'.PLX_PLUGINS.__CLASS__.'/css/tabs.css?v='.self::V.'" />'.PHP_EOL;
     echo '<link rel="stylesheet" type="text/css" href="'.PLX_PLUGINS.__CLASS__.'/css/administration.css?v='.self::V.'" />'.PHP_EOL;
     echo '<noscript><style>.hide{display:inherit !important;}</style></noscript>'.PHP_EOL;
     echo '<?php '; ?>
     if ((isset($plxAdmin->version) && version_compare($plxAdmin->version, '5.3.1', '<=')))#$plxMotor/$plxAdmin->version removed in 5.5
      echo '<link rel="stylesheet" type="text/css" href="'.PLX_PLUGINS.'<?php echo __CLASS__ ?>/css/5.6.css" />'.PHP_EOL;
?><?php
     break;
    default:
   }
  }
 }
 public function plxMyShopShowMiniPanier(){
  $class = $this->plxMotor->get=='boutique/panier'?'active':'noactive';
?>
  <h3 class="mybasket <?php echo ($class=='active'?' red':''); ?>">
   <span><img class="icon" src="<?php echo PLX_PLUGINS.__CLASS__; ?>/icon.png"></span>&nbsp;<?php $this->lang('L_PUBLIC_BASKET'); ?></h3>
<?php
  //~ if (isset($_SESSION[__CLASS__]['ncart'][$this->dLang]) && $_SESSION[__CLASS__]['ncart'][$this->dLang]>0 && !empty($_SESSION[__CLASS__]['prods'][$this->dLang])){
  if (!empty($_SESSION[__CLASS__]['ncart'][$this->dLang]) AND !empty($_SESSION[__CLASS__]['ncart'][$this->dLang])){
   echo '<ul class="cat-list unstyled-list mybasket">'.PHP_EOL;
   foreach($_SESSION[__CLASS__]['prods'][$this->dLang] as $k => $v){
    echo '<li>
           <form method="POST" id="FormRemProd'.$k.'" class="formRemProd">
            <input type="hidden" name="idP" value="'.htmlspecialchars($k).'" />
            <sub><input class="miniDel badge red" type="submit" id="remProd'.$k.'" name="remProd" value="-" title="'.$this->getLang('L_PUBLIC_DEL_BASKET').'"/></sub>
           </form>
           <a href="'.$this->productRUrl($k).'">'.$this->aProds[$this->dLang][$k]['name'].'</a><sup><span class="badge">'.$v.'</span></sup></li>'.PHP_EOL;
   }
   echo '</ul>
   <p>'.($class!='active'?'<a class="button blue" href="'.$this->donneesModeles['lienPanier'].'" title="'.$this->getLang('L_PUBLIC_BASKET_MINI_TITLE').'">'.$this->getLang('L_PUBLIC_BASKET_MINI').'</a>':'').'</p>'.PHP_EOL;
  }else{
   echo '<ul class="lastart-list unstyled-list"><li><em>'.$this->getLang('L_PUBLIC_NOPRODUCT').'</em></li></ul>';
  }
 }
 public function ThemeEndBody(){
  echo '<?php if($plxMotor->mode == "product" || $plxMotor->mode == "products" || strstr($plxMotor->template,"boutique") || $plxMotor->plxPlugins->aPlugins["'.__CLASS__.'"]->shortcodeactif){ ?>';
#javascript de bascule des boutons produits
?>
<script type="text/javascript">function chngNbProd(e,t){var a=document.getElementById('addProd'+e),d=document.getElementById('nbProd'+e);'<?php echo plxUtils::strCheck($this->getLang('L_PUBLIC_ADD_BASKET')); ?>'!=a.value&&(d.value==d.getAttribute('data-o')||0==d.value?(t&&(d.value='0'),a.value='<?php echo plxUtils::strCheck($this->getLang('L_PUBLIC_DEL_BASKET')); ?>',a.setAttribute('class','red')):(a.value='<?php echo plxUtils::strCheck($this->getLang('L_PUBLIC_MOD_BASKET')); ?>',a.setAttribute('class','orange')))}</script>
<?php
  echo '<?php } ?>';# fi if mode product || strstr template boutique || shortcode
  if (isset($_SESSION[__CLASS__]['msgProdUpDate']) && $_SESSION[__CLASS__]['msgProdUpDate']){
   unset($_SESSION[__CLASS__]['msgProdUpDate']);
#Les messages de MAJ panier
?>
<div id="msgUpDateCart"><?php ((isset($_SESSION[__CLASS__]['prods'][$this->dLang]) && $_SESSION[__CLASS__]['prods'][$this->dLang])?$this->lang('L_PUBLIC_MSG_BASKET_UP'):$this->lang('L_PUBLIC_NOPRODUCT')); ?></div>
<script type="text/javascript">
 var msgUpDateCart = document.getElementById('msgUpDateCart');
 msgUpDateCart.style.display = 'block';
 setTimeout(function(){document.getElementById('msgUpDateCart').style.display = 'none'; }, 3000);
 //~ var shoppingCart = null;//why here?
</script>
<?php }# fi Les messages de MAJ panier
 }#end ThemeEndBody
 public function IndexEnd(){#MyshopCookie : Garde la session ds le cookie
# localhost pour test ou véritable domaine ?
  $cookie_domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
  $cookie_path = '/';
  $cookie_secure = 0;
  $cookie_value = array();
  $json = version_compare(PHP_VERSION, '5.2.0', '>=');
# Durée de vie cookie = fin de session par défaut
  $cookie_expire = 0;
# Durée de vie du cookie = 2 mois si au moins un produit dans le panier
  if(!empty($_SESSION[__CLASS__]['ncart'][$this->dLang]))
   $cookie_expire = time() + 3600 * 24 * 30 * 2;#60 jours
  if(!empty($_SESSION[__CLASS__]['prods'][$this->dLang])){
   $cookie_value['prods'][$this->dLang]=preg_replace('/[^\d]/','',$_SESSION[__CLASS__]['prods'][$this->dLang]);
   $cookie_value['ncart'][$this->dLang]=intval($_SESSION[__CLASS__]['ncart'][$this->dLang]);
  }else{
   if($json)
    $cookie_val = json_decode($_COOKIE[__CLASS__],true);
   else
    $cookie_val = unserialize($_COOKIE[__CLASS__]);
   if(empty($cookie_val)) return;#si déjà vide on stoppe
  }
  if($json)
   setcookie(__CLASS__, json_encode($cookie_value), $cookie_expire, $cookie_path, $cookie_domain, $cookie_secure, true);
  else
   setcookie(__CLASS__, serialize($cookie_value), $cookie_expire, $cookie_path.'; HttpOnly', $cookie_domain, $cookie_secure);
 }
 public function Index(){#MyshopCookie : Recharge la session
  # MyShopCookie Index hook
  if(!empty($_COOKIE[__CLASS__]) && !isset($_SESSION['IS_NOT_NEW'])) {
   if (version_compare(PHP_VERSION, '5.2.0', '>='))
    $cookie_value = json_decode($_COOKIE[__CLASS__],true);
   else
    $cookie_value = unserialize($_COOKIE[__CLASS__]);

   if(!empty($cookie_value['prods'])) {
    # v1.0.0 multilingue, 1 panier / langue
    if(isset($cookie_value['prods'][$this->dLang])) {
     $_SESSION[__CLASS__]['prods'][$this->dLang] =
      preg_replace('~[^\d]~','',$cookie_value['prods'][$this->dLang]);#Juste les nombres
     $_SESSION[__CLASS__]['ncart'][$this->dLang] =
      intval($cookie_value['ncart'][$this->dLang]);
    }
    elseif(is_int($cookie_value['ncart'])){# !is_array() Retro Compat 0.13.2
     $_SESSION[__CLASS__]['prods'][$this->dLang] =
      preg_replace('~[^\d]~','',$cookie_value['prods']);#Juste numérique
     $_SESSION[__CLASS__]['ncart'][$this->dLang] =
      intval($cookie_value['ncart']);
    }
   }
  }
  $_SESSION['IS_NOT_NEW']=true;
 }
#hook des boutons localStorage du formulaire pour les clients au milieu du Panier
 public function inlineLocalStorageHtml(){#MyshopCookie ?>
    <p><span id="bouton_sauvegarder">&nbsp;</span>&nbsp;<span id="bouton_effacer">&nbsp;</span>&nbsp;<span id="bouton_raz">&nbsp;</span></p>
    <p id="alerte_sauvegarder" class="alert green" style="transition-duration:1s;display:none;">&nbsp;</p>
<?php
}
#hook js localStorage du formulaire pour les clients à la fin du Panier
 public function inlineLocalStorageJs(){#MyshopCookie ?>
<script type="text/JavaScript">
const l_address_saved = '<?php echo plxUtils::strCheck($this->getLang('L_ADDRESS_SAVED')); ?><br />(<?php echo plxUtils::strCheck($this->getLang('L_DO_NOT_SHARED')); ?>)';
const l_address_deleted = '<?php echo plxUtils::strCheck($this->getLang('L_ADDRESS_DELETED')); ?>';
const l_save_my_address = '<?php echo plxUtils::strCheck($this->getLang('L_SAVE_MY_ADDRESS')); ?>';
const l_delete_my_address = '<?php echo plxUtils::strCheck($this->getLang('L_DELETE_MY_ADDRESS')); ?>';
const l_reset_address = '<?php echo plxUtils::strCheck($this->getLang('L_RESET_ADDRESS')); ?>';
</script>
<script type="text/JavaScript" src="<?=PLX_PLUGINS.__CLASS__?>/js/localstorage.js?v=<?=self::V?>"></script>
<?php
 }
#hook js à la fin du Panier #toujours
 public function inlineBasketJs(){ ?>
<script type="text/JavaScript">
ShowMyForm=<?='<?=intval($nprod)?>'?>;
delivery=<?=intval($this->getParam('delivery_date'))?>;
</script>
<script type="text/javascript" src="<?=PLX_PLUGINS.__CLASS__;?>/js/panier.js?v=<?=self::V?>"></script>
<?php
  if(isset($_SESSION[__CLASS__]['errorPost'])){
?>
<script type="text/JavaScript">
 const conteneurNomCadeau=document.getElementById('conteneurNomCadeau');
 emailCart.value='<?php echo $_SESSION[__CLASS__]['errorPost']['email']; ?>';
 firstnameCart.value="<?php echo trim(json_encode($_SESSION[__CLASS__]['errorPost']['firstname']), '"') ?>";
 lastnameCart.value="<?php echo trim(json_encode($_SESSION[__CLASS__]['errorPost']['lastname']), '"') ?>";
 adressCart.value="<?php echo trim(json_encode($_SESSION[__CLASS__]['errorPost']['adress']), '"') ?>";
 postcodeCart.value="<?php echo trim(json_encode($_SESSION[__CLASS__]['errorPost']['postcode']), '"') ?>";
 cityCart.value="<?php echo trim(json_encode($_SESSION[__CLASS__]['errorPost']['city']), '"') ?>";
 countryCart.value="<?php echo trim(json_encode($_SESSION[__CLASS__]['errorPost']['country']), '"') ?>";
 telCart.value="<?php echo trim(json_encode($_SESSION[__CLASS__]['errorPost']['tel']), '"') ?>";
 nomCadeau.value="<?php echo trim(json_encode($_SESSION[__CLASS__]['errorPost']['nomCadeau']), '"') ?>";
 choixCadeau.checked=!!nomCadeau.value;
 conteneurNomCadeau.style.display=nomCadeau.value?'initial':'';
 msgCart.value="<?php echo trim(json_encode($_SESSION[__CLASS__]['errorPost']['msg']), '"') ?>";
<?php if($this->getParam('delivery_date')){?>
 deliverydate.value="<?php echo trim(json_encode($_SESSION[__CLASS__]['errorPost']['deliverydate']), '"') ?>";
 delivery_date.value="<?php echo trim(json_encode($_SESSION[__CLASS__]['errorPost']['delivery_date']), '"') ?>";
 delivery_interval.value="<?php echo trim(json_encode($_SESSION[__CLASS__]['errorPost']['delivery_interval']), '"') ?>";
<?php }#Fi delivery_date?>
</script>
<?php
   unset($_SESSION[__CLASS__]['errorPost']);
  }# fi errorPst
 }
#hook js à la fin du Panier #Choix de date/heure de livraison #pikaday
 public function inlineDeliverydateJs(){
  #disallowed Dates
  $dDates = array();
  $disallowedDates = $this->getParam('delivery_disallowed_dates');
  $disallowedDates = explode(',', $disallowedDates);
  foreach($disallowedDates AS $dDate){
   $dDate = explode('_', trim($dDate));#Found range by _
   if(isset($dDate[1])){#create range days
    $dFrom = explode('-',$dDate[0]);#YYYY MM DD
    $dTo = explode('-',$dDate[1]);#YYYY MM DD
    $year = $dFrom[0];
    $month = $dFrom[1];
    $day = $dFrom[2];
    while($year <= $dTo[0]){
     while($month <= 12){
      while($day <= 31){
       $dDay = $year.'-'.str_pad($month, 2, '0', STR_PAD_LEFT).'-'.str_pad($day, 2, '0', STR_PAD_LEFT);
       $dDates[] = $dDay;
       if($dDay == $dDate[1]){#day == last day
        break(2);
       }
       $day++;
       #checkdate ( int $month , int $day , int $year )
       if(!checkdate($month, $day ,$year)){
        $day = 1;
        break;
       }
      }
      $day = 1;
      $month++;
      if($month > 12){
       $month = 1;
       break;
      }
     }
     $year++;
    }
   }else{#classic (one day)
    $dDates[] = $dDate[0];
   }
  }#hcaerof
  $disallowedDates = array();#clear
  foreach($dDates AS $dDate){
   $disallowedDates[] = $dDate;#date + range
  }
  $disallowedDates = "['" . implode("','", $disallowedDates) . "']";
  #disalowed days of week
  $delivery_day = $this->getParam('delivery_day');#v0.13.2
  $delivery_day_js = '';
  if(!!$delivery_day AND $delivery_day != '1,1,1,1,1,1,1'){#weekday (if 1 or more not open)
   $delivery_js = '';
   $delivery_day = explode(',',$delivery_day);#v0.13.2
   foreach($delivery_day AS $day => $v){
    if(!$v){#closed weekday
     $delivery_js .= 'case '.$day.':';#add js case
    }
   }
   $delivery_day_js = 'if(!outDate)switch(theDate.getDay()){'.$delivery_js.'outDate=!0;break;}'.PHP_EOL;#js switch
  }
?>
<script type="text/javascript">
const mindays = <?php echo intval($this->getParam('delivery_nb_days')); ?>;
const today = new Date();
const nextdelivery = new Date();
nextdelivery.setDate(today.getDate() + mindays);
<?php echo $this->dLang!='en' ? "moment.locale('".$this->dLang."');" : ''; ?>
const dDates = [];
const disallowedDates = <?php echo $disallowedDates ?>;
const delivery_interval = document.getElementById('id_delivery_interval');
const delivery_date = document.getElementById('id_delivery_date');
for(let dd = 0; dd < disallowedDates.length; dd++){
 let tDate = new Date(disallowedDates[dd] + 'T00:00:00');
 if(tDate.toString() != 'Invalid Date')
   dDates[dd] = tDate.toISOString().split('T')[0];
}
const picker_date = new Pikaday(
 {
  disableDayFn: function(theDate) {//inspired by idea of ppmt
   let outDate = theDate.toISOString().split('T')[0];
   outDate = (dDates.indexOf(outDate) != -1);//only select day
   <?php echo $delivery_day_js?>
   return outDate;
  },
  field: document.getElementById('id_deliverydate'),
  format: '<?php $this->lang('L_FORMAT_PIKADAY'); ?>',
<?php
if($this->dLang!='en'):
$pikaM = explode(' ', ucwords($this->getLang('L_MONTH_PIKADAY')));
$pikaD = explode(' ', ucwords($this->getLang('L_WEEK_PIKADAY_LONG')));
$pikaS = explode(' ', ucwords($this->getLang('L_WEEK_PIKADAY')));
?>
  i18n: {
    previousMonth : '<?php echo plxUtils::strCheck($this->getLang('L_PREV_PIKADAY'))?>',
    nextMonth     : '<?php echo plxUtils::strCheck($this->getLang('L_NEXT_PIKADAY'))?>',
    months        : ['<?php echo implode("','", $pikaM)?>'],
    weekdays      : ['<?php echo implode("','", $pikaD)?>'],
    weekdaysShort : ['<?php echo implode("','", $pikaS)?>']
  },
<?php endif; ?>
  firstDay: <?php echo intval($this->getParam('delivery_firstday')) ?>,
  defaultDate: nextdelivery,// the initial date to view when first opened
//  setDefaultDate: true,// make the `defaultDate` the initial selected value
//  position: 'top right',//default is bottom left
  minDate: nextdelivery,
  maxDate: new Date(<?php echo (date('Y')+3) ?>, 12, 31),
  yearRange: [<?php echo date('Y') ?>,<?php echo (date('Y')+3) ?>],
  onSelect: function() {
   let date = document.createTextNode(this.getMoment() + ' ');
   delivery_date.value = this.getMoment()+'';//epoc unix avec +'' at 00:00 +miliseconds ;) gmt?
<?php if(!$this->getParam('delivery_nb_days')){#delivery_nb_days<1 ?>
   if(mindays<1) checkTimes(this._d);//Si même jour
<?php }#delivery_nb_days<1 ?>
  }
 }
);//Fi pikaDay
<?php #tep
# Pour pikaDay : js callback function onSelect   // onOpen: null, onClose: null, onDraw: null
# Quand les livraisons le jour même sont activées (0 jour)
# Verifie et ajuste les crénaux horaires en rapport avec les minutes mini pour une livraison
if(!$this->getParam('delivery_nb_days')){# delivery_nb_days<1
?>
let mintimes = <?php echo intval($this->getParam('delivery_nb_times')); ?>;
function checkTimes(t){
 if(!t) return;
 let now = new Date();
 let isSelToday = (t.toLocaleDateString() == now.toLocaleDateString());
 now = now.toLocaleTimeString().split(':');//heure:minutes
 now = 1 * (1*mintimes + ((1*now[0]) * 60) + (1*now[1]));//en minutes
 for(let i=0;i <  delivery_interval.length; i++){
  if(!delivery_interval[i].value)continue;//1er est vide ;)
  //heure:minutes - heure:minutes ::: 2e inutile ici
  let sel = delivery_interval[i].value.split(' - ')[0].split(':');
  sel = 1 * (((1*sel[0]) * 60) + (1*sel[1]));//en minutes
  //Désactive l'horaire
  if(now >= sel && isSelToday){
   delivery_interval[i].disabled = !0;
   delivery_interval[i].style.display = 'none';
  }
  else{//!isSelToday
   delivery_interval[i].disabled = !1;
   delivery_interval[i].style.display = '';
  }
 }
}

//Une erreur est survenue, verifie le selecteur d'horaires #zéroDay
if(delivery_date.value){
 window.setTimeout(function(){
  let ld=new Date();
  ld.setTime(delivery_date.value);
  checkTimes(ld);
  }, 333);
}
<?php }# Fi delivery_nb_days<1 ?>
</script>
<?php
 }
#hook js du Panier #Choix de date/heure de livraison #pikaday head js & css
 public function themeEndHeadDeliverydateJs(){ ?>
  <link rel="stylesheet" href="<?php echo $this->plxMotor->racine . PLX_PLUGINS . __CLASS__ ;?>/css/pikaday.css" media="screen"/>
  <script type='text/javascript' src='<?php echo $this->plxMotor->racine . PLX_PLUGINS . __CLASS__;?>/js/moment<?php echo $this->dLang!='en' ? '-with-locales' : ''; ?>.min.js'></script>
  <script type='text/javascript' src='<?php echo $this->plxMotor->racine . PLX_PLUGINS . __CLASS__;?>/js/pikaday.js'></script>
<?php
 }

 # Sur une idée de garys02 : command_number
 public function totalCommandFiles(){
  $html=PLX_ROOT.$this->aConf['racine_commandes'].$this->lang.'*.html';
  $files_list = glob($html);#Lister les fichiers avec une extension .html
  return count($files_list) -1;# Nombre de fichiers dans le dossier (-1 pour ne pas prendre en compte le fichier > index.html)
 }
 # Si $up = 0 on retourne le compte des commandes envoyés #src maxiCommandes
 public function commandNumber($up=0, $set=false){
  $fold = PLX_ROOT . $this->aConf['racine_products'];
  if(!file_exists($fold)) return 0;# Non configuré?

  $file = $fold.'.ordersended.zip';# Faux .zip, c'est un .txt ;)
  if(!file_exists($file))file_put_contents($file,'');//create it if need
  $compte = !$set?intval(file_get_contents($file)):0;
  if(!$set&&$up===0) return $compte;
  file_put_contents($file,intval($compte+$up));//+|- 1, 10, 100, ...
 }
 /**
  * Méthode qui retourne le nombre d'éléments (prods & groups (cats) #Todo? in commands: paids, awaits, canceled)
  * le tableau aProds
  * @param	select	critere de recherche: draft, published, all ####, n° categories séparés par un |
  * @param	select	critere de recherche: cancel(ed), await, paid, all ####, n° categories séparés par un |
  * @param	onglet produits, categories, commandes
  * @return	integer	nombre d'elements
  * @author Thomas I.
  **/
 public function nbElements($select='all', $onglet='produits'){
  $nb = 0;
  if($onglet!='commandes'){
   $pcat = $onglet!='produits'?1:0;#pcat?
   if($this->aProds[$this->dLang])
    foreach(array_keys($this->aProds[$this->dLang]) as $i)
     if($this->aProds[$this->dLang][$i]['pcat'] == $pcat)
      if($select=='all'
       OR ($select=='draft' && !$this->aProds[$this->dLang][$i]['active'])
       OR ($select=='published' && $this->aProds[$this->dLang][$i]['active'])
      )
       #if($this->aProds[$this->dLang][$i]['visible'])# dynamic count #maybe afraid if 0 0 0 #See: 'SEARCH' ou '!1' in begin of admin.php
        $nb++;
  }
  return $nb;
 }
 #Change (') simple quote &#39; to Right single quotation mark &#146; &rsquo; ::: http://ascii-code.com/
 static function apostrophe($str){#tips iso = chr(146)
  return strtr($str, array(chr(39)=>'’'));#str_replace(chr(39),'’',$str);
 }
 #Return true \w plxMsg::info (Retail From maxiCo(ntact)mmandes)
 public function cleanOrZip($file=false,$zip=false,$list=false){# Méthode appelée dans l'administration pour nettoyer le cache et créer le zip de sauvegarde * @return null * @author i M@N, Stephane F. Thomas Ingles. #clean cache dir or one file, or zip all cached pages
  $dirComs = PLX_ROOT.$this->aConf['racine_commandes'];# with '/' @end
  # Multilingue : $preload for del or zip all
  if($this->aLangs AND (!$file AND !$list)){
   $list = array();
   foreach($this->aLangs AS $lang){
    $lgf = $lang . DIRECTORY_SEPARATOR;
    $dh = opendir($dirComs.$lgf);
    while (false !== ($file = readdir($dh))){
     if (is_file($dirComs.$lgf.$file) && $file!='.' && $file!='..' && $file!='index.html'){
      $list[] = $lgf.$file;
     }
    }
   }
   unset($dh,$lgf,$lang);
   $file = false;
   rsort($list);
  }
  #Check
  if($list){
   if(empty($list)){
    return plxMsg::Info($this->getLang('L_ERROR_EMPTY_LIST'));
   }
   $list = is_array($list)? $list: array($list);
  }
  #Begin treatment
  if($file){# Clean file
   $file = $dirComs.$file;
   unlink($file);//On l'éfface de la mémoire
   return plxMsg::Info($this->getLang('L_FILE_CLEANED').'&nbsp;: '.$file);
  }
  if($zip){# Zip preload
   include(PLX_PLUGINS.__CLASS__.'/php/ZipHelper.php');# return text if error
   $rootPath = realpath($dirComs);
   $zipname = $dirComs.'.'.__CLASS__.'_'.@$_SERVER['HTTP_HOST'].'_backup.commands.zip';#lost $zip->filename after close() :/
   $zip = new ZipArchive();# Initialize archive object
   if(!$zip->open($zipname, ZipArchive::CREATE | ZipArchive::OVERWRITE))
    return plxMsg::Info($this->getLang('L_CACHE_ZIP_PB').$this->getLang('L_CACHE_ZIP_PC').' : '.zipStatusString($zip->status));
   $zip->setArchiveComment(__CLASS__.' backup : '.date('Y-m-d H:i'));
  }
  if($list){# Zip or del list
   foreach($list as $file){
    if($zip){# Add in archive
     $zip->addFile($dirComs . $file, $file);
    }else{# Del list (unused @now)
     unlink($file);
    }
   }
  }
  elseif(extension_loaded('glob')){# If glob
   if($zip){
    $zip->addGlob($dirComs . '[^index]*\.html$');
   }else{# Clean
    $cached = glob($dirComs . '[^index]*\.html$');
    foreach ($cached as $file) {
     unlink($file);
    }
    unset($cached);
   }
  }
  else{# Classic
   if($cached = opendir($dirComs)){
    while(($file = readdir($cached))!== false){
     if( $file == '.' || $file == '..'|| $file == 'index.html' )
     continue;
     if(strtolower(strrchr($file,'.')=='.html')){
      if($zip){# Zip
       $filePath = realpath($dirComs . $file);# Get real ...
       $relativePath = substr($filePath, strlen($rootPath) + 1);# ... and relative path for current file
       $zip->addFile($filePath, $relativePath);
      }else# Clean
       unlink($dirComs.$file);# Del
     }
    }
    closedir($cached);
   }
  }
  if($zip){
   $zip->close();# Zip archive will be created only after closing object && display real zip status
   if($zip->status != ZIPARCHIVE::ER_OK)
    return plxMsg::Info($this->getLang('L_CACHE_ZIP_PB').$this->getLang('L_CACHE_ZIP_PW').' : '.zipStatusString($zip->status));
   return plxMsg::Info($this->getLang('L_CACHE_ZIPPED'));
  }
  else{
   return plxMsg::Info($this->getLang('L_CACHE_CLEANED'));
  }
 }
 /**
  * Will add money symbol and position(s) to the price
  * Based on the config or lang (if not set)
  * Before or After the price
  * @return formated price
  **/
 public function pos_devise($price, $lang=''){
  $lang = !empty($lang)?$lang:$this->dLang;
  $price = number_format(
     $price
   , $this->getLang('NOMBRE_DECIMALES')
   , $this->getLang('POINT_DECIMAL')
   , $this->getLang('SEPARATEUR_MILLIERS')
  );
  $dev = trim($this->getParam('devise_' . $lang));
  $dev = !empty($dev)?$dev:$this->getLang('MONEY');
  $pos = trim($this->getParam('position_devise_' .$lang));
  $pos = ''!==$pos?$pos:intval($this->getLang('MONEY_POSITION'));
  if($pos)
   return $dev.$price;#before: 1
  return $price.$dev;#after: 0
 }
 /* Crée et retourne un tableau vide ou avec les drapeaux */
 public function mmlFlagImgs($format = '', $reload = false){
  if($this->aLangs){
   if(!$reload AND !empty(self::$mmlFlags))
    return self::$mmlFlags;
   $format = !empty($format)?$format:'<img class="lang #class" src="../../plugins/plxMyMultiLingue/img/#lang.png" alt="#lang">';
   # Créer les images des drapeaux
   foreach($this->aLangs AS $lang){
    self::$mmlFlags[$lang] = strtr($format, array(
     '#lang' => $lang,
     '#class' => ($lang !== $this->dLang? 'No': '') . 'Active ' . ($lang == $_SESSION['default_lang']? '': 'No') . 'Default',
    ));
   }
  }
  return self::$mmlFlags;
 }
 /* Affiche le menu de l'admin. Pincipaux boutons (idée: param icons ♥ ⛘ ⛟ ⏰ ⏱ ⌚ ···) */
 public function adminMenu($ongletEnCours){
  $listeOnglets = array(
   'produits' => array(
    'titre' => $this->getLang('L_MENU_PRODUCTS'),
    'urlHtml' => 'plugin.php?p='.__CLASS__,
    'img' => '⚛'
   ),
   'categories' => array(
    'titre' => $this->getLang('L_MENU_CATS'),
    'urlHtml' => 'plugin.php?p='.__CLASS__.'&amp;mod=cat',
    'img' => '★'
   ),
   'commandes' => array(
    'titre' => $this->getLang('L_MENU_ORDERS'),
    'urlHtml' => 'plugin.php?p='.__CLASS__.'&amp;mod=cmd',
    'img' => '✍'
   )
  );
  if($_SESSION['profil'] == PROFIL_ADMIN){#PROFIL_MODERATOR
   $listeOnglets['configuration'] = array(
    'titre' => $this->getLang('L_MENU_CONFIG'),
    'urlHtml' => 'parametres_plugin.php?p='.__CLASS__,
    'img' => '✔'
   );
  }
  if($this->aLangs OR $ongletEnCours=='configuration'){
   $listeOnglets['showall'] = array(// only Multilingue cats, prods + config
    'titre' => $this->getLang('L_MENU_SHOW_ALL'),
    'urlHtml' => "javascript:void(0);\" id=\"toggleTablesBtn\" style=\"display:none\" onClick=\"var t=this;var tp=t.getAttribute('data-toggle');toggleTables(tp);t.setAttribute('data-toggle',tp!='none'?'none':'block');t.firstChild.textContent=(tp!='none'?'⛗':'⛖');\" data-toggle=\"block" ,
    'img' => '⛖'# '<img class="icon" src="'.PLX_PLUGINS.__CLASS__.'/images/sort_both.png">'
   );
  }
  foreach ($listeOnglets as $codeOnglet => $o){
?>
 <a href="<?php echo $o['urlHtml'];?>"><button type="button"<?php echo ($codeOnglet !== $ongletEnCours) ? '' : ' disabled="disabled" class="myhide"';?> title="<?php echo addslashes($o['titre']);?>"><?php echo $o['img'];?></button></a>
<?php
  }
 }
 #src : www.codexworld.com/how-to/convert-file-size-to-human-readable-format-php/
 public function smartStock($num, $decimals = 0){#todo $this->getLang('POINT_DECIMAL')
  $size = array(' ','k','M','G','T','P','E','Z','Y');
  $factor = floor((strlen($num) - 1) / 3);
  return sprintf("%.{$decimals}f", $num / pow(1000, $factor)) . @$size[$factor];
 }
 public function smartWeight($num, $decimals = 2){#todo $this->getLang('POINT_DECIMAL')
  $size = array($this->getLang('KG'),'T');
  $factor = floor((strlen($num) - 1) / 3);
  return sprintf("%.{$decimals}f", $num / pow(1000, $factor)) . @$size[$factor];
 }

 /**
  * Méthode qui fait une redirection de type 301
  * Venant de PluXml 5.6 (garde compat 5.4 & 5.5)
  * @return null
  * @author Stephane F, Thomas I
  **/
 public function redir301($url) {
  if(method_exists($this->plxMotor,'redir301'))#PluXml 5.6+
   $this->plxMotor->redir301($url);
  header('Status: 301 Moved Permanently', false, 301);
  header('Location: '.$url);
  exit;
 }

 /*
 * Méthode qui retourne une chaine de caractères nettoyée des cdata
 * Méthode qui controle une chaine de caractères pour un fichier .xml
 * Si la chaine est vide ou numérique : la chaine est retournée sans modification
 * Autrement, la chaine est encadrée automatiquement par "<![CDATA[ ... ]]>" si besoin.
 * Si "<![CDATA[" et "]]>" sont présents à l'intérieur de la chaine, alors conversion
 * en entités HTML.
 *
 * @param	str		chaine de caractères à nettoyer
 * @return	string	chaine de caractères nettoyée
 * @author	Stephane F (Origin PluXml 5.7)
 */
 public static function cdataCheck($str) {
  $str = str_ireplace('!CDATA', '&#33;CDATA', $str);
  return str_replace(']]>', ']]&gt;', $str);
 }

/**
  * Méthode qui affiche un message si l'adresse email du contact n'est pas renseignée ou si la langue est absente
  *
  * @return stdio
  * @author Stephane F, Thomas Ingles
  **/
 public function AdminTopBottom() {
  echo '<?php ';?>
  if($plxAdmin->plxPlugins->aPlugins['<?php echo __CLASS__?>']->getParam('email')=='') {
   echo '<p class="warning">Plugin <?php echo __CLASS__?><br /><?php echo $this->getLang('L_ERR_EMAIL')?></p>';
   plxMsg::Display();
  }

  $file = PLX_PLUGINS.'<?php echo __CLASS__?>/lang/'.$plxAdmin->aConf['default_lang'].'.php';
  if(!file_exists($file)) {
   echo '<p class="warning">Plugin <?php echo __CLASS__?><br />'.sprintf('<?php echo $this->getLang('L_LANG_UNAVAILABLE')?>', $file).'</p>';
   plxMsg::Display();
  }
  if(strstr($plxAdmin->get,'<?php echo __CLASS__?>')) echo '<noscript><div class="alert orange" title="Oups! No JS">⚠ <?php $this->lang('L_NOSCRIPT');?>.</div></noscript>';
?><?php
 }

 /**
  * Méthode de traitement du hook plxShowConstruct
  * Si c'est un mode de plxMyShop, injecte la page ds le tableau aStats
  * @return stdio
  * @author Stephane F, Thomas Ingles
  **/
 public function plxShowConstruct(){
  $name = (isset($this->aProds[$this->dLang][$this->idProduit]['name']));
  if($this->plxMotor->mode == 'products' OR $name){
   $lng = '_'.$this->dLang;#MyMultiLingue ou non certains params sont multilingue par défaut (ex: monnaies)
   if($name)
    $name = $this->aProds[$this->dLang][$this->idProduit]['name'];
   else
    $name = ($this->getParam('name_home'.$lng)? $this->getParam('name_home'.$lng): $this->getLang('L_PRODUCTS_HOME_PAGE'));
echo '<?php ';?>
   # infos sur la page statique
   if($this->plxMotor->mode == 'products' OR $this->plxMotor->mode == 'product'){
    $this->plxMotor->cible = $this->plxMotor->cible;
    $array = array();
    $array[$this->plxMotor->cible] = array(
     'name' => '<?php echo $name ?>',
     'menu' => '',
     'url'  => '/template/affichageProduitPublic',
     'readable' => 1,
     'active'   => 1,
     'group'    => ''
    );
    $this->plxMotor->aStats = array_merge($this->plxMotor->aStats, $array);
   }
?><?php
  }
 }

  /**
  * Méthode qui Charge plxMotor
  * Charge plxMotor, les produits, d(ata)Lang, cheminImages, $imageNo
  * et lance traitementPanier() si #Frontend
  * @return nothing
  * @author Thomas Ingles
  **/
 public function plxMotorConstruct() {
  echo '<?php /* ' . __CLASS__ .'->' . __FUNCTION__ .'() */'; ?>
# Charge le Data lang (langue / défaut du site) #v1.0.0 #Memo : #OldFix 0.13: $plugin->default_lang protected in admin plx.5.2 + only work for admin?
  $this->plxPlugins->aPlugins['<?php echo __CLASS__?>']->dLang = $this->aConf['default_lang'];
# 'images' jusqu'au moteur v5.3.1
  $medias = isset($this->aConf['images'])?'images':'medias';
# Charge le chemin des médias en relation avec le moteur de PluXml
  $this->plxPlugins->aPlugins['<?php echo __CLASS__?>']->cheminImages = $this->aConf[$medias];
# Charge le moteur de PluXml et les produits
  $this->plxPlugins->aPlugins['<?php echo __CLASS__?>']->plxMotor = &$this;
  $this->plxPlugins->aPlugins['<?php echo __CLASS__?>']->getProducts();
# Change l'image par defaut si elle est définie
  if($imageNo = $this->plxPlugins->aPlugins['<?php echo __CLASS__?>']->getParam('image_none')){
   <?php echo __CLASS__?>::$imageNo = $this->urlRewrite($imageNo);# $imageNo;#tep²
  }
# Pour le coté public du site (frontend)
  if(!defined('PLX_ADMIN')){
   $this->plxPlugins->aPlugins['<?php echo __CLASS__?>']->traitementPanier();#Always
  }
  # Hook Plugins plxMyShopMotorConstruct
  eval($this->plxPlugins->callHook('<?php echo __CLASS__?>MotorConstruct'));
?><?php
 }

 # 4 shortcode in article [boutonPanier ###]
 public function plxMotorParseArticle() {
  echo '<?php ';?>
  if(get_class($this)=='plxMotor'){#only 4 public page!
   $plxPlugin = $this->plxPlugins->aPlugins['<?php echo __CLASS__; ?>'];
   if(!empty($art['chapo']))
    $art['chapo'] = $plxPlugin->traitementPageStatique($art['chapo']);
   $art['content'] = $plxPlugin->traitementPageStatique($art['content']);
   unset($plxPlugin);
  }
  ?>
<?php
 }
 # 4 shortcode in page [boutonPanier ###]
 public function plxShowStaticContent(){
  echo '<?php ';?>
   $plxPlugin = $this->plxMotor->plxPlugins->aPlugins['<?php echo __CLASS__; ?>'];
   $output = $plxPlugin->traitementPageStatique($output);
   unset($plxPlugin);
  ?>
<?php
 }

 /**
  * Methode pour afficher les shortcodes [boutonPanier(_|&nbsp;|\s)###]
  * Parse les contenus des articles et des pages (inclus celles de myShop)
  * Il est possible de remplacer l'espace par un tiret bas "_" (undrscore)
  * Si &nbsp; est ajouter par l'éditeur html, ça le capte aussi ;)
  * @return null
  * @author Anthony GUÉRIN, Florent MONTHEL, Stéphane FERRARI, Thomas INGLES
  **/
 public function traitementPageStatique($output){# 4 shortcode in article & static [boutonPanier ###]
  $rx = "~\[{$this->shortcode}(_|&nbsp;|\s)([\d]{3})\]~U";
  preg_match_all($rx, $output, $resultat);
  if (0 < count($resultat[2])){
   $tabCodes = array();
   $tabRemplacement = array();
   $this->donneesModeles['plxPlugin'] = $this;
   foreach ($resultat[2] as $i => $codeProduit){
    $this->shortcodeactif++;
    $tabCodes[] = '['.$this->shortcode.$resultat[1][$i].$codeProduit.']';
    ob_start();
    $this->donneesModeles['k'] = $codeProduit;
    $this->modele('espacePublic/boucle/produitRubrique');
    $tabRemplacements[] = ob_get_clean();
   }
   $output = str_replace($tabCodes, $tabRemplacements, $output);

   ob_start();
   if (in_array($this->getParam('affPanier'),
     array('basPageStars', 'basPage', 'partout')
    )){
    $_SESSION[__CLASS__]['msgCommand']='';
    $this->validerCommande();
    $this->modele('espacePublic/panier');
   }
   $output .= ob_get_clean();
  }
  //~ $this->shortcodeactif = false;# fix if is home mode & have shortcode : script tag for panier.js not in the html :/
  return $output;
 }

 /**
  * Méthode qui effectue une analyse de la situation et détermine
  * le mode à appliquer. Cette méthode alimente ensuite les variables
  * de classe adéquates
  * @return null
  * @author Anthony GUÉRIN, Florent MONTHEL, Stéphane F, Thomas INGLES
  **/
 public function plxMotorPreChauffageBegin(){

  # Hook Plugins plxMyShopMotorPreChauffageBegin
  if(eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . 'MotorPreChauffageBegin'))) return;

  if(!$this->plxMotor->get) return;

  $home = $this->lang . 'products/stars';
  $panier = $this->lang . 'boutique/panier';
  $product = $this->lang . 'product';

  # contrôleur des pages du plugin
  # en vedette (products/stars) todo /page2 #idée /all
# if(preg_match('~^'.$home.'$~',$this->plxMotor->get)){#regex
  if($this->plxMotor->get === $home AND strpos($this->plxMotor->get,$home)===0){
   $template = $this->getParam('template_home_' . $this->dLang);
   $template = $template? $template: $this->getParam('template');
   $this->plxMotor->mode = 'products';
   $this->plxMotor->aConf['racine_statiques'] = '';
   $this->plxMotor->cible = $this->plxMotor->aConf['racine_plugins'] . __CLASS__ . '/';#maybe in old pluxml add slash or form '/$nomPlugin/form' ?
   $this->plxMotor->template = $template;
   echo '<?php return TRUE;?>';
  }
  # le panier
# elseif(preg_match('~^'.$panier.'$~',$this->plxMotor->get)){#regex
  elseif($this->plxMotor->get === $panier AND strpos($this->plxMotor->get, $panier)===0){
   $classeVue = 'panier';

   require_once 'classes/vues/' . $classeVue . '.php';
   $this->vue = new $classeVue();
   $this->vue->plxPlugin = $this;
   $this->vue->traitement();

   $this->plxMotor->mode = 'boutique';
   $this->plxMotor->cible = __CLASS__ . '/';
   $this->plxMotor->template = $this->getParam('template');
   $this->plxMotor->aConf['racine_statiques'] = $this->plxMotor->aConf['racine_plugins'];
   $this->plxMotor->aStats[$this->plxMotor->cible] = array(
    'name' => $this->vue->titre(),
    'menu' => '',#non
    'url' => '/template/vue',#maybe in old pluxml add slash __CLASS__.'/template/vue' ?
    'active' => 1,
    'readable' => 1,
    'title_htmltag' => '',
   );
   echo '<?php return TRUE;?>';
  }
  # pages des produits et des catégories #Todo 3 numbers max
  elseif(preg_match('~^'.$product.'([\d]+)/?([a-z0-9-]+)?~', $this->plxMotor->get, $capture)){
   $this->idProduit = str_pad($capture[1], 3, '0', STR_PAD_LEFT);
   if(!isset($this->aProds[$this->dLang][$this->idProduit]) OR !$this->aProds[$this->dLang][$this->idProduit]['active']){
    $this->plxMotor->error404(L_ERR_PAGE_NOT_FOUND);
   }else{
    if(isset($capture[2]) AND $this->aProds[$this->dLang][$this->idProduit]['url']==$capture[2]){
     $template = $this->aProds[$this->dLang][$this->idProduit]['template'] === ''
       ? $this->getParam('template')
       : $this->aProds[$this->dLang][$this->idProduit]['template'];

     $this->plxMotor->mode = 'product';
     $this->plxMotor->aConf['racine_statiques'] = '';
     $this->plxMotor->cible = $this->plxMotor->aConf['racine_plugins'] . __CLASS__ . '/';#maybe in old pluxml add slash or form '/$nomPlugin/form' ?
     $this->plxMotor->template = $template;
     echo '<?php return TRUE;?>';
    }else{
     $this->redir301($this->plxMotor->urlRewrite('?product'.intval($this->idProduit).'/'.$this->aProds[$this->dLang][$this->idProduit]['url']));
    }
   }
  }

  # Hook Plugins plxMyShopMotorPreChauffageBeginEnd
  eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . 'MotorPreChauffageBeginEnd'));
 }
 /**
  * Méthode qui référence les produits dans le sitemap
  * @return stdio
  * @author David.L, Thomas I.
  **/
 public function SitemapStatics(){
  if (isset($this->aProds[$this->dLang]) && is_array($this->aProds[$this->dLang])){
   $home = FALSE;#products/stars
   foreach($this->aProds[$this->dLang] as $key => $value){
    if ($value['active']==1 &&  $value['readable']==1){
     #products/stars
     if (!$home && strpos($value['active'], '000') !== FALSE){
      $home = TRUE;
     }
?>
	<url>
		<loc><?php echo $this->plxMotor->urlRewrite('?'.$this->lang.'product'.intval($key).'/'.$value['url'])?></loc>
		<changefreq>daily</changefreq>
		<priority>0.8</priority>
	</url>
<?php
    }
   }
   if ($home){
?>
	<url>
		<loc><?php echo $this->plxMotor->urlRewrite('?'.$this->lang.'products/stars')?></loc>
		<changefreq>daily</changefreq>
		<priority>0.8</priority>
	</url>
<?php
   }
  }
 }

 /**
  * Méthode qui parse le fichier les produits et alimente
  * le tableau aProds
  * @param filename emplacement du fichier XML des produits
  * @return null
  * @author David.L, Thomas I.
  **/
 public function getProducts($filename=''){
  $aLangs = ($this->aLangs)?$this->aLangs:array($this->dLang);
  $filenameOrigin = $filename;
  foreach($aLangs as $lang) {
   $lgf=($this->aLangs)?$lang.'/':'';#folders
   $lng=($this->aLangs)?'_'.$lang:'';#post vars
   $filename = $filenameOrigin=='' ? PLX_ROOT.PLX_CONFIG_PATH.$lgf.'products.xml' : $filename;
   $this->aProds[$lang]=false;
   if(!is_file($filename)){
    touch($filename);#create it
    continue;
   }
   # Mise en place du parseur XML
   $data = implode('',file($filename));
   $parser = xml_parser_create(PLX_CHARSET);
   xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING,0);
   xml_parser_set_option($parser,XML_OPTION_SKIP_WHITE,0);
   xml_parse_into_struct($parser,$data,$values,$iTags);
   xml_parser_free($parser);
   if(isset($iTags['product']) AND isset($iTags['name'])){
    $nb = sizeof($iTags['name']);
    $size=ceil(sizeof($iTags['product'])/$nb);
    for($i=0;$i<$nb;$i++){
     $attributes = $values[$iTags['product'][$i*$size]]['attributes'];
     $number = $attributes['number'];

     # Recuperation du nom du produit
     $this->aProds[$lang][$number]['name']=plxUtils::getValue($values[$iTags['name'][$i]]['value']);

     # Recuperation prix ttc
     $pricettc = plxUtils::getValue($iTags['pricettc'][$i]);
     $this->aProds[$lang][$number]['pricettc']=plxUtils::getValue($values[$pricettc]['value']);

     # Recuperation noaddcart
     $noaddcart = plxUtils::getValue($iTags['noaddcart'][$i]);
     $this->aProds[$lang][$number]['noaddcart']=plxUtils::getValue($values[$noaddcart]['value']);
     $notice_noaddcart = plxUtils::getValue($iTags['notice_noaddcart'][$i]);
     $this->aProds[$lang][$number]['notice_noaddcart']=plxUtils::getValue($values[$notice_noaddcart]['value']);
     # Recuperation nombre en stock
     $iteminstock = plxUtils::getValue($iTags['iteminstock'][$i]);
     $this->aProds[$lang][$number]['iteminstock']=plxUtils::getValue($values[$iteminstock]['value']);

     # Recuperation poid
     $poidg = plxUtils::getValue($iTags['poidg'][$i]);
     $this->aProds[$lang][$number]['poidg']=plxUtils::getValue($values[$poidg]['value']);

     # Recuperation image
     $image = plxUtils::getValue($iTags['image'][$i]);
     $this->aProds[$lang][$number]['image']=plxUtils::getValue($values[$image]['value']);

     # Recuperation chapo
     $chapo = plxUtils::getValue($iTags['chapo'][$i]);
     $this->aProds[$lang][$number]['chapo']=plxUtils::getValue($values[$chapo]['value']);

     # Recuperation de la balise title
     $title_htmltag = plxUtils::getValue($iTags['title_htmltag'][$i]);
     $this->aProds[$lang][$number]['title_htmltag']=plxUtils::getValue($values[$title_htmltag]['value']);

     # Recuperation du meta description
     $meta_description = plxUtils::getValue($iTags['meta_description'][$i]);
     $this->aProds[$lang][$number]['meta_description']=plxUtils::getValue($values[$meta_description]['value']);

     # Recuperation du meta keywords
     $meta_keywords = plxUtils::getValue($iTags['meta_keywords'][$i]);
     $this->aProds[$lang][$number]['meta_keywords']=plxUtils::getValue($values[$meta_keywords]['value']);

     # Recuperation de(s) groupe(s) du produit
     $this->aProds[$lang][$number]['group']=plxUtils::getValue($values[$iTags['group'][$i]]['value']);

     # Recuperation de la variable categorie 1 si c'est une categorie
     $this->aProds[$lang][$number]['pcat']=plxUtils::getValue($values[$iTags['pcat'][$i]]['value']);

     if($this->aProds[$lang][$number]['pcat'] == 0 AND empty($this->aProds[$lang][$number]['group'])) {
       $this->aProds[$lang][$number]['group'] = '000';#home of MyShop v1.0.0
     }

     # Recuperation si afficher au menu
     $this->aProds[$lang][$number]['menu']=plxUtils::getValue($values[$iTags['menu'][$i]]['value']);

     # Recuperation de l'url du produit
     $this->aProds[$lang][$number]['url']=strtolower($attributes['url']);

     # Recuperation de l'etat du produit
     $this->aProds[$lang][$number]['active']=intval($attributes['active']);

     # recuperation du fichier template
     $this->aProds[$lang][$number]['template']=isset($attributes['template'])?$attributes['template']:$this->getParam('template');

     # On verifie que le produit existe bien
     $file = PLX_ROOT.$this->aConf['racine_products'].$lgf.$number.'.'.$attributes['url'].'.php';

     # On teste si le fichier est lisible
     $this->aProds[$lang][$number]['readable'] = (is_readable($file) ? 1 : 0);

     # Hook Plugins plxMyShopGetProducts
     eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . 'GetProducts'));#Fixed  Fatal error: Uncaught Error: Call to a member function callHook() on null (admin/auth.php)
    }
   }
  }
 }

 /**
  * Méthode qui édite le fichier XML des produits selon le tableau $content
  * @param content tableau multidimensionnel des produits
  * @param action permet de forcer la mise àjour du fichier
  * @return string
  * @author David L., Thomas I.
  **/
 public function editProducts($content, $action=false){
  $retour = $reterr = '';
  $dfltLng = '_'.$this->dLang;
  $aLangs = ($this->aLangs)?$this->aLangs:array($this->dLang);
  foreach($aLangs as $lang) {
   $save = $this->aProds[$lang];
   $lgf=($this->aLangs)?$lang.'/':'';#folders
   $lng=($this->aLangs)?'_'.$lang:'';#post vars
   # suppression
   if(!empty($content['selection']) AND $content['selection']=='delete' AND isset($content['idProduct'])){
    foreach($content['idProduct'] as $product_id){
     $filename = PLX_ROOT.$this->aConf['racine_products'].$lgf.$product_id.'.'.$this->aProds[$lang][$product_id]['url'].'.php';
     if(is_file($filename)) unlink($filename);
     # si le produit supprimé est en page d'accueil on met à jour le parametre
     unset($this->aProds[$lang][$product_id]);
     $action = true;
    }
   }
   # mise à jour de la liste des produits
   elseif(!empty($content['update'])){
    foreach($content['productNum'] as $product_id){
     $stat_name = isset($content[$product_id.'_name'.$lng])?$content[$product_id.'_name'.$lng]:$content[$product_id.'_name'.$dfltLng];
     if(0<strlen($stat_name) && $stat_name !== '0'){#$stat_name!='' FIX is title = 0
      $url = (isset($content[$product_id.'_url'.$lng])?trim($content[$product_id.'_url'.$lng]):'');
      $stat_url = (!empty($url)?plxUtils::title2url($url):plxUtils::title2url($stat_name));
      if(empty($stat_url)) $stat_url = plxUtils::title2url($this->getLang('L_DEFAULT_NEW_PRODUCT_URL').'-'.$product_id);
      # On vérifie si on a besoin de renommer le fichier du produit
      if(isset($this->aProds[$lang][$product_id]) AND $this->aProds[$lang][$product_id]['url']!=$stat_url){
       $oldfilename = PLX_ROOT.$this->aConf['racine_products'].$lgf.$product_id.'.'.$this->aProds[$lang][$product_id]['url'].'.php';
       $newfilename = PLX_ROOT.$this->aConf['racine_products'].$lgf.$product_id.'.'.$stat_url.'.php';
       if(is_file($oldfilename)) rename($oldfilename, $newfilename);
      }
      $this->aProds[$lang][$product_id]['pcat'] = trim(isset($content[$product_id.'_pcat'.$lng])?$content[$product_id.'_pcat'.$lng]:$content[$product_id.'_pcat'.$dfltLng]);
      $this->aProds[$lang][$product_id]['menu'] = trim(isset($content[$product_id.'_menu'.$lng])?$content[$product_id.'_menu'.$lng]:'');
      $this->aProds[$lang][$product_id]['group'] = isset($this->aProds[$lang][$product_id]['group'])?$this->aProds[$lang][$product_id]['group']:'';
      $this->aProds[$lang][$product_id]['name'] = self::apostrophe($stat_name);
      $this->aProds[$lang][$product_id]['url'] = plxUtils::checkSite($url)?$url:$stat_url;
      $this->aProds[$lang][$product_id]['active'] = isset($content[$product_id.'_active'.$lng])?$content[$product_id.'_active'.$lng]:$content[$product_id.'_active'.$dfltLng];
      $this->aProds[$lang][$product_id]['ordre'] = intval(isset($content[$product_id.'_ordre'.$lng])?$content[$product_id.'_ordre'.$lng]:$content[$product_id.'_ordre'.$dfltLng]);
      $this->aProds[$lang][$product_id]['template'] = isset($this->aProds[$lang][$product_id]['template'])?$this->aProds[$lang][$product_id]['template']:$this->getParam('template');
      $this->aProds[$lang][$product_id]['title_htmltag'] = isset($this->aProds[$lang][$product_id]['title_htmltag'])?$this->aProds[$lang][$product_id]['title_htmltag']:'';
      $this->aProds[$lang][$product_id]['image'] = isset($this->aProds[$lang][$product_id]['image'])?$this->aProds[$lang][$product_id]['image']:'';
      $this->aProds[$lang][$product_id]['chapo'] = isset($this->aProds[$lang][$product_id]['chapo'])?$this->aProds[$lang][$product_id]['chapo']:'';
      $this->aProds[$lang][$product_id]['noaddcart'] = isset($this->aProds[$lang][$product_id]['noaddcart'])?$this->aProds[$lang][$product_id]['noaddcart']:'';
      $this->aProds[$lang][$product_id]['iteminstock'] = isset($this->aProds[$lang][$product_id]['iteminstock'])?$this->aProds[$lang][$product_id]['iteminstock']:'';
      $this->aProds[$lang][$product_id]['notice_noaddcart'] = isset($this->aProds[$lang][$product_id]['notice_noaddcart'])?$this->aProds[$lang][$product_id]['notice_noaddcart']:'';
      $this->aProds[$lang][$product_id]['pricettc'] = isset($this->aProds[$lang][$product_id]['pricettc'])?$this->aProds[$lang][$product_id]['pricettc']:'';
      $this->aProds[$lang][$product_id]['poidg'] = isset($this->aProds[$lang][$product_id]['poidg'])?$this->aProds[$lang][$product_id]['poidg']:'';
      $this->aProds[$lang][$product_id]['meta_description'] = isset($this->aProds[$lang][$product_id]['meta_description'])?$this->aProds[$lang][$product_id]['meta_description']:'';
      $this->aProds[$lang][$product_id]['meta_keywords'] = isset($this->aProds[$lang][$product_id]['meta_keywords'])?$this->aProds[$lang][$product_id]['meta_keywords']:'';
      # Hook Plugins plxMyShopEditProductsUpdate
      eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . 'EditProductsUpdate'));
      $action = true;
     }
    }
    # On va trier les clés selon l'ordre choisi
    //~ if(sizeof($this->aProds[$lang])>0) uasort($this->aProds[$lang], create_function('$a, $b', 'return $a["ordre"]>$b["ordre"];'));# create_function deprecated php 7.2 : https://www.php.net/manual/en/function.create-function.php
    if(sizeof($this->aProds[$lang])>0) uasort($this->aProds[$lang], function($a, $b) { return $a['ordre']>$b['ordre']; });#php 5.3+
   }
   # sauvegarde
   if($action){
    $products_name = array();
    $products_url = array();
    # On génére le fichier XML
    $xml = '<?xml version="1.0" encoding="'.PLX_CHARSET.'"?>' . PHP_EOL;
    $xml .= '<document>' . PHP_EOL;
    if (isset($this->aProds[$lang]) && is_array($this->aProds[$lang])){
     foreach($this->aProds[$lang] as $product_id => $product){
      # garder une compatibilité de l'image avec l'existant.
      $product['image'] = str_replace($this->plxMotor->aConf['medias'],'',$product['image']);
      # control de l'unicité du titre de la page
      if(in_array($product['name'], $products_name, true))//Fix 00 not same as 000 \W strict ;)
       $reterr .= plxUtils::strCheck($this->getLang('L_ERR_PRODUCT_ALREADY_EXISTS').' : '.$product['name']).PHP_EOL;
      else
       $products_name[] = $product['name'];

      # control de l'unicité de l'url de la page
      if(in_array($product['url'], $products_url, true)){//Fix 00 not same as 000 \W strict ;)
       $this->aProds[$lang] = $save;
       $reterr .= L_ERR_URL_ALREADY_EXISTS.' : '.plxUtils::strCheck($product['url']).PHP_EOL;
      }
      else
       $products_url[] = $product['url'];

      if(!$product['pcat'] AND (strlen($product['group']) < 2))
       $product['group'] = '000';#MyShop home if 0 or '' #prod

      $xml .= "\t".'<product number="'.$product_id.'" active="'.$product['active'].'" url="'.$product['url'].'" template="'.basename($product['template']).'">';
      $xml .= '<pcat><![CDATA['.self::cdataCheck($product['pcat']).']]></pcat>';
      $xml .= '<menu><![CDATA['.self::cdataCheck($product['menu']).']]></menu>';
      $xml .= '<group><![CDATA['.self::cdataCheck($product['group']).']]></group>';
      $xml .= '<name><![CDATA['.self::cdataCheck($product['name']).']]></name>';
      $xml .= '<image><![CDATA['.self::cdataCheck($product['image']).']]></image>';
      $xml .= '<noaddcart><![CDATA['.self::cdataCheck($product['noaddcart']).']]></noaddcart>';
      $xml .= '<iteminstock><![CDATA['.self::cdataCheck($product['iteminstock']).']]></iteminstock>';
      $xml .= '<notice_noaddcart><![CDATA['.self::cdataCheck(($product['noaddcart']&&empty($product['notice_noaddcart']))?$this->getLang('L_NOTICE_NOADDCART'):$product['notice_noaddcart']).']]></notice_noaddcart>';
      $xml .= '<pricettc><![CDATA['.self::cdataCheck($product['pricettc']).']]></pricettc>';
      $xml .= '<poidg><![CDATA['.self::cdataCheck(($product['poidg']==0?'0.0':$product['poidg'])).']]></poidg>';
      $xml .= '<meta_description><![CDATA['.self::cdataCheck($product['meta_description']).']]></meta_description>';
      $xml .= '<meta_keywords><![CDATA['.self::cdataCheck($product['meta_keywords']).']]></meta_keywords>';
      $xml .= '<title_htmltag><![CDATA['.self::cdataCheck($product['title_htmltag']).']]></title_htmltag>';
      $xml .= '<chapo><![CDATA['.self::cdataCheck($product['chapo']).']]></chapo>';
      # Hook Plugins plxMyShopEditProductsXml
      eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . 'EditProductsXml'));
      $xml .= '</product>' . PHP_EOL;
     }
    }
    $xml .= '</document>';
    # On écrit le fichier si une action valide a été faite
    if(plxUtils::write($xml,PLX_ROOT.PLX_CONFIG_PATH.$lgf.'products.xml')){
     $retour .= $this->aLangs?$lang.', ':'';
    } else {
     $this->aProds[$lang] = $save;
     $reterr .= L_SAVE_ERR.' '.PLX_ROOT.PLX_CONFIG_PATH.$lgf.'products.xml'.PHP_EOL;
    }
   }
  }
  if($action===true) {
   $reterr = !empty($reterr)?plxMsg::Error(nl2br($reterr)):'';
   return $reterr.plxMsg::Info(L_SAVE_SUCCESSFUL.($this->aLangs?' ('.trim($retour,', ').')':''));
  }
 }

 /**
  * Méthode qui lit le fichier d'un produit
  * @param num numero du fichier du produit
  * @return string contenu de la page
  * @author Stephane F.
  **/
 public function getFileProduct($num,$lang){
  $lgf=(!empty($lang) && $this->aLangs)?$lang.'/':'';#folders
  $lng=(!empty($lang) && $this->aLangs)?$lang:$this->dLang;#Product lang or not
  $content = '';
  # Emplacement de la page
  $filename = PLX_ROOT.$this->aConf['racine_products'].$lgf.$num.'.'.$this->aProds[$lng][$num]['url'].'.php';
  if(is_file($filename) AND filesize($filename) > 0){
   if($f = fopen($filename, 'r')){
    $content = fread($f, filesize($filename));
    fclose($f);
    # On retourne le contenu
    return $content;
   }
  }
  $content = trim($content);#in php < 5.5: Can't use function return value in write context with this: empty(trim($content)
  if ($this->aLangs && empty($content)){# si contenu vide en multilingue on essaye de recuperer sans la langue.
   $filename = PLX_ROOT.$this->aConf['racine_products'].$num.'.'.$this->aProds[$lng][$num]['url'].'.php';
   if(is_file($filename) AND filesize($filename) > 0){
    if($f = fopen($filename, 'r')){
     $content = fread($f, filesize($filename));
     fclose($f);
     # On retourne le contenu
     return $content;
    }
   }
  }
  return null;
 }

 /**
  * Méthode qui sauvegarde le contenu d'un produit
  * @param content données à sauvegarder
  * @return string
  * @author David.L, Thomas I
  **/
 public function editProduct($content){
  # Hook Plugins plxMyShopEditProductBegin
  if(eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . 'EditProductBegin'))) return;
  # Mise à jour du fichier product.xml
  $lngd=($this->aLangs)?'_'.$_SESSION['default_lang']:'';#default lang 4 stock system
  $aLangs = ($this->aLangs)?$this->aLangs:array($this->dLang);
  $pricettc4all = isset($content['pricettc4all']);#'pricettc4all' => string 'on' OR unsetted ;)
  $noaddcart4all = isset($content['noaddcart4all']);#'noaddcart4all' => string 'on' OR unsetted ;)
  foreach($aLangs as $lang) {#stock & weight 4 all langs
   $lang=($this->aLangs)?'_'.$lang:'';#post vars
   $content['iteminstock'.$lang] = $content['iteminstock'.$lngd];#with default lang stock
   $content['poidg'.$lang] = $content['poidg'.$lngd];#with default lang weight
   if($pricettc4all)
    $content['pricettc'.$lang] = $content['pricettc'.$lngd];#with default lang pricettc
   if($noaddcart4all)
    $content['noaddcart'.$lang] = $content['noaddcart'.$lngd];#with default lang noaddcart
  }
  foreach($aLangs as $lang) {
   $lgf=($this->aLangs)?$lang.'/':'';#folders
   $lng=($this->aLangs)?'_'.$lang:'';#post vars

   # données des groupes du produit
   if (!$this->aProds[$lang][$content['id']]['pcat']){# products/stars #Home of MyShop
    $this->aProds[$lang][$content['id']]['group'] = '000';# by default v1.0.0
   }
   if (isset($content['listeCategories'.$lng])){
    $this->aProds[$lang][$content['id']]['group'] = implode(',', $content['listeCategories'.$lng]);
   }

   # formatage du prix et du poids à l'édition
   foreach (array('pricettc', 'poidg') as $champ){
    $content[$champ.$lng] = floatval(number_format($content[$champ.$lng], 2, '.', ''));
   }

   # données du produit
   $this->aProds[$lang][$content['id']]['image'] = $content['image'.$lng];
   $this->aProds[$lang][$content['id']]['chapo'] = $content['chapo'.$lng];
   $this->aProds[$lang][$content['id']]['noaddcart'] = $content['noaddcart'.$lng];
   $this->aProds[$lang][$content['id']]['iteminstock'] = $content['iteminstock'.$lng];
   $this->aProds[$lang][$content['id']]['notice_noaddcart'] = $content['notice_noaddcart'.$lng];
   $this->aProds[$lang][$content['id']]['pricettc'] = $content['pricettc'.$lng];
   $this->aProds[$lang][$content['id']]['poidg'] = $content['poidg'.$lng];
   $this->aProds[$lang][$content['id']]['template'] = $content['template'.$lng];
   $this->aProds[$lang][$content['id']]['title_htmltag'] = trim($content['title_htmltag'.$lng]);
   $this->aProds[$lang][$content['id']]['meta_description'] = trim($content['meta_description'.$lng]);
   $this->aProds[$lang][$content['id']]['meta_keywords'] = trim($content['meta_keywords'.$lng]);
   # Hook Plugins plxMyShopEditProduct
   eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . 'EditProduct'));
  }
  if($this->editProducts(null,true)){
   if (!is_dir(PLX_ROOT.$this->aConf['racine_products'])){#créer les dossiers de sauvegarde selon la config
    mkdir(PLX_ROOT.$this->aConf['racine_products'], 0755, true);
   }
   $aLangs = array($this->dLang);
   if($this->aLangs){#créer les dossiers de sauvegarde si MyMultilingue
    $aLangs = $this->aLangs;
    foreach ($this->aLangs as $lang){
     if (!is_dir(PLX_ROOT.$this->aConf['racine_products'].$lang.'/')){
      mkdir(PLX_ROOT.$this->aConf['racine_products'].$lang.'/', 0755, true);
     }
    }
   }
   $infos = $err = null;
   foreach($aLangs as $lang){
    $lgf=($this->aLangs)?$lang.'/':'';#folders
    $lng=($this->aLangs)?'_'.$lang:'';#post vars
    # Génération du nom du fichier de la page statique
    $filename = PLX_ROOT.$this->aConf['racine_products'].$lgf.$content['id'].'.'.$this->aProds[$lang][ $content['id'] ]['url'].'.php';
    # On écrit le fichier
    if(!plxUtils::write($content['content'.$lng],$filename))
     $err .= L_SAVE_ERR.' '.$filename.'<br />';
    else
     $infos .= $lang.', ';
   }
   $infos = ' ('.trim($infos,', ').')';
   $filename = ' '.str_replace($lang.'/','',$filename).' ';# on enleve la langue pour l'affichage du message
   if(!empty($err))
    $infos = plxMsg::Error($err.L_SAVE_SUCCESSFUL.$infos);
   else
    $infos = plxMsg::Info(L_SAVE_SUCCESSFUL.$infos);
   return $infos;
  }
 }

 /**
  * Méthode qui change les stocks des produits commandés par un client
  * @param content : données à changer $_SESSION[__CLASS__]['prods'][$this->dLang]
  * @author Philippe.LT, Thomas I
  **/
 public function editStocks($content){
  $aLangs = ($this->aLangs)?$this->aLangs:array($this->dLang);
  foreach ($content as $pId => $nb){
   if ($this->aProds[$this->dLang][$pId]['iteminstock'] != '' OR $this->aProds[$this->dLang][$pId]['iteminstock'] > 0){
    if (intval($this->aProds[$this->dLang][$pId]['iteminstock']) >= intval($nb)){
     foreach($aLangs as $lang) {#4 all langs
      $item=array();
      $item['changeStock'] = true;#4 activate hook plxMyShopEditProductBegin (4 changeStock only)
      $this->aProds[$lang][$pId]['iteminstock'] -= intval($nb);#decrease stock
      if (empty($this->aProds[$lang][$pId]['iteminstock'])) {#if stock == 0
       $this->aProds[$lang][$pId]['noaddcart']=1;#auto hide basket button
      }
      $this->editProduct($item);
     }
    }
   }
  }
 }

 /**
  * Méthode qui sauvegarde le nouveau stock du produit commandé (hook plxMyShopEditProductBegin)
  * @author Thomas I
  **/
 public function changeStock(){
  echo '<?php ';?>
  if(isset($content['changeStock'])){
   $this->editProducts(null,'public');#public (or other str) fix 4 uncall class plxMsg::####() at the end of editProducts() (only 4 admin)
   return true;#break editProduct() (only change stock & basket button)
  }
?><?php
 }

 /**
  * Méthode qui retourne l'id du produit active
  * @return int
  * @scope product
  * @author David.L
  **/
 public function productId(){
  # Verifie s'il existe (en mode categorie?)
  if($this->plxMotor->mode == 'product' AND isset($this->aProds[$this->dLang][$this->idProduit]))
   return intval($this->idProduit);
 }

 /**
  * Méthode qui affiche l'url du produit de type relatif ou absolu
  * @param type type de lien : relatif ou absolu (URL complète)
  * @return stdout
  * @scope product
  * @author David.L, Thomas I.
  **/
 public function productUrl($type='relatif'){
  echo $this->productRUrl($this->idProduit);
 }

 /**
  * Méthode qui retoune l'URL complète réécrite si no 'relatif' (products/stars, produit & groupe)
  * @param $key : identifiant type 001
  * @return string : URL
  * @scope product
  * @author David.L, Thomas I.
  **/
 public function productRUrl($key=false, $type='relatif'){
  $key = $key?$key:$this->idProduit;
  if(empty($key)) $key = $this->donneesModeles['k'];# = $k;
  # HomePage
  if($key == '000' AND !intval($key))
   $uri = $this->lang.'products/stars';# MyShop HomePage
  elseif(isset($this->aProds[$this->dLang][$key]))# Recupération ID URL
   $uri = $this->lang.'product'.intval($key).'/'.$this->aProds[$this->dLang][$key]['url'];
  if($uri)
   return $type!='relatif'?$uri:$this->plxMotor->urlRewrite('?'.$uri);
 }

 /**
  * Méthode qui retourne le titre du produit
  * @return stdout
  * @scope product
  * @author Thomas
  **/
 public function productRTitle(){
  if($this->idProduit == '000'){
   $name = $this->getParam('name_home_'.$this->dLang);#MyMultiLingue ou non);
   $name = $name? $name: $this->getLang('L_PRODUCTS_HOME_PAGE');# 000 == MyShopHomePage
  }else{
    $name = $this->aProds[$this->dLang][$this->idProduit]['name'];
  }
  return $name;
 }

 /**
  * Méthode qui affiche le titre du produit
  * @return stdout
  * @scope product
  * @author David.L
  **/
 public function productTitle(){
   echo plxUtils::strCheck($this->productRTitle());
 }

 /** (since 1.0.0)
  * Méthode qui affiche l'image du produit ou celle par défaut selon un formet
  * @
  * @scope product
  * @author  Thomas I.
  **/
 public function productImage($format='', $class='', $extra=''){
  $format = $format?$format:'<img src="#src" alt="#alt" title="#title" #class#extra />';
  $class = $class?'class="'.$class.'"':'';
  $extra = $extra?$extra:'';
  $title = plxUtils::strCheck($this->productRTitle());
  $rc = plxUtils::strCheck($this->productRImage());
  echo strtr($format ,
   array(
    '#src' => $rc,
    '#alt' => $title,
    '#title' => $title,
    '#class' => $class,
    '#extra' => $extra
   )
  );
 }

 /** (since 1.0.0)
  * Méthode qui retourne l'url de image du produit ou celle par défaut
  * @return stdout
  * @scope product
  * @author David.L, Thomas I.
  **/
 public function productRImage(){
  if($this->idProduit == '000'){
   $i = $this->getParam('image_home_'.$this->dLang);#MyMultiLingue ou non);
  }else{
   $i = $this->aProds[$this->dLang][$this->idProduit]['image'];
  }
  $i = (!empty($i) AND is_readable(PLX_ROOT . $this->cheminImages.$i))?$this->plxMotor->urlRewrite($this->cheminImages.$i):self::$imageNo;
  return plxUtils::strCheck($i);
 }

 /**
  * Méthode qui retourne l'id du produit
  * @return stdout
  * @scope product
  * @author David.L
  **/
 public function productNumber(){
  return $this->idProduit;
 }

 /**
  * Méthode qui affiche le prix TTC du produit
  * @return stdout
  * @scope product
  * @author David.L
  **/
 public function productPriceTTC(){

  #echo plxUtils::strCheck($this->aProds[$this->idProduit]['pricettc']);
  return plxUtils::strCheck($this->aProds[$this->dLang][$this->idProduit]['pricettc']);
 }

 /**
  * Méthode qui affiche ou pas le bouton acheter
  * @return stdout
  * @scope product
  * @author David.L
  **/
 public function productNoAddCart(){
  return plxUtils::strCheck($this->aProds[$this->dLang][$this->idProduit]['noaddcart']);
 }

 /**
  * Méthode qui affiche une notice si le bouton ajouter au panier n'est pas afficher
  * @return stdout
  * @scope product
  * @author David.L
  **/
 public function productNoticeNoAddCart(){
  return plxUtils::strCheck($this->aProds[$this->dLang][$this->idProduit]['notice_noaddcart']);
 }

 /**
  * Méthode qui affiche le poid en gramme du produit
  *
  * @return stdout
  * @scope product
  * @author David.L
  **/
 public function productPoidG(){
  return plxUtils::strCheck($this->aProds[$this->dLang][$this->idProduit]['poidg']);
 }

 /** Inutilisé
  * Méthode qui affiche le groupe du produit
  *
  * @return stdout
  * @scope nothing
  * @author David.L
  **/
 public function productGroup(){
  echo plxUtils::strCheck($this->aProds[$this->dLang][$this->idProduit]['group']);
 }

 /**
  * Méthode qui retourne le(s) titre(s) d(u|es) groupe(s) du produit
  *
  * @return array
  * @scope modeles/espacePublic/produit.php (product breadcrumb)
  * @author David.L, Thomas I.
  **/
 public function productGroupTitle(){
  $gt=explode(',',$this->aProds[$this->dLang][$this->idProduit]['group']);
  $tmp=array();
  $home = $this->getParam('name_home_'.$this->dLang);#MyMultiLingue ou non);
  foreach($gt as $gTitle){
   if($gTitle == '000'){# 000 == MyShopHomePage
    $tmp[$gTitle] = $home? $home: $this->getLang('L_PRODUCTS_HOME_PAGE');
    continue;
   }
   elseif($this->aProds[$this->dLang][$gTitle]['active'])
    $tmp[$gTitle]=$this->aProds[$this->dLang][$gTitle]['name'];
  }
  return $tmp;
 }

 /**
  * Méthode qui affiche la date de la dernière modification du produit selon le format choisi
  *
  * @param format format du texte de la date (variable: #minute, #hour, #day, #month, #num_day, #num_month, #num_year(4), #num_year(2))
  * @return stdout
  * @scope product
  * @author David.L
  **/
 public function productDate($format='#day #num_day #month #num_year(4)'){

  # On genere le nom du fichier dont on veux récupérer la date
  $file = PLX_ROOT.$this->aConf['racine_products'].$this->idProduit;
  $file .= '.'.$this->aProds[$this->dLang][$this->idProduit]['url'].'.php';
  # Test de l'existence du fichier
  if(!is_file($file)) return;
  # On récupère la date de la dernière modification du fichier qu'on formate
  echo plxDate::formatDate(date('YmdHi', filemtime($file)), $format);
 }

 /**
  * Méthode qui affiche le chapô du produit
  *
  * @return stdout
  * @scope product
  * @author Thomas I.
  **/
 public function plxShowProductChapo($id = false){

  $productNumber = $id? $id: $this->idProduit;
  # Hook Plugins plxShowProductChapoBegin
  if(eval($this->plxMotor->plxPlugins->callHook(__FUNCTION__ . 'Begin'))) return;

  # On va verifier que le chapô a inclure existe : retro compat
  if(!empty($this->aProds[$this->dLang][$productNumber]['chapo'])){
   # Inclusion du chapô
   ob_start();
   echo $this->aProds[$this->dLang][$productNumber]['chapo'];
   # Hook Plugins plxShowProductChapo
   eval($this->plxMotor->plxPlugins->callHook(__FUNCTION__));
   # On évalue le code. $this->plxMotor est dispo et ici $this est plxMyShop si besoin est ;) #aProds[]
   return eval('?>'.ob_get_clean().'<?php ');
  }

  #products/stars #home of plxMyShop
  if(!$id AND !$productNumber AND $this->plxMotor->mode == 'products'){# AND !$this->idProduit
   # Inclusion du param
   ob_start();
   echo $this->getParam('chapo_home_' . $this->dLang);
   # Hook Plugins plxShowProductChapoProducts
   eval($this->plxMotor->plxPlugins->callHook(__FUNCTION__ . 'Products'));
   # On évalue le code. $this->plxMotor est dispo si besoin est.
   return eval('?>'.ob_get_clean().'<?php ');
  }
 }


 /**
  * Méthode qui inclut le code source du produit
  *
  * @return stdout
  * @scope product
  * @author David.L, Thomas I.
  **/
 public function plxShowProductContent(){

  # Hook Plugins plxShowProductContentBegin
  if (eval($this->plxMotor->plxPlugins->callHook(__FUNCTION__ . 'Begin'))) return;

  #products/stars #home of plxMyShop
  if($this->plxMotor->mode == 'products' OR $this->idProduit == '000'){# AND !$this->idProduit
   # Inclusion du param
   ob_start();
   echo $this->getParam('content_home_' . $this->dLang);
   # Hook Plugins plxShow,ProductContentProducts
   eval($this->plxMotor->plxPlugins->callHook(__FUNCTION__ . 'Products'));
   # On évalue le code. $this->plxMotor est dispo et ici $this est plxMyShop si besoin est ;) #aProds[]
   return eval('?>'.ob_get_clean().'<?php ');
  }

  # On va verifier que la page a inclure est lisible
  if($this->aProds[$this->dLang][$this->idProduit]['readable'] == 1){
   $url_read = '';
   if($this->aLangs && isset($_SESSION['lang'])){
    $url_read = $_SESSION['lang'].'/';
   }
   # On genere le nom du fichier a inclure
   $file = PLX_ROOT.$this->aConf['racine_products'].$url_read.$this->idProduit;
   $file .= '.'.$this->aProds[$this->dLang][$this->idProduit]['url'].'.php';
   if(!is_file($file)){
    # On tente de recuperer le contenu du fichier sans langue.
    $file = PLX_ROOT.$this->aConf['racine_products'].$this->idProduit;
    $file .= '.'.$this->aProds[$this->dLang][$this->idProduit]['url'].'.php';
   }
   # Inclusion du fichier
   ob_start();
   require $file;

   # Hook Plugins plxShowProductContent
   eval($this->plxMotor->plxPlugins->callHook(__FUNCTION__));

   $output = ob_get_clean();
   echo $output;
  } else {
   echo '<p>'.L_STATICCONTENT_INPROCESS.'</p>';
  }
 }

 /**
  * Méthode qui affiche un produit en lui passant son id (si ce produit est active ou non)
  *
  * @param id numérique de la page product
  * @return stdout
  * @scope global
  * @author David.L
  **/
 public function plxShowProductInclude($id){
  # Hook Plugins plxShowProductInclude
  if(eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . 'ProductInclude'))) return;

  $url_read = '';
  if($this->aLangs && isset($_SESSION['lang'])){
   $url_read = $_SESSION['lang'].'/';
  }

  # On génère un nouvel objet plxGlob
  $plxGlob_stats = plxGlob::getInstance(PLX_ROOT.$this->aConf['racine_products'].$url_read);
  if($files = $plxGlob_stats->query('/^'.str_pad($id,3,'0',STR_PAD_LEFT).'\.[a-z0-9-]+\.php$/')){
   include(PLX_ROOT.$this->aConf['racine_products'].$url_read.$files[0]);
  }
  else{
   # on tente sans la langue.
   $plxGlob_stats = plxGlob::getInstance(PLX_ROOT.$this->aConf['racine_products']);
   if($files = $plxGlob_stats->query('/^'.str_pad($id,3,'0',STR_PAD_LEFT).'\.[a-z0-9-]+\.php$/')){
    include(PLX_ROOT.$this->aConf['racine_products'].$files[0]);
   }
  }
 }

 /**
  * Méthode de traitement du hook plxShowStaticListEnd
  * Gere les menus et ajout du lien vers le panier si besoin est
  *
  * @return stdio
  * @author David.L, Thomas I.
  **/
 public function plxShowStaticListEnd(){
  # initialise submenu
  $submenu = boolval($this->getParam('submenu_' . $this->dLang));
  $submenuCategories = '';
  $submenuPanier = '';
  $regex = '~products?[/stars]?|boutique~';
  $baseHook = ucfirst(__FUNCTION__);

  # Hook Plugins plxMyShopPlxShowStaticListEndTop
  if (eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook . 'Top'))) return;

  if ($submenu) {
   $active = '';#First link (if submenu)
   if (!preg_match($regex, $this->plxMotor->mode))
    $active = 'no';
   $active .= 'active';
  }
  #intval Fix if is unconfigured == Warning: A non-numeric value encountered in : plxMyShop.php on line 1472
  $positionMenu = intval($this->getParam('menu_position')) - 1;
  $lienPanier = $this->plxMotor->urlRewrite('?'.$this->lang.'boutique/panier');
  $affPanier = $this->getParam('affPanier');
  $menuPanier = $this->getParam('affichePanierMenu');

  if (!in_array($affPanier, array('pageSepareeStars', 'pageSeparee'))) {
   if( ($this->plxMotor->mode == 'boutique')
    OR ($this->plxMotor->mode == 'products' && $affPanier == 'basPageStars')
    OR ($this->plxMotor->mode == 'product' && strpos($affPanier,'basPage') !== FALSE)
   )
    $lienPanier = $this->plxMotor->urlRewrite('#panier');
  }
  if ($menuPanier) {# Afficher la page panier dans le menu ?
  //~ if (in_array($menuPanier, array('pageSepareeStars', 'pageSeparee', 'partout'))) {
   # ajout du lien vers le panier
   $nomPlugin = __CLASS__;
   $panierSelectionne = (
     ('boutique' === $this->plxMotor->mode)
    && ($nomPlugin === rtrim($this->plxMotor->cible,'/'))#Fix Menu Panier non actif si sélectionné / @ end ?
    && ('panier' === get_class($this->vue))
   );
   $classeCss = ($panierSelectionne ? '' : 'no') . 'active';

   require_once 'classes/vues/panier.php';
   $vuePanier = new panier();
   $vuePanier->plxPlugin = $this;
  //~ }


   $submenuPanier = '<li class="menu-item static menu '.$classeCss.'"><a href="'.$lienPanier.'" title="' . plxUtils::strCheck($vuePanier->titre()) . '">'.$vuePanier->titre().'</a></li>';
   while(1){

    # Hook Plugins plxMyShopPlxShowStaticListEndSubmenuPanier #stop loop #in test
    if (eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook . 'SubmenuPanier'))) break;

    if (!$submenu){
     echo "<?php array_splice(\$menus, $positionMenu, 0, '$submenuPanier'); ?>";
    }
    break;
   }
  }

  # Hook Plugins plxMyShopPlxShowStaticListEndPanier
  if (eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook . 'Panier'))) return;

  $this->donneesModeles['lienPanier'] = $lienPanier;# lien panier top + IN  plxShowLienPanierTop()

  # products/stars
  $submenuHome = '';
  $homeMenu = boolval($this->getParam('afficheProductsMenu'));#bep²
  $myHome = FALSE;
  if($homeMenu){
   $categorieSelectionnee = ('products' === $this->plxMotor->mode);
   $classeCss = ($categorieSelectionnee ? '' : 'no') . 'active';
   $lien = $this->plxMotor->urlRewrite('?'.$this->lang.'products/stars');
   $submenuHome = '   <li class="menu-item static menu '.$classeCss.'"><a href="'.$lien.'" title="'.plxUtils::strCheck($this->getLang('L_PRODUCTS_HOME_PAGE')).'">'.$this->getLang('L_PRODUCTS_HOME_PAGE').'</a></li>'.PHP_EOL;
  }

  # ajout du menu pour accèder aux rubriques
  if (isset($this->aProds[$this->dLang])
   && is_array($this->aProds[$this->dLang])
   && $this->getParam('afficheCategoriesMenu')
  ){

   # Hook Plugins plxMyShopPlxShowStaticListEndCategoriesMenu
   if (eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook . 'CategoriesMenu'))) return;

   foreach(array_reverse($this->aProds[$this->dLang]) as $k=>$v){
    if($homeMenu && !$myHome && $v['readable'] && $v['active'] && empty($v['pcat']) && strpos($v['group'], '000') !== FALSE){
     $myHome=TRUE;
    }
    if (!empty($v['active']) && $v['menu']!='non' && $v['menu']!=''){# Fix show in public menu if not active
     #init
     $k = intval($k);
     $categorieSelectionnee = (
       ('product' === $this->plxMotor->mode)
      && ("product$k/{$v['url']}" === $this->plxMotor->get)
     );
     $classeCss = ($categorieSelectionnee ? '' : 'no') . 'active';
     $lien = $this->plxMotor->urlRewrite('?'.$this->lang."product$k/{$v['url']}");
     $submenuTemp = '   <li class="menu-item static menu '.$classeCss.'"><a href="'.$lien.'" title="'.plxUtils::strCheck($v['name']).'">'.plxUtils::strCheck($v['name']).'</a></li>'.PHP_EOL;
     $submenuCategories .= $submenuTemp;

     # Hook Plugins plxMyShopPlxShowStaticListEndPanierCategorie + stop loop #in test
     if (eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook . 'Categorie'))) continue;

     if (!$submenu){
      echo "<?php array_splice(\$menus, $positionMenu, 0, '$submenuTemp'); ?>";
     }
    }
   }#for each prods

   # Hook Plugins plxMyShopPlxShowStaticListEndMenu
   if (eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook . 'Menu'))) return;

   if ($submenu){
    echo '<?php array_splice($menus, '.$positionMenu.', 0,'."'"
    .PHP_EOL.' <li id="static-myshop" class="static menu menu-item page_item menu-item-object-page menu-item-has-children has-children">'
    .PHP_EOL.'<span class="menu-item static group '.$active.'">'.$this->getParam('submenu_' . $this->dLang).'</span>'
    .PHP_EOL.'  <ul class="sub-menu">'
    .PHP_EOL.($myHome?$submenuHome.'   ':'').$submenuCategories.'   '.$submenuPanier
    .PHP_EOL.'  </ul>'
    .PHP_EOL.' </li>'
    .PHP_EOL."'".') ?>';
   }elseif($myHome){#home link at begining
    echo '<?php array_splice($menus, '.$positionMenu.', 0, \''.$submenuHome.'\'); ?>';
   }

  }# FIN if ajout du menu pour accèder aux rubriques
  elseif($homeMenu){#home link at begining
   echo '<?php array_splice($menus, '.$positionMenu.', 0, \''.$submenuHome.'\'); ?>';
  }
  # Hook Plugins plxMyShopPlxShowStaticListEndEnd
  eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook . 'End'));

 }# FIN public function plxShowStaticListEnd(){

 /**
  * Méthode de choix du modèle de template
  *
  * @param modele en cours
  * @return stdout
  * @scope global
  * @author
  **/
 public function modele($modele){
  if (!isset($this->donneesModeles['pileModeles'])){
   $this->donneesModeles['pileModeles'] = array();
  }

  # One basket #shortcode protect multiples paniers
  if(isset($_SERVER['unPanier']) AND $_SERVER['unPanier'] == $modele){
   RETURN;
  }
  if($modele == 'espacePublic/panier'){
   $_SERVER['unPanier'] = $modele;
  }

  $this->donneesModeles['pileModeles'][] = $modele;
  # fichier du modèle dans le thème
  $racineTheme = PLX_ROOT . $this->plxMotor->aConf['racine_themes'] . $this->plxMotor->style;
  $fichier = $racineTheme . '/modeles/' . __CLASS__ . '/' . $modele . '.php';
  # si le fichier du modèle est inexistant pas dans le thème
  if (!is_file($fichier)){
   $fichier = 'modeles/' . $modele . '.php';# on choisit le fichier par défaut dans le répertoire de l'extension
  }
  $d = $this->donneesModeles;
  require $fichier;
  # Rétablissement des noms des modèles
  array_pop($this->donneesModeles['pileModeles']);
 }
 # One func to send emails of validerCommande()
 public function sendMail($destinataire,$sujet,$message,$headers){
  #if(self::$PLX_DEBUG_MYSHOP) return true;#debug
  # les.pages.perso.chez.free.fr/l-art-d-envoyer-des-mails-depuis-les-pp-de-free.io
  $start_time = time();//free.fr
  $res = mail($destinataire,$sujet,$message,$headers);
  $time = time() - $start_time;//free.fr
  if(strpos($_SERVER["HTTP_HOST"],'free.fr')) $res = $res & ($time>1);//free.fr
  #debug si PLX_DEBUG & Si profil manager+
  if(self::$PLX_DEBUG_MYSHOP){
   $error_get_last = error_get_last();
   if($error_get_last)
    echo __CLASS__.' '.__FUNCTION__.' error_get_last<br />'.implode('<br />',$error_get_last) . PHP_EOL;//debug
   echo '<hr>Mail TO: '.$destinataire.' is '.($res?'':'not ').'send<hr>Subject:<br>'.$sujet.'<hr>Body:<br>'.$message.'<hr>Headers:<br>'.$headers.'<hr><hr>'.PHP_EOL;
   return true;
  }
  return $res;
 }
 # Will send 2 mails (vendor & customer) & save one to DB if all its correct
 public function validerCommande(){
  # Nom de base des hook de la fonction, modifiable par le(s) hook (why not?)
  $baseHook = ucfirst(__FUNCTION__);
  # Hook Plugins plxMyShopValiderCommandeBegin
  if(eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook . 'Begin'))) return;

  # Mini Check if push valid command & basket prod(s)
  if( !isset($_POST['validerCommande'])
   || !isset($_SESSION[__CLASS__]['ncart'][$this->dLang])
   || !isset($_SESSION[__CLASS__]['prods'][$this->dLang])
   || empty($_SESSION[__CLASS__]['ncart'][$this->dLang])
   //~ || (0 === count($_SESSION[__CLASS__]['prods'][$this->dLang]))#Fix old data (in traitement panier) //Warning: count(): Parameter must be an array or an object that implements Countable
   || empty($_SESSION[__CLASS__]['prods'][$this->dLang])
  ){
   return;
  }

  $msgCommand='';

  # Hook Plugins plxMyShopValiderCommande
  if(eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook))) return;

  # Verifie la méthode de paiement
  if ( isset($_POST['methodpayment'])
   && !isset($this->donneesModeles['tabChoixMethodespaiement'][$_POST['methodpayment']])
  ){
   # si la méthode de paiement n'est pas autorisé, choix par défaut
   $_POST['methodpayment'] = current($this->donneesModeles['tabChoixMethodespaiement'][$_POST['methodpayment']]);
  }

  # delivery_date check : Verifie si c'est au minimum aujourd'hui (zéro day delivery) ou date future v1.0.0
  if($this->getParam('delivery_date')){
   # Une date est envoyée?
   $delivery_date_ok = !empty($_POST['delivery_date']);
   if($delivery_date_ok){
    $nbDays = $this->getParam('delivery_nb_days');
    $dd = $_POST['delivery_date'];#epoc unix at 00:00 #non deliverydate + friendly ;)
    $dd = intval(substr($dd, 0, strlen($dd)-3));# - millisecondes
    # Ajoute les secondes de minuit a maintenant
    $adjust = intval((date('H') * 60 * 60) + (date('i') * 60) + date('s'));
    # 1ere verif (si zero day & même jour de livraison, ils sont même égaux)
    $delivery_date_ok = (($dd + $adjust) >= (time() + ($nbDays * 24 * 60 * 60)));#v1.0.0
    # var_dump($delivery_date_ok, date('Y-m-d-H-i-s', $dd + $adjust), date('Y-m-d-H-i-s'));
   }
   if (!$delivery_date_ok){
    $msgCommand.= '<h3 class="msgerror">'. $this->getLang('L_DELIVERY_ERROR_MIN_DATE') .'</h3>';
   }
   elseif(empty($nbDays)){# delivery_nb_days 0 check
    $today = date('Y-m-d');
    $forToday = ($today == date('Y-m-d', $dd));
    $delivery_date_ok = !empty($_POST['delivery_interval']);
    if($forToday AND $delivery_date_ok){
     $ud = explode(' - ', $_POST['delivery_interval']);#user delivery choice
     $ud = explode(':', $ud[0]);#1ere heure
     $ud = ($ud[1]*60) + $ud[0];#en minutes
     $mm = intval($this->getParam('delivery_nb_times'));#minutes mini
     $mm = $mm + (date('H')*60) + date('i');
     $delivery_date_ok = ($mm >= $ud);
    }
    if(!$delivery_date_ok){
     $msgCommand.= '<h3 class="msgerror">'. $this->getLang('L_DELIVERY_ERROR_MIN_TIME') .'</h3>';
    }
   }
   # Redirige au même endroit (aucun envoi) et informe au retour
   if(!$delivery_date_ok){
    $_SESSION[__CLASS__]['msgCommand'] = $msgCommand;
    if(isset($_POST['methodpayment'])) $_SESSION[__CLASS__]['methodpayment'] = $_POST['methodpayment'];
    $_SESSION[__CLASS__]['errorPost'] = $_POST;
    header('Location:' . $_SERVER['REQUEST_URI']);
    exit;
   }
  }# Fin delivery_date check


  # Les festivitées commences ici
  if( (isset($_POST['email']) && $_POST['email']!='')
   && (isset($_POST['firstname']) && self::cdataCheck($_POST['firstname'])!='')
   && (isset($_POST['lastname']) && self::cdataCheck($_POST['lastname'])!='')
   && (isset($_POST['adress']) &&  self::cdataCheck($_POST['adress'])!='')
   && (isset($_POST['postcode']) && self::cdataCheck($_POST['postcode'])!='')
   && (isset($_POST['city']) && self::cdataCheck($_POST['city'])!='')
   && (isset($_POST['country']) && self::cdataCheck($_POST['country'])!='')
   && (!isset($_POST['choixCadeau']) || self::cdataCheck($_POST['nomCadeau'])!='')
   && (
    ('' === $this->getParam('urlCGV'))
    ||
    (  ('' !== $this->getParam('urlCGV'))
     && isset($_POST['valideCGV'])
    )
   )
  ){

   $TONMAIL=$this->getParam('email');
   $TON2EMEMAIL=$this->getParam('email_cc');
   $SHOPNAME=$this->getParam('shop_name');
   $COMMERCANTNAME=$this->getParam('commercant_name');
   $COMMERCANTPOSTCODE=$this->getParam('commercant_postcode');
   $COMMERCANTCITY=$this->getParam('commercant_city');
   $COMMERCANTSTREET=$this->getParam('commercant_street');
   $FILEORDER = $this->getLang('L_FILE_ORDER');#In $TIILEORDER : Commande du, Order of, Commanda del, ...

   #récupération de la liste des produit du panier*
   $totalpricettc = 0;
   $totalpoidg = 0;
   $totalpoidgshipping = 0;
   $productscart = array();

   # *ICI
   foreach ($_SESSION[__CLASS__]['prods'][$this->dLang] as $idP => $nb){
    $productscart[$idP] = array(
     'name' => $this->aProds[$this->dLang][$idP]['name'],
     'pricettc' => floatval($this->aProds[$this->dLang][$idP]['pricettc']) * $nb,//floatval Fix Notice : a non numeric value prod if unedited
     'poidg' => floatval($this->aProds[$this->dLang][$idP]['poidg']) * $nb,//floatval Fix Notice : a non numeric value if prod unedited
     'nombre' => $nb,
    );
    $totalpricettc += $productscart[$idP]['pricettc'];
    $totalpoidg += $productscart[$idP]['poidg'];
   }

   $totalpoidgshipping = $this->shippingMethod($totalpoidg, $totalpricettc, 0);

   $methodpayment = $this->getLang('L_PAYMENT_'.strtoupper($_POST['methodpayment']));

   $message = '';

   # Hook Plugins plxMyShopValiderCommandePost
   eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook . 'Post'));

   $message .= self::cdataCheck($_POST['firstname']).' '.self::cdataCheck($_POST['lastname']).'<br/>'.
   self::cdataCheck($_POST['adress']).'<br/>'.
   self::cdataCheck($_POST['postcode']).' '.self::cdataCheck($_POST['city']).'<br/>'.
   self::cdataCheck($_POST['country']).'<br/>'.
   $this->getLang('L_EMAIL_TEL').
   self::cdataCheck($_POST['tel'])
   .'<br/><br/><strong>'.
   $this->getLang('L_PAIEMENT').' :</strong> '.$methodpayment;

   $messCommon = '<br/><br/>' . (!isset($_POST['choixCadeau'])
    ? $this->getLang('L_EMAIL_NO_GIFT')
    : '<strong>'.$this->getLang('L_EMAIL_GIFT_FOR').' :</strong> '.htmlspecialchars($_POST['nomCadeau'])
   );

   # Hook Plugins plxMyShopValiderCommandeMessage
   eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook . 'Message'));

   if($this->getparam('delivery_date')){
    $messCommon .= '<br/><br/>';
    $messCommon .= '<strong>'.$this->getLang('L_EMAIL_DELIVERYDATE').' :</strong> ';
    $messCommon .= self::cdataCheck($_POST['deliverydate']).'<br/>';
    $messCommon .= '<strong>'.$this->getLang('L_EMAIL_DELIVERYTIME').' :</strong> ';
    $messCommon .= self::cdataCheck($_POST['delivery_interval']).'<br/>';
   }
   # products list
   $poidsTotal = 0.00;
   $messCommon .= '<br/><br/><strong>'.$this->getLang('L_EMAIL_PRODUCTLIST').' :</strong><br/><ul>';
   foreach ($productscart as $k => $v){
    $messCommon.="<li>{$v['nombre']} × ".$v['name'].': '.$this->pos_devise($v['pricettc']). (floatval($v['poidg'])>0?' '. $this->getLang('L_FOR').' ' .$v['poidg'].' '.$this->getLang('KG'):'').'</li>';
    $poidsTotal += floatval($v['poidg']);
   }
   $messCommon .= '</ul>';

   $messCommon .= '<br/><br/>';
   $messCommon .= '<em><strong>'. $this->getLang('L_EMAIL_DELIVERY_COST'). ' : '.$this->pos_devise($totalpoidgshipping). '</strong>';
   $messCommon .= '<br/>';
   if(!$this->getParam('shipping_by_price') AND !empty($poidsTotal))
    $messCommon .= '<strong>'.$this->getLang('L_EMAIL_WEIGHT').' : '.$totalpoidg.$this->getLang('KG').'</strong></em>';
   $messCommon .= '<br/>';
   $messCommon .= '<strong>' . $this->getLang('L_TOTAL_BASKET').' '.$this->pos_devise(($totalpricettc+$totalpoidgshipping)). '</strong>';
   $messCommon .= '<br/><br/>';
   $messCommon .= $this->getLang('L_EMAIL_COMMENT').' : ';

   $messCommon .= $_POST['msg'];

   #Mail de nouvelle commande pour le commerçant.
   $sujet = $this->getParam('newsubject_' . $this->dLang);
   $sujet = !empty($sujet)?$sujet:$this->getLang('L_EMAIL_SUBJECT') . ' ' .$SHOPNAME;
   $destinataire = $TONMAIL.(isset($TON2EMEMAIL) && !empty($TON2EMEMAIL)?', '.$TON2EMEMAIL:'');
   $message .= ($this->shipOverload?'<h3><b class="startw">'.$this->getLang('L_SHIPMAXWEIGHTADMIN').'</b></h3>':'').$messCommon;

   $headers  = 'From: "'.self::cdataCheck($_POST['firstname']).' '.self::cdataCheck($_POST['lastname']).'" <'.$_POST['email'].">\r\n";
   $headers .= 'Reply-To: '.$_POST['email']."\r\n";
   $headers .= "Content-Type: text/html;charset=".PLX_CHARSET."\r\nContent-Transfer-Encoding: 8bit\r\n";


   # Hook Plugins plxMyShopValiderCommandeValid
   eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook . 'Valid'));

   if($this->sendMail($destinataire,$sujet,$message,$headers)){#si envoi au commerçant

    $msgCommand.= '<h3 class="msgyeah">'.$this->getLang('L_EMAIL_CONFIRM_'.strtoupper($_POST['methodpayment'])).'</h3>';
    $commandNumber = 1 + $this->commandNumber(0);

    # Hook Plugins plxMyShopValiderCommandeEmailBegin
    eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook . 'EmailBegin'));

    #Mail de récapitulatif de commande pour le client.
    switch ($_POST['methodpayment']){
     case 'cheque' :
      $status = $this->getLang('L_WAITING');
      $method = $this->getLang('L_PAYMENT_CHEQUE');
      break;
     case 'cash':
      $status = $this->getLang('L_WAITING');
      $method = $this->getLang('L_PAYMENT_CASH');
      break;
     default:
    }

    # Hook Plugins plxMyShopValiderCommandeEmailSwitchPayments
    eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook . 'EmailSwitchPayments'));

    if(!isset($method)){
     $_SESSION[__CLASS__]['msgCommand'] = '<h3 class="msgerror">'. $this->getLang('L_PAYMENT_ERROR') .'</h3>';#A method of payment is required!
     $_SESSION[__CLASS__]['methodpayment'] = $_POST['methodpayment'];
     $_SESSION[__CLASS__]['errorPost'] = $_POST;
     header('Location:' . $_SERVER['REQUEST_URI']);
     exit;
    }

    $message = '<p>'
     . $this->getLang('L_EMAIL_CUST_MESSAGE1')
     . ' <a href="'.$this->plxMotor->racine.'">'.$SHOPNAME.'</a><br/><span class="cust_mess">'
     . $this->getLang('L_EMAIL_CUST_MESSAGE2').' '. $status .' '.$this->getLang('L_EMAIL_CUST_MESSAGE3').'</span></p>';

    if ($_POST['methodpayment']=='cheque'){
     $message .='<p>'. $this->getLang('L_EMAIL_CUST_CHEQUE') .' : '.$COMMERCANTNAME.'<br/>'. $this->getLang('L_EMAIL_CUST_SENDCHEQUE') .' :'.
     '<br/><em>&nbsp;&nbsp;&nbsp;&nbsp;'.$SHOPNAME.''.
     '<br/>&nbsp;&nbsp;&nbsp;&nbsp;'.$COMMERCANTNAME.''.
     '<br/>&nbsp;&nbsp;&nbsp;&nbsp;'.$COMMERCANTSTREET.''.
     '<br/>&nbsp;&nbsp;&nbsp;&nbsp;'.$COMMERCANTPOSTCODE.' '.$COMMERCANTCITY.'</em></p>';
    } elseif ($_POST['methodpayment']=='cash'){
      $message .='<p>'.$this->getLang('L_EMAIL_CUST_CASH').'</p>';
    }

    # Hook Plugins plxMyShopValiderCommandeEmailPayments #here include checkout.php
    eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook . 'EmailPayments'));

    $message .= '<br/><h1><u>'.$this->getLang('L_EMAIL_CUST_SUMMARY').' :</u></h1>'.
    '<br/><strong>'.$this->getLang('L_EMAIL_CUST_ADDRESS').' :</strong><br/>'.self::cdataCheck($_POST['firstname']).' '.self::cdataCheck($_POST['lastname']).'<br/>'.
    self::cdataCheck($_POST['adress']).'<br/>'.
    self::cdataCheck($_POST['postcode']).' '.self::cdataCheck($_POST['city']).'<br/>'.
    self::cdataCheck($_POST['country']).'<br/>'.
    '<strong>Tel : </strong>'.self::cdataCheck($_POST['tel']) .
    '<br/><br/><strong>' . $this->getLang('L_EMAIL_CUST_PAYMENT') . ' : </strong>'. $method;

    $sujet = $this->getParam('subject_' . $this->dLang);
    $sujet = !empty($sujet)?$sujet:$this->getLang('L_EMAIL_CUST_SUBJECT') .' ' . $SHOPNAME;
    $destinataire = $_POST['email'];
    $message .= ($this->shipOverload?'<h3><b class="startw">'.$this->getLang('L_SHIPMAXWEIGHT').'</b></h3>':'').$messCommon;
    $headers  = 'From: "'.$SHOPNAME.'" <'.$TONMAIL.">\r\n";
    $headers .= 'Reply-To: '.$TONMAIL.(isset($TON2EMEMAIL) && !empty($TON2EMEMAIL)?', '.$TON2EMEMAIL:'')."\r\n";
    $headers .= "Content-Type: text/html;charset=".PLX_CHARSET."\r\nContent-Transfer-Encoding: 8bit\r\n";

    # Hook Plugins plxMyShopValiderCommandeEmailShop
    eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook . 'EmailShop'));

    if($this->sendMail($destinataire,$sujet,$message,$headers)){#si envoi au client

     # Save File Order
     //~ $langPath = $this->lang;#no folder on default lang
     $langPath = $this->aLangs?$this->dLang.'/':'';#fix $this->lang (empty) bad folder on default lang multilingue or not
     $nfl=$this->aConf['racine_commandes'].$langPath.date('Y-m-d_H-i-s_').$_POST['methodpayment'].'_'.$totalpricettc.'_'.$totalpoidgshipping.'_A.html';
     $nf=PLX_ROOT.$nfl;#Public

     $ROOT_ADMIN = '../../';#fix downlad (HTML) link when in admin

/*
core/admin/plugin.php?p=plxMyShop&mod=cmd&html=ZnIvMjAyMC0wNi0yNV8yMy0zNS0wNF9jaGVxdWVfMjJfN19BLmh0bWwqYTg4YmE5&html=Li4vLi4vZGF0YS9jb21tYW5kZXMvZnIvMjAyMC0wNi0yNV8yMS0wNC00NV9zdHJpcGVfMTJfN19QLmh0bWwqYzIyMjZm
../../data/commandes/fr/2020-06-25_21-04-45_stripe_12_7_P.html
*/
     # url crypted download ;)
     $urlCommand = $this->plxMotor->racine.'core/admin/plugin.php?p='.__CLASS__.'&amp;mod=cmd&amp;html='.plxEncrypt::encryptId($ROOT_ADMIN.$nfl);
     $refCommand = $this->getLang('L_FILE_REF').' '.$this->getLang('L_FILE_NUM').$commandNumber.($this->aLangs?' · '.$this->dLang:'');

     $dateFormat = $this->getLang('DATEFORMAT');
     $dateCommand = date($dateFormat);

     $TIILEORDER = $FILEORDER.' '.$dateCommand.' · '.$COMMERCANTNAME.' · '.$SHOPNAME;
     $TAGSORDER = explode('·', $TIILEORDER.' · '.$commandNumber.($this->aLangs?' · '.$this->dLang:''));#Array + com Number & (is plxmultilingue + lang)
     $REFORDER = "{$refCommand} <br><a href=\"{$urlCommand}\">{$ROOT_ADMIN}{$nfl}</a>";

     # Hook Plugins plxMyShopValiderCommandeEmailBeginClient
     eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook . 'EmailBeginClient'));

     $this->commandNumber(1, true);#+1 realy
     $thankyou = $this->getParam('thanks_msg_' . $this->dLang);
     $msgCommand .= '<h3 class="msgyeah1">' . ($thankyou? $thankyou: $this->getLang('L_EMAIL_SENT_THANKS')) . '</h3>';#Thank you for your Order
     $msgCommand .= '<h3 class="msgyeah2">'. $this->getLang('L_EMAIL_SENT1') . '</h3>';#green
     if($this->getParam('order_view')){
      $msgCommand .= '<div class="msgyeahR">' . $message . '</div>';#résumé?
     }
     $msgCommand .= '<h3 class="msgyeah3">' . sprintf($this->getLang('L_EMAIL_SENT2'), $TONMAIL) . '</h3>';#spam

     # Hook Plugins plxMyShopValiderCommandeEmailPaymentsClient
     eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook . 'EmailPaymentsClient'));

     $monfichier = fopen($nf, 'w+');
     $commandeContent='<!DOCTYPE html>
<html>
<head>
<title>'.$TIILEORDER.'</title>
<meta charset="'.PLX_CHARSET.'">
<meta name="description" content="'.$FILEORDER.'">
<meta name="keywords" content="'.implode(',', $TAGSORDER).'">
<meta name="author" content="'.__CLASS__.'·'.self::V.'">
</head>
<body>
<hr/>
'.$message."
<hr/>
<a href=\"mailto:$destinataire?subject=ref:$commandNumber\">$destinataire</a>
<hr />$REFORDER
</body>
</html>";

     if(self::$PLX_DEBUG_MYSHOP)#si debug de pluxml actif et Si manager ou + on afffiche la commande (et on stoppe a la fin)
      echo ("#PLX_DEBUG_MYSHOP :<br>command :<br>$nf<br>" . nl2br(htmlspecialchars($commandeContent)));

     # Hook Plugins plxMyShopValiderCommandeEmailSave
     if(eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook . 'EmailSave'))) return;

     fputs($monfichier, $commandeContent);
     fclose($monfichier);
     chmod($nf, 0644);

     # MAJ du nombre d'article en stock pour chaques produits commandés
     $this->editStocks($_SESSION[__CLASS__]['prods'][$this->dLang]);

     # Remove objects present in  in Basket
     unset($_SESSION[__CLASS__]['prods'][$this->dLang]);
     unset($_SESSION[__CLASS__]['ncart'][$this->dLang]);

     # Hook Plugins plxMyShopValiderCommandeEmailEnd
     eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook . 'EmailEnd'));

    }else{
     $msgCommand.= '<h3 class="msgerror">'. $this->getLang('L_EMAIL_ERROR1') .'</h3>';
     $_SESSION[__CLASS__]['errorPost'] = $_POST;
    }
   }else{
    $msgCommand.= '<h3 class="msgerror">'. $this->getLang('L_EMAIL_ERROR2') .'</h3>';
    $_SESSION[__CLASS__]['errorPost'] = $_POST;#echo  '<script type="text/javascript">error=true;</script>';
   }
  }#Fi post email...
  else {
   if ( (!isset($_POST['email']) || empty($_POST['email']) || $_POST['email']=='') ){
    $msgCommand.= '<h3 class="msgerror">'. $this->getLang('L_MISSING_EMAIL') .'</h3>';
   }
   if ( (!isset($_POST['firstname']) ||  self::cdataCheck($_POST['firstname'])=='') ){
    $msgCommand.= '<h3 class="msgerror">'. $this->getLang('L_MISSING_FIRSTNAME') .'</h3>';
   }
   if ( (!isset($_POST['lastname']) ||  self::cdataCheck($_POST['lastname'])=='') ){
    $msgCommand.= '<h3 class="msgerror">'. $this->getLang('L_MISSING_LASTNAME')  .'</h3>';
   }
   if ( (!isset($_POST['adress']) ||  self::cdataCheck($_POST['adress'])=='') ){
    $msgCommand.= '<h3 class="msgerror">'. $this->getLang('L_MISSING_ADDRESS') .'</h3>';
   }
   if ( (!isset($_POST['postcode']) ||  self::cdataCheck($_POST['postcode'])=='') ){
    $msgCommand.= '<h3 class="msgerror">'. $this->getLang('L_MISSING_ZIP')  .'</h3>';
   }
   if ( (!isset($_POST['city']) ||  self::cdataCheck($_POST['city'])=='') ){
    $msgCommand.= '<h3 class="msgerror">'. $this->getLang('L_MISSING_TOWN') .'</h3>';
   }
   if ( (!isset($_POST['country']) ||  self::cdataCheck($_POST['country'])=='') ){
    $msgCommand.= '<h3 class="msgerror">'. $this->getLang('L_MISSING_COUNTRY') .'</h3>';
   }
   if ( (isset($_POST['choixCadeau']) &&  self::cdataCheck($_POST['nomCadeau']) === '') ){
    $msgCommand.= '<h3 class="msgerror">'. $this->getLang('L_MISSING_GIFTNAME') .'</h3>';
   }
   if ( ('' !== $this->getParam('urlCGV')) && !isset($_POST['valideCGV']) ){
    $msgCommand.= '<h3 class="msgerror">'. $this->getLang('L_MISSING_VALIDATION_CGV') .'</h3>';
   }
   $_SESSION[__CLASS__]['errorPost'] = $_POST;#echo  '<script type="text/javascript">error=true;</script>';
  }

  $_SESSION[__CLASS__]['msgCommand'] = $msgCommand;
  $_SESSION[__CLASS__]['methodpayment'] = $_POST['methodpayment'];

  # Hook Plugins plxMyShopValiderCommandeEnd
  eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . $baseHook . 'End'));

  if(self::$PLX_DEBUG_MYSHOP)
   echo '#PLX_DEBUG_MYSHOP : header go to : Location:<a href="' . $_SERVER['REQUEST_URI'].'">' . $_SERVER['REQUEST_URI'].'#panier</a>';# dont reload if sharp # on link
  else
   header('Location:' . $_SERVER['REQUEST_URI'].'#panier');
  exit;
 }# FIN validerCommande()

 public function shippingMethod($kg, $prx, $op = 1){
  $shippingPrice=0.00;
  if($this->getParam('shipping_colissimo')=='0'){
   return (float) $shippingPrice;
  }
  $accurecept = (float) $this->getParam('acurecept');
  if($this->getParam('shipping_by_price')){
   $kg=$prx;#Transform to total price
   #$op=0;#lock display free shipping (in hook)
   #$this->setParam('shipping_colissimo','0');#lock display shipping (kg)(in hook)
  }

  #hook plugin plxMyShopShippingMethod
  if(eval($this->plxMotor->plxPlugins->callHook(__CLASS__ . 'ShippingMethod'))) return;

  if ($kg<=0){
   $shippingPrice=$accurecept;
  }else{
   for($i=1;$i<=$this->getParam('shipping_nb_lines');$i++){
    $num=str_pad($i, 2, '0', STR_PAD_LEFT);
    if ((float)$kg<=(float)$this->getParam('p'.$num)){
     $shippingPrice=((float)$this->getParam('pv'.$num)+$accurecept);
     break;
    }
   }
   #Prevenir si erreur de réglage des frais de port
   $wOrP = $this->getParam('shipping_by_price')?'p':'w';#price Or Weight
   if($this->getParam('freeship'.$wOrP) != '' && $kg<$this->getParam('freeship'.$wOrP)){
    if($kg > 0 && ($this->getParam('p'.$num) * $this->getParam('pv'.$num)) > 0){
     if($kg > $this->getParam('p'.$num)){
      $this->shipOverload = true;
      echo '<b class="startw">' . $this->getLang('L_SHIPMAXWEIGHT') . '</b>';
      return (float) (($kg / $this->getParam('p'.$num)) * $this->getParam('pv'.$num)) + $accurecept;
     }
    }
   }
  }
  return (float) $shippingPrice;
 }

 /**
 * hook interne de shippingMethod() : gratuité des frais de port
 * config et public
 **/
 public function plxMyShopShippingMethod() {
  echo '<?php ';?>
  if($op){
   if(
    (!!($this->getParam('freeshipw')) && $kg>=$this->getParam('freeshipw'))
    OR
    (!!($this->getParam('freeshipp')) && $prx>=$this->getParam('freeshipp'))
   ){
    echo '<p class="msgyeah"><b>'.$this->getLang('L_FREESHIP').'</b></p>';
    return true;#To stop shippingmethod return true ;)
   }
   $freeShipM = '';
   if(!!($this->getParam('freeshipw')) OR !!($this->getParam('freeshipp'))){
    $freeShipM .= '<b class="msgyeah2">'.$this->getLang('L_FREESHIP').'</b>';
   }
   if(!!($this->getParam('freeshipw'))){
    $freeShipM .= '&nbsp;'.$this->getLang('L_A').'&nbsp;<b class="msgyeah2">'.$this->getParam('freeshipw').'&nbsp;'.$this->getLang('KG').'</b>';
   }
   if(!!($this->getParam('freeshipp'))){
    if(!!($this->getParam('freeshipw')))
     $freeShipM .= '&nbsp;'.$this->getLang('L_AND');
    $freeShipM .= '&nbsp;'.$this->getLang('L_A').'&nbsp;<b class="msgyeah2">'.$this->pos_devise($this->getParam('freeshipp')).'</b>';
   }
   if(!!($freeShipM))
    echo '<p>'.$freeShipM.'</p>';
   unset($freeShipM);
  }
?><?php
 }

 public function traitementAjoutPanier(){
  if (!isset($_POST['ajouterProduit'])) return;
  if (!isset($_SESSION)) session_start();
  if (!isset($_SESSION[__CLASS__]['prods'][$this->dLang])) $_SESSION[__CLASS__]['prods'][$this->dLang]= array();
  if (!isset($_SESSION[__CLASS__]['ncart'][$this->dLang])) $_SESSION[__CLASS__]['ncart'][$this->dLang]= 0;
  $nombre = $_POST['nb'];
  $_SESSION[__CLASS__]['ncart'][$this->dLang] += $nombre;
  $_SESSION[__CLASS__]['prods'][$this->dLang][$_POST['idP']] = $nombre;
  $_SESSION[__CLASS__]['msgProdUpDate'] = TRUE;
  header('Location:' . $_SERVER['REQUEST_URI']);
  exit;
 }

 public function traitementPanier(){
  if (!isset($_SESSION)) session_start();
  # Ajout ds le du panier
  if(isset($_POST['ajouterProduit'])) {
   $this->traitementAjoutPanier();
  }
  # Mise a jour des variables de sessions du panier
  if (isset($_SESSION[__CLASS__]['prods'][$this->dLang])){#si on a des produits dans la sessions
   foreach ($_SESSION[__CLASS__]['prods'][$this->dLang] as $pId => $nb) {#on boucle dessus
    # Si Produit désactivé/supprimé/indisponible(noAddCartButton) entre temps
    if (!isset($this->aProds[$this->dLang][$pId]) OR $this->aProds[$this->dLang][$pId]['active']==0 OR $this->aProds[$this->dLang][$pId]['noaddcart']==1){
     $_SESSION[__CLASS__]['ncart'][$this->dLang] -= $nb;#on recalcule le nb de prod
     unset($_SESSION[__CLASS__]['prods'][$this->dLang][$pId]);#on efface sa variable de session
    }
   }
   # Supprimé par mini panier
   if(isset($_POST['remProd']) && !empty($_POST['idP']) && isset($_SESSION[__CLASS__]['prods'][$this->dLang][$_POST['idP']])){
    $_SESSION[__CLASS__]['ncart'][$this->dLang] -= $_SESSION[__CLASS__]['prods'][$this->dLang][$_POST['idP']];#on recalcule le nb de prod
    unset($_SESSION[__CLASS__]['prods'][$this->dLang][$_POST['idP']]);#on efface sa variable de session
    header('Location:' . $_SERVER['REQUEST_URI']);
    exit;
   }
   if(empty($_SESSION[__CLASS__]['prods'][$this->dLang]))
    $_SESSION[__CLASS__]['ncart'][$this->dLang] = 0;
  }

  # v0.13.2 compat #untested (v1.0.0)
  if (!isset($_SESSION[__CLASS__]['ncart'][$this->dLang])
   && isset($_SESSION[__CLASS__]['ncart'])#old is present
   && is_numeric($_SESSION[__CLASS__]['ncart'])#old, #now is 2 letters
  ){
    $prods = $_SESSION[__CLASS__]['prods'];#save
    $ncart = $_SESSION[__CLASS__]['ncart'];#olds data
    unset($_SESSION[__CLASS__]);#clear old session
    $_SESSION[__CLASS__]['prods'][$this->dLang] = $prods;#restore to new formula
    $_SESSION[__CLASS__]['ncart'][$this->dLang] = $ncart;
    unset($prods, $ncart);
  }#FI v0.13.2 compat

  # Traitements
  if (isset($_POST['retirerProduit'])){
   $cles = array_keys($_POST['retirerProduit']);
   $idP = array_pop($cles);
   if (isset($_SESSION[__CLASS__]['prods'][$this->dLang][$idP])){
    $_SESSION[__CLASS__]['ncart'][$this->dLang] -= $_SESSION[__CLASS__]['prods'][$this->dLang][$idP];
    unset($_SESSION[__CLASS__]['prods'][$this->dLang][$idP]);
    $_SESSION[__CLASS__]['msgProdUpDate'] = TRUE;
   }
   header('Location: ' . $_SERVER['REQUEST_URI']);
   exit;
  }
  if (isset($_POST['recalculer'])){
   foreach ($_POST['nb'] as $idP => $nb){
    $nb = floor($nb);
    $nb = max(0, $nb);
    if (isset($_SESSION[__CLASS__]['prods'][$this->dLang][$idP])){
     $_SESSION[__CLASS__]['ncart'][$this->dLang] -= $_SESSION[__CLASS__]['prods'][$this->dLang][$idP];
     if (0 === $nb){
      unset($_SESSION[__CLASS__]['prods'][$this->dLang][$idP]);
     } else {
      $_SESSION[__CLASS__]['ncart'][$this->dLang] += $nb;
      $_SESSION[__CLASS__]['prods'][$this->dLang][$idP] = $nb;
     }
     $_SESSION[__CLASS__]['msgProdUpDate'] = TRUE;
    }
   }
   header('Location:' . $_SERVER['REQUEST_URI']);
   exit;
  }
 }
}