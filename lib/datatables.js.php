<script>
!window.jQuery && document.write('<script src="<?php echo PLX_PLUGINS.$plxPlugin->plugName ?>/js/jquery-3.3.1.min.js"><\/script>');
</script>
<script src="<?php echo PLX_PLUGINS.$plxPlugin->plugName ?>/js/featherlight-1.7.14.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo PLX_PLUGINS.$plxPlugin->plugName ?>/js/jquery.dataTables-1.10.21.min.js" type="text/javascript"></script>
<script src="<?php echo PLX_PLUGINS.$plxPlugin->plugName ?>/js/dataTables.responsive-2.2.5.min.js" type="text/javascript"></script>
<?php /*
<!--
<script type="text/javascript" src="//code.jquery.com/jquery-latest.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="//cdn.rawgit.com/noelboss/featherlight/1.7.14/release/featherlight.min.js" charset="utf-8"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
-->
//js 4 cdn css exemple
 "<link type='text/css' media='screen' rel='stylesheet' href='//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css' />"+
 "<link type='text/css' media='screen' rel='stylesheet' href='//cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css' />"+
 "<link type='text/css' media='screen' rel='stylesheet' href='//cdn.rawgit.com/noelboss/featherlight/1.7.14/release/featherlight.min.css' />"
*/
$lngs = '';//var_export($aLangs,false);
foreach($aLangs as $lang)
 $lngs.=($plxPlugin->aLangs)?"'_$lang',":'';//table tabs vars
$lngs = trim($lngs,',');
?>
<script type="text/javascript" class="init">
$(document).ready(function(){
 $("head link[rel='stylesheet']").last().after("<style>"+
 ".dataTables_wrapper{position: static !important;}"+
 ".lightbox { display: none; }.featherlight .featherlight-content {min-width:51%;}"+
 "</style>"+
 "<link type='text/css' media='screen' rel='stylesheet' href='<?php echo PLX_PLUGINS.$plxPlugin->plugName ?>/css/jquery.dataTables-1.10.21.min.css' />"+
 "<link type='text/css' media='screen' rel='stylesheet' href='<?php echo PLX_PLUGINS.$plxPlugin->plugName ?>/css/responsive.dataTables-2.2.5.min.css' />"+
 "<link type='text/css' media='screen' rel='stylesheet' href='<?php echo PLX_PLUGINS.$plxPlugin->plugName ?>/css/featherlight-1.7.14.min.css' />");
 var langs = [<?php echo !empty($lngs)?$lngs:"''"; ?>];
 langs.forEach(function(item, index, array) {
  this['table'+item] = $('#myShop-table' + item).DataTable({// DataTable dynamic lang var
   "order": [[ 1, "desc" ]],
   "responsive": true,
   "language":{
    "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/<?php $plxPlugin->lang('L_DATABLEJS'); ?>.json"
   }
  });
  this['table'+item].columns().every(function(){// Apply the search
  //console.log('table.columns.every',this.value);
   if($(this).text()!=''){
    var that = this;
    $('input',this.footer()).on('keyup change',function(){
     if(that.search() !== this.value){
      that
       .search(this.value)
       .draw();
     }
    });
   }
  });
 });
});
</script>