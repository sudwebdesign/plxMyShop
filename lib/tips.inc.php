<?php if(!defined('PLX_ROOT')) exit;//Part of plxMyShop plugin for PluXml
$greffe = isset($this)?$this:$plxPlugin;
$juneUrl = PLX_PLUGINS . get_class($greffe) . '/images/june.png';//https://g1.duniter.fr/#/app/wot/C4UAKZW8yteocrV3LCbtJBLGjc5XG8Twrk2Ynv9t1gDr/ThomasIngles
?>
<span id="contribute" style="position:fixed;bottom:0.16em;right:0.64em;">
 <span id="june" title="<?php $greffe->lang('L_CONTRIBUTE');?> <?php $greffe->lang('L_JUNE');?>.">
  <a rel="noreferrer" href="https://demo.cesium.app/#/app/wot/C4UAKZW8yteocrV3LCbtJBLGjc5XG8Twrk2Ynv9t1gDr/ThomasIngles"><img alt="<?php $greffe->lang('L_JUNE');?>." src="<?php echo $juneUrl ?>"></a>
 </span>
</span>