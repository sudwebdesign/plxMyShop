<?php if(!defined('PLX_ROOT')) exit; ?>
<p class="in-action-bar page-title">Aide de plxMyShop</p>
<p style="color:orange"><i>Cette aide n'est pas exhaustive de toutes les possibilités...</i></p>
<h1><span>Sommaire</span></h1>
<ul>
 <li><a href="#2.installation">Installation</a></li>
 <li><a href="#3.configuration">Configuration</a></li>
 <li><a href="#4.product">Créer un produit</a></li>
 <li><a href="#5.group">Créer un groupe de produit</a></li>
 <li><a href="#6.orders">Les commandes</a></li>
</ul>
<h1><a name="2.installation"></a><span>Installation</span></h1>
Extraire l'archive zip dans le répertoire <b>plugins</b> de PluXml. Ça doit être fait 😉.
<br />Ensuite dans l'administration de PluXml dans le menu <b>Paramètres&gt;plugins</b>,
dans la section <b>Plugins inactifs</b>, coché et activer plxMyShop.
<br />Une fois activé plxMyShop se trouvera dans la section <b>Plugins actifs</b>.

<h1>Pour afficher le mini panier dans votre thème</h1>
Éditez le fichier de votre thème "sidebar.php". Ajoutez y le code suivant à l'endroit où vous souhaitez voir apparaître le mini panier :</p>
<pre>
 &lt;?php eval($plxShow-&gt;callHook('plxMyShopShowMiniPanier')); ?&gt;
</pre>

<h1><a name="3.configuration"></a><span>Configuration</span></h1>
Dans <b>Plugins actifs </b>ou<b> inactifs </b>cliquez sur <b>Configuration</b> de plxMyShop. Dans cette page vous pourrez configurer :
<ul>
 <li><a href="#3.0.shop">les informations relatives au commerçant</a></li>
 <li><a href="#3.0.modules">les modules paiement/livraison</a>
<ul>
 <li><a href="#3.0.ships">Module de livraison</a></li>
 <li><a href="#3.1.ports">Module de Frais de ports</a></li>
 <li><a href="#3.2.payments">Module(s) de paiement(s)</a></li>
</ul>
</li>
 <li><a href="#3.3.mails">Les Courriels de commande pour le client et le commerçant</a></li>
 <li><a href="#3.4.menu.groups">La position dans le menu pour les groupes</a></li>
 <li><a href="#3.5.template">Le template par défaut pour les pages produit et groupe</a></li>
 <li><a href="#3.6.theme">La boutique à mes couleurs</a></li>
</ul>
<br /><i>Remarque : Lorsque vous êtes administrateur, le bouton <span style="font-size:x-large">✔</span> apparait au menu principal, si plxMyShop est actif 😉.</i>
<h2><a name="3.0.shop"></a> Informations relatives au commerçant</h2>
Entrer dans les différents champs les informations d'adressage du commerçant utilisés en entre autres par le module chèque. Veuillez aussi renseigner le nom de la boutique.

<h2><a name="3.0.modules"></a>Modules paiement/livraison</h2>
Activer ou pas les modules de paiement(s)/livraison désirés.
<h3><a name="3.0.ships"></a>Module de livraisons</h3>
Les réglages sont basiques.
<br />Une fois activé, il suffit d'indiquer dans le tableau les dates/heures (minutes) des champs correspondants.
<br />La particularité réside dans le fait que vous pouvez mettre des <i>dates indisponibles sur le calendrier</i>.

<h3><a name="3.1.ports"></a>Module de frais de ports</h3>
Les réglages sont assez simples.
<br />Une fois activé, il suffit d'indiquer dans le tableau les poids/prix et les tarifs qui correspondent.
<br />La particularité réside dans le fait que vous pouvez mettre à jour vos tarifications de livraison à la volé
et qu'ils peuvent être liés soit au poids ou prix total du panier.

<h3><a name="3.2.payments"></a>Paiements</h3>
<i>Depuis la version 1.0, aucun module de paiement (externe) est inclus avec plxMyShop.</i>
<i>Seul les paiements par chèques ou espèces sont présent.</i>

<ul>
 <li>Pour utiliser les paiements externes :</li>
 <ul>
  <li>Des plugins dédiés s(er)ont créés, voir le <a href="http://sudwebdesign.free.fr/depot.php#shop">dépôt</a> ou le <a href="https://forum.pluxml.org/discussions/tagged/myshop">forum</a>.</li>
  <li>Une fois le plugin installé et activé, R.D.V. dans sa configuration ou dans la configuration de MyShop (onglet Paiements).</li>
  <li>Ou vous pouvez créer votre plugin dédié et/ou demander de l'aide sur le <a href="https://forum.pluxml.org/discussion/6794/">forum</a>.</li>
 </ul>
</ul>
<i>Note importante : Veuillez au minimum activer un moyen de paiement.</i>
<h2><a name="3.3.mails"></a>Mails de commande pour le client et le commerçant</h2>
Entrez les adresse mail utilisé pour recevoir les mails des commandes effectué. Vous pouvez aussi définir le titre de vos mails, pour le mail commerçant ainsi que celui du client.
<h2><a name="3.4.menu.groups"></a>Les menus</h2>
Vous pouvez de définir la position par défaut des menus.
<br />Lorque le nom du menu est renseigné, les groupes s'afficherons en sous-menu.
<br />Il est possible à la création d'un groupe de l'afficher dans le menu principal du site si l'option "Afficher les groupes dans le menu" est active.
<br />Il est possible d'afficher le lien du panier dans le menu ainsi que d'afficher le lien "votre panier" en haut des pages (de la boutique)
<h2><a name="3.5.template"></a>Template par défaut pour les pages produit et groupe</h2>
Cette option vous permettra de définir le template utilisé par défaut par vos page de fiche produit et groupe de produit.
<h2><a name="3.6.theme"></a>La boutique à mes couleurs</h2>
Il est possible d'intégrer les modèles dans votre thème afin que plxMyShop soit a vos couleurs.
Cela évitera de tout recommencer a chaque mise a jour.
<br />Pour ce faire :
<ul>
 <li>Créez un dossier <i>modeles/<b>plxMyShop</b></i> dans votre thème</li>
 <li>Dupliquer le(s) fichier(s) du dossier <i>modeles</i> de plxMyShop souhaité(s)</li>
</ul>
<i>Note importante : veuillez respecter l'architecture des dossier/fichier pour qu'il(s) soi(en)t pris en compte(s) par plxMyShop.</i>
<br /><i>Note subtile : Lors des mises a jour, il se peut q'un modèle évolue, veuillez en rapporter les changements principaux dans vos fichiers de modèles personnels.</i>
<h1><a name="4.product"></a><span>Créer un produit</span></h1>
Une fois plxMyShop d'activé, un nouveau menu apparaît dans l'administration de PluXml.
<br />Ce menu porte le nom de votre boutique <i>(My Shop et en dessous des pages statiques au début)</i> ainsi que le numéro de version de plxMyShop.
<br />Dans ce menu en haut de page vous avez quatre boutons :
<ul style="font-size:x-large">
 <li>⚛ <sup>pour gérer les Produits</sup></li>
 <li>★ <sup>pour gérer les Groupes</sup></li>
 <li>✍ <sup>pour gérer les Commandes</sup></li>
 <li>✔ <sup>pour régler la la boutique</sup></li>
</ul>
Dans la liste des <b>produits</b>, pour créer un produit il suffit de faire la même chose que pour créer une page statique.
<br />Renseigner le nom de votre produit, activez le ou pas et ensuite cliquer sur le bouton <b>Modifier la liste des produits</b>.
<br />Une fois créé cliquer sur le lien <b>éditer</b> à la droite du produit (ou sur son identifiant numérique a gauche) pour accéder à sa page d'édition.

<br />Dans la page d'édition du produit, veuillez renseigner le lien de l'image du produit, que le lien soit en relatif ou absolue ne pose aucun problème,
en utilisant le bouton vous pourrez directement choisir une image disponible dans votre zone de média.
<br />Ensuite saisir une description et renseignez le prix a affiché du produit et si besion est, le poids et son stock.

<br />Si le poids n'est pas renseigner ou égal à zéro il ne sera pas prix en compte.
<br />Ensuite comme pour les pages statiques, veuillez renseigner le template utilisé et les informations des balises méta.

<br />Cliquer sur le bouton <b>Enregistrer ce produit</b>.

<br />Pour visualiser le produit dans la partie publique de votre site,
cliquer sur le lien <b>Voir</b> à coté du lien <b>Éditer</b> à droite de chaque produits de la liste.

<h1><a name="5.group"></a>Créer un groupe de produit</h1>
A quelques détails près le processus de création est exactement le même que celui d'un produit.

<br />Pour attribuer un produit à un groupe,
il vous suffira de cocher le(s) groupe(s) en question dans le champs <b>Groupes de ce produit</b> lorsque vous modifiez un produit.
<br /><b>Par défaut le groupe <i>Produit d'accueil</i> est coché</b>. il s'agit d'un groupe spécial (le 000) qui ne peut être supprimé.
<i>(Voir <b>Accueil</b> de la configuration pour le régler)</i>

<br />Comme indiqué plus haut, vous avez la possibilité d'afficher vos groupes dans le menu principal du site.

<h1><a name="6.orders"></a>Les commandes</h1>
La liste des commandes vous permettra d'avoir un visuel rapide des commandes réalisées.

<ul>
 <li>Vous pouvez les visualisées (il s'agit du courriel (html) envoyé au client un peu modifié).</li>
 <li>Vous pouvez modifier leurs statuts : Attente/Payée/Annulée (Attention, cela modifie chaque fois l'entête de la commande).</li>
 <li>Il est possible de réaliser des sauvegardes au format zip de toutes ou de celle(s) sélectionnée(s).</li>
 <li>Il est possible de toutes les supprimées d'un coups ou juste celle(s) sélectionnée(s).</li>
</ul>