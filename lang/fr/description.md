# plxMyShop version 1.0.0 du 18/07/2020

## Une boutique en ligne pour PluXML

Cette extension au code source libre fournit les fonctionnalités suivantes :
- liste de produits classés par catégorie
- champs du produit : image, chapô, description, prix, poids
- devise et prix varient selon la langue du site (peut-être multi-monnaies avec le plugin plxMyMultiLingue)
- paiement par chèque, virement ou par l'intermediaire de plugins dédiés
- calcul des frais de port en fonction du poids (optionnel)
- choix de la date de livraison (optionnel)
- e-mails au client et au vendeur à chaque commande
- shortcode pour placer le bouton `Ajouter au panier` d'un produit dans une page statique

Exemples de sites utilisant plxMyShop :
- https://longslowbakery.co.uk/
- http://institut-perle-de-beaute.fr/

## Personnalisation

Chaque produit et chaque groupe peut utiliser un template de page statique différent.
Vous trouverez aussi dans le répertoire `exemplesTemplate`, des exemples de template pour afficher sur une seule page tous les produits ou la liste des groupes de la boutique.

Pour des modifications plus importantes, vous pouvez surcharger tous les fichiers du répertoire `modeles` avec un fichier placé dans le thème.
Par exemple pour modifier le fichier `espacePublic/boucle/produitRubrique.php` qui présente un résumé du produit dans la rubrique :
1. à la racine du thème, créez un répertoire `modeles/plxMyShop/espacePublic/boucle`
2. copiez le(s) fichier(s) de base(s) dans le répertoire en conservant le nom
3. vous pouvez maintenant modifier le fichier du thème qui sera pris en compte automatiquement quand ce thème est activé

## Support

Pour toute question concernant l'utilisation de l'extension, vous pouvez vous rendre dans cette discussion sur le forum de PluXML :
https://forum.pluxml.org/discussion/6794/plugin-plxmyshop-1-0

Vous trouverez les modifications apportées par les différentes versions dans le fichier suivant :
https://codeberg.org/sudwebdesign/plxMyShop/src/branch/master/readme/CHANGELOG

## Contributions

Vous pouvez contribuer au projet de différentes façons :
- En nous aidant à tenir les traductions à jour ou bien en proposant une nouvelle langue :
https://codeberg.org/sudwebdesign/plxMyShop/issues
- En corrigant des erreurs de code ou en codant des nouvelles fonctionnalités :
https://codeberg.org/sudwebdesign/plxMyShop/pulls

