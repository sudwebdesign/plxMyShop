<?php
$LANG = array(
'L_LOAD_MULTILINGUE_BEFORE'         => 'Fout Laad de plxMyMultilingual plug-in voordien',

# Formatage des prix
'NOMBRE_DECIMALES'                  => '2',
'POINT_DECIMAL'                     => ',',
'SEPARATEUR_MILLIERS'               => ' ',
'MONEY'                             => '€',
'MONEY_POSITION'                    => '0',
'KG'                                => 'Kg',

'L_PAGE_TITLE'                      => 'Beheer producten',
'L_ADMIN_MENU_TOOTIP'               => 'Beheer mijn web-commerce (producten, groepen en bestellingen)',
'L_LANG_UNAVAILABLE'                => 'Taal niet beschikbaar %s',
'L_DEFAULT_NEW_PRODUCT_URL'         => 'nieuw product',
'L_CONFIG_PRIX_BASE'                => 'Beginprijs',
'L_MENU_PRODUCTS_TITLE'             => 'Lijst en wijziging van producten',
'L_MENU_PRODUCTS'                   => 'Producten',
'L_MENU_CATS'                       => 'Groepen',
'L_MENU_ORDERS'                     => 'Bestellingen',
'L_MENU_CONFIG'                     => 'Configuratie',
'L_VIEW_ONLINE'                     => 'Bekijk %s op de site',
'L_DATE'                            => 'Gedateerd',
'L_PAIEMENT'                        => 'Betaalmiddelen',
'L_MONTANT'                         => 'Bedrag',
'L_ACTIONS'                         => 'Acties',
'L_BY_DEFAULT'                      => 'Standaard',
'L_AND'                             => 'en',
'L_OR'                              => 'of',
'L_A'                               => 'Bij',
'L_OPTIONEL'                        => 'optioneel',

'L_EMAIL'                           => 'E-mail ontvanger (s) *',
'L_EMAIL_CC'                        => 'Ontvanger (s) in kopie van de e-mail',
'L_EMAIL_BCC'                       => 'Ontvanger (s) in verborgen kopie van de e-mail',
'L_EMAIL_SUBJECT'                   => 'Nieuwe bestelling',
'L_ERR_EMAIL'                       => 'Vul een geldig e-mailadres in',
'L_DEFAULT_OBJECT'                  => 'Overzicht van de bestelling',
'L_DEFAULT_CONFIRMATION_OBJECT'     => 'Order bevestiging',
'L_TEMPLATE'                        => 'Sjabloon',
'L_UNKNOWN_PRODUCT'                 => 'Dit product bestaat niet of niet meer!',
'L_PRODUCTCONTENT_INPROCESS'        => 'Dit product wordt momenteel geschreven.',

'L_MENU_SHOW_ALL'                   => 'Toon of verberg alle tabbladen',
'L_CONTRIBUTE'                      => 'Bijdragen',
'L_JUNE'                            => 'Donatie in Ğ1, 1e gratis digitale valuta',
'L_NOSCRIPT'                        => 'Activeer a.u.b. het JavaScript van uw browser om van alle mogelijkheden te profiteren',

# admin produits et groupes
'L_ALL_GROUPS'                      => 'Alle groepen ...',
'L_SPECIFIC_GROUPS_TABLE'           => 'Specifieke groepen',
'L_ALL_GROUPS_TABLE'                => 'Alle producten',
'L_SEARCH_PLACEHOLDER'              => 'ID of titel',
'L_LIST_GROUPS'                     => 'Groepen',
'L_CATS_PAGE_TITLE'                 => 'Beheer groepen',
'L_PRODUCTS_PAGE_TITLE'             => 'Beheer producten',
'L_PRODUCTS_ID'                     => 'Gebruikersnaam',
'L_PRODUCTS_GROUP'                  => 'Groep',
'L_PRODUCTS_TITLE'                  => 'Kop',
'L_PRODUCTS_URL'                    => 'URL',
'L_PRODUIT_ACTIF'                   => 'Actief',
'L_PRODUCTS_ORDER'                  => 'Bestellen',
'L_PRODUCTS_MENU'                   => 'Menu',
'L_PRODUCTS_ACTION'                 => 'Actie',
'L_CART_NAME_FIELD'                 => 'Titel van de pagina "mijn winkelmandje"',
'L_PRODUCTS_NAME_FIELD'             => 'Pagina titel',
'L_PRODUCTS_TEMPLATE_FIELD'         => 'Sjabloon',
'L_PRODUCTS_PAGE_HOME'              => 'Definieer als een welkom product',
'L_PRODUCTS_HOME_PAGE'              => 'Welkom   product',
'L_PRODUCTS_SRC_TITLE'              => 'Bewerk de broncode van dit product',
'L_PRODUCTS_SRC'                    => 'Bewerk',
'L_PRODUCTS_NEW_PAGE'               => 'Nieuw product',
'L_PRODUCTS_UPDATE'                 => 'Wijzig de lijst met producten',
'L_ERR_PRODUCT_ALREADY_EXISTS'      => 'Titel van het reeds gebruikte product',
'L_PRODUCTS_IMAGE'                  => 'Presentatie-afbeelding',
'L_PRODUCTS_IMAGE_CHOICE'           => 'Kies een afbeelding',
'L_PRODUCTS_SHORTCODE'              => 'Shortcode voor artikel en statische pagina',
'L_PRODUCTS_BASKET_BUTTON'          => 'In winkelwagen',
'L_PRODUCTS_ITEM_INSTOCK'           => 'Aantal producten op voorraad (optioneel)',
'L_PRODUCTS_ITEM_INSTOCK_HINT'      => 'Bij de laatste bestelling uit voorraad is het product niet meer leverbaar! De knop Toevoegen aan winkelwagentje wordt automatisch verborgen. Deactiveer de maskering als de voorraad is geleegd en vervolgens is gevuld. (Indien leeg gelaten, is het productvoorraadsysteem inactief).',
'L_PRODUCTS_BASKET_NO_BUTTON'       => 'Toevoegen aan winkelwagentje',
'L_PRODUCTS_CATEGORIES'             => 'Groepen van dit product',
'L_PRODUCTS_STOCK'                  => 'Voorraad',
'L_PRODUCTS_STOCK_NO'               => 'Inactief',
'L_PRODUCTS_PRICE'                  => 'Alle belastingen zijn inclusief prijs.',
'L_PRODUCTS_WEIGHT'                 => 'Gewicht',
'L_CONFIRM_DELETE'                  => 'Bevestig verwijdering?',

'L_COMMAND_OF'                      => 'Bevel van',

# editionProduitAdmin.php
'L_PRODUCT_NAME'                    => 'productnaam',
'L_CAT_NAME'                        => 'Naam van de groep',
'L_PRODUCT_BACK_TO_PAGE'            => 'Ga terug naar de productlijst',
'L_CAT_BACK_TO_PAGE'                => 'Terug naar groepen',
'L_PRODUCT_UNKNOWN_PAGE'            => 'Dit product bestaat niet of niet meer!',
'L_PRODUCT_TITLE'                   => 'Producteditie',
'L_CAT_TITLE'                       => 'Groep bewerken',
'L_THUMBNAIL_SELECTION'             => 'Selecteer een afbeelding',
'L_PRODUCT_VIEW_PAGE_ON_SITE'       => 'Bekijk product %s op de site',
'L_CAT_VIEW_PAGE_ON_SITE'           => 'Zie de groep %s op de site',

'L_PRODUCT_UPDATE'                  => 'Registreer dit product',
'L_CAT_UPDATE'                      => 'Bewaar deze groep',
'L_CONTENT_OF_TAG'                  => 'Inhoud van de tag',
'L_FOR_THIS_CAT'                    => 'voor deze groep',
'L_FOR_THIS_PRODUCT'                => 'voor dit product',

'L_GROUP'                           => 'Groep',

# Commandes
'LIST_ORDERS'                       => 'Lijst met opdrachten',
'DATEFORMAT'                        => 'd/m/Y H:i:s',
'L_DATABLEJS'                       => 'Danish',
'L_STATUS'                          => 'Status',
'L_UNKNOWN'                         => 'Onbekend',
'L_SET_PAID'                        => 'Betaald',
'L_PAID'                            => 'Betaald',
'L_SET_CANCELED'                    => 'Is geannuleerd',
'L_CANCELED'                        => 'Geannuleerd',
'L_SET_AWAIT'                       => 'Aan het wachten',
'L_AWAIT'                           => 'Aan het wachten',

'L_REFRESH'                         => 'Herlaad de pagina',
'L_DOWNLOAD'                        => 'Downloaden',
'L_CACHE_ZIP'                       => 'Besparen',
'L_CACHE_ZIPDEL'                    => 'Back-upbestand verwijderen',
'L_CACHE_ZIPPED'                    => 'Het orderbestand is opgeslagen',
'L_CACHE_ZIP_PB'                    => 'er is een fout opgetreden',
'L_CACHE_ZIP_PC'                    => 'Kan geen back-upbestand maken',
'L_CACHE_ZIP_PW'                    => 'Kan bestand niet in back-up plaatsen',
'L_ZIP_SERVER'                      => 'Backup zip-bestand aanwezig op de server',
'L_CLEAN_CACHE'                     => 'Leeg',
'L_CACHE_CLEANED'                   => 'Leeg orderbestand',
'L_FILE_CLEANED'                    => 'Bestand verwijderd',
'L_CACHE_CONFIRM_DEL'               => 'Wilt u de bestellingen leegmaken?',
'L_CACHE_CONFIRM_DEL_2'             => 'Toutes les commandes, c’est sur?',
'L_CACHE_CONFIRM_DEL_3'             => 'Kopieer het onderstaande woord om alle bestellingen te bevestigen en te verwijderen',

# Relatif au panier
'L_PANIER_POS_BOTTOM'               => 'Onderaan de producten en groepen',
'L_PANIER_POS_STARS'                => 'Onderaan de producten, welkom en groepen',
'L_PANIER_POS_SEPARATE'             => 'Op een aparte pagina',
'L_PANIER_POS_BOTH'                 => 'Onderaan de producten, groepen en op een aparte pagina',
'L_PANIER_POS_BOTH_STARS'           => 'Onderaan de pagina’s, welkom en op een aparte pagina',
'L_PANIER_RECALCULER'               => 'Herbereken het totaal',

# config onglets
'L_SHOP'                            => 'kledingwinkel',
'L_DELIVERY'                        => 'Poorten',
'L_PAYMENTS'                        => 'Betalingen',
'L_EMAILS'                          => 'E-mails',
'L_ORDERS'                          => 'Bestellingen',
'L_MENU'                            => 'Menu’s',
'L_PAGES'                           => 'Pagina’s',
'L_HOME'                            => 'ontvangst',
'L_DATA'                            => 'Gegevens',

'L_CONFIG_REZET'                    => 'Duidelijke configuratie',
'L_CONFIG_REZET_TITLE'              => 'Klik hier om de parameters te resetten. vink het tweede vakje hieronder aan om te valideren,',
'L_CONFIG_REZET_BACK'               => 'Maak eerder een reservekopie',
'L_CONFIG_REZET_OK'                 => 'Wis de huidige instellingen',
'L_CONFIG_REZET_BACK_MSG'           => 'Vorig bestand opgeslagen als',
'L_CONFIG_REZET_OK_MSG'             => 'Alle parameters zijn uit het bestand verwijderd',
'L_CONFIG_REZET_NO_MSG'             => 'Geen parameters worden gewist, kan het bestand niet verwijderen',
'L_REZET_DO_NOTHING_MSG'            => 'Geen actie om uit te voeren',

'L_CONFIG_FOLDERS'                  => 'Gegevenslocatie (met een schuine streep "/")',
'L_CONFIG_ORDERS_NUMBER'            => 'Laatste ordernummer',
'L_CONFIG_ORDERS_FOLDER'            => 'Locatie van de map met bestellingen',
'L_CONFIG_PRODUCTS_FOLDER'          => 'Locatie van productmap',
'L_CONFIG_AFFICHER_PANIER_MENU'     => 'Toon winkelwagen in menu',
'L_CONFIG_AFFICHER_PRODUCTS_MENU'   => 'Geef %s weer in het menu (Store home)',
'L_CONFIG_AFFICHER_GROUPS_MENU'     => 'Groepen weergeven in het menu',
'L_CONFIG_AFFICHER_LIEN_PANIER_TOP' => 'Toon de link "uw winkelmandje" bovenaan de pagina’s',
'L_CONFIG_SHOP_INFO'                => 'Informatie opslaan',
'L_CONFIG_SHOP_NAME'                => 'Winkel naam',
'L_CONFIG_POSITION_ADMIN'           => 'Menupositie (admin)',
'L_CONFIG_SHOP_OWNER'               => 'Naam en voornaam van de handelaar',
'L_CONFIG_SHOP_STREET'              => 'Straat van de handelaar',
'L_CONFIG_SHOP_ZIP'                 => 'Merchant Postcode',
'L_CONFIG_SHOP_TOWN'                => 'Stad van de handelaar',
'L_CONFIG_SHOP_CURRENCY'            => 'Motto',
'L_CONFIG_POSITION_CURRENCY'        => 'Valutapositie',
'L_BEFORE'                          => 'Voor de prijs',
'L_AFTER'                           => 'Na de prijsuitreiking',
'L_CONFIG_SECURITY'                 => 'veiligheid',
'L_CONFIG_SECURITY_KEY'             => 'Coderingssleutel',

# Dates et heures de livraisons. Merci @ppmt
'L_CONFIG_DELIVERY_TITLE'           => 'Configuratie van leverings- en betalingsmethoden',
'L_CONFIG_DELIVERY_SHIPPING'        => 'Toevoeging van verzendkosten',
'L_CONFIG_DELIVERY_BY_PRICE'        => 'Gebaseerd op het totaal van de mand',
'L_CONFIG_DELIVERY_BY_PRICE_TITLE'  => 'Gebaseerd op totale prijs (ttc)',
'L_CONFIG_DELIVERY_DATE'            => 'Geef de keuze van de leverdatum en -tijd weer',
'L_CONFIG_DELIVERY_MINDAYS'         => 'Dag en tijd van levering',
'L_CONFIG_FIRSTDAYS'                => 'Eerste dag van de kalender',
'L_CONFIG_NB_MIN_TIME'              => 'Minimale levertijd',
'L_CONFIG_DELIVERY_STARTTIME'       => 'Eerste uur levering',
'L_CONFIG_DELIVERY_ENDTIME'         => 'Laatste levertijd',
'L_CONFIG_DELIVERY_TIMESLOT'        => 'Intervallen tussen leveringen',
'L_M'                               => 'minuten',
'L_J'                               => 'dagen',
# 0.13.2 Jours feriés, Vacances... Merci @ppmt
'L_CONFIG_DELIVERY_DISALOWED_DATES' => 'Datums niet beschikbaar op de kalender',
'L_CONFIG_DELIVERY_DISALOW_DATES'   => 'Data in het formaat <b>JJJJ-MM-DD</b> en gescheiden door komma’s.<br />Intervallen met een streepje "_"<br />tussen twee datums. <b><i>Voorbeeld</i></b>',
'L_DELIVERY_DAY'                    => 'Leveringen open op',
'L_DAY_0'                           => 'zondag',
'L_DAY_1'                           => 'maandag',
'L_DAY_2'                           => 'dinsdag',
'L_DAY_3'                           => 'woensdag',
'L_DAY_4'                           => 'donderdag',
'L_DAY_5'                           => 'vrijdag',
'L_DAY_6'                           => 'zaterdag',

'L_SHIPMAXWEIGHTADMIN'              => 'Mogelijk moeten de bedragen van de verzendkosten opnieuw worden beoordeeld of opnieuw worden geconfigureerd.',
'L_SHIPMAXWEIGHT'                   => 'Het bedrag van de verzendkosten kan opnieuw worden beoordeeld.',
'L_CONFIG_FREE'                     => 'Vrij',
'L_CONFIG_FREESHIPP'                => 'vanaf',
'L_FREESHIP'                        => 'Geen verzendkosten',
'L_CONFIG_DELIVERY_CONFIG'          => 'Configuratie van verzendkosten',
'L_CONFIG_DELIVERY_RECORDED'        => 'Ontvangstbevestiging',
'L_CONFIG_DELIVERY_WEIGHT'          => 'Gewicht in',
'L_CONFIG_DELIVERY_PRICE'           => 'Prijs in',
'L_CONFIG_NB_LINES'                 => 'Aantal rijen in de tabel',
'L_CONFIG_NB_LINES_HINT'            => 'Aantal configuratieregels in de verzendkostenstabel. Opslaan om het aantal regels effectief te maken, wijzig het dan en bewaar het opnieuw.',

'L_CONFIG_PAYMENTS_TITLE'           => 'Instellingen voor betalingsmethoden',
'L_CONFIG_PAYMENT_CHEQUE'           => 'Betaling per cheque',
'L_CONFIG_PAYMENT_CASH'             => 'Contante betaling',

'L_CONFIG_EMAIL_ORDER_TITLE'        => 'Bestel e-mailconfiguratie',
'L_CONFIG_EMAIL_ORDER_SUBJECT_CUST' => 'Titel van de e-mail "Besteloverzicht" (voor de klant)',
'L_CONFIG_EMAIL_ORDER_SUBJECT_SHOP' => 'Nieuwe bestelling',
'L_CONFIG_EMAIL_ORDER_THANKS'       => 'Bericht om te bedanken',
'L_CONFIG_ORDER_VIEW'               => 'Geef de bestelling weer nadat deze is verzonden',

'L_CONFIG_MENU_TITLE'               => 'Menuconfiguratie en tactiek van klantcookies',
'L_CONFIG_MENU_POSITION'            => 'Positie in het menu van groepen en vaste pagina’s (winkelmandje)',
'L_CONFIG_SUBMENU'                  => 'Naam van hoofdwinkel submenu (optioneel)',# groupe les groupes et le panier

'L_CONFIG_PAGE'                     => 'Pagina-instellingen',
'L_NO_IMAGE_CHOICE'                 => 'Kies de standaardafbeelding',

'L_CONFIG_BASKET_DISPLAY'           => 'Winkelwagenweergave',
'L_CONFIG_PAGE_TEMPLATE'            => 'Standaardsjabloon voor groepen, producten en vaste pagina’s',

'L_CONFIG_DATA_TITLE'               => 'Gegevensinstellingen en tactiek van klantcookies',

'L_CONFIG_SUBMIT'                   => 'Besparen',
'L_CONFIG_LOCALSTORAGE'             => 'Activeer lokale opslag van klantgegevens',
'L_CONFIG_COOKIE'                   => 'Houd 60 dagen aan om toekomstige bestellingen te vereenvoudigen',
# local storage
'L_ADDRESS_SAVED'                   => 'Uw gegevens zijn lokaal in uw browser opgeslagen.',
'L_ADDRESS_DELETED'                 => 'Uw contactgegevens zijn verwijderd uit uw browser.',
'L_DO_NOT_SHARED'                   => 'Gebruik deze optie niet op een gedeelde computer.',
'L_SAVE_MY_ADDRESS'                 => 'Bewaar uw gegevens',
'L_DELETE_MY_ADDRESS'               => 'Verwijder uw gegevens',
'L_RESET_ADDRESS'                   => 'Terug naar af',

'L_ADMIN_MODIFY'                    => 'Wijzig de lijst van',

'L_PRODUCTS'                        => 'Producten',
'L_GROUPS'                          => 'Groepen',
'L_NEW_PRODUCT'                     => 'Nieuw product',
'L_NEW_CATEGORY'                    => 'Nieuwe groep',

'L_ADMIN_ORDER_VIEW'                => 'zien',
'L_ADMIN_ORDER_DELETE'              => 'verwijderen',
'L_ADMIN_CONFIRM_DELETE'            => 'Bevestigt u het verwijderen van deze opdracht?',

# plxMyShop.php
'L_PAYMENT_CHEQUE'                  => 'Cheque',
'L_PAYMENT_CASH'                    => 'Contant geld',

'L_PAYMENT_ERROR'                   => 'Een betaalmethode is vereist!',

'L_EMAIL_SUBJECT'                   => 'Nieuwe bestelling',
'L_EMAIL_TEL'                       => 'Telefoon',
'L_EMAIL_NO_GIFT'                   => 'De bestelling is voor de klant',
'L_EMAIL_GIFT_FOR'                  => 'De bestelling is een cadeau voor',
'L_EMAIL_PRODUCTLIST'               => 'Producten lijst',
'L_EMAIL_WEIGHT'                    => 'Gewicht',
'L_EMAIL_DELIVERY_COST'             => 'Verzendkosten',
'L_EMAIL_COMMENT'                   => 'Commentaar',
'L_EMAIL_CONFIRM_CHEQUE'            => 'De bestelling is bevestigd en per e-mail verzonden.',
'L_EMAIL_CONFIRM_CASH'              => 'De bestelling is bevestigd en per e-mail verzonden.',

'L_EMAIL_CUST_SUBJECT'              => 'overzicht van de bestelling',
'L_EMAIL_CUST_MESSAGE1'             => 'U heeft zojuist een bestelling bevestigd op',
'L_EMAIL_CUST_MESSAGE2'             => 'Deze opdracht is binnen',
'L_WAITING'                         => 'aan het wachten',
'L_ONGOING'                         => 'Klassen',
'L_EMAIL_CUST_MESSAGE3'             => 'van regelgeving',
'L_EMAIL_CUST_CHEQUE'               => 'Om deze bestelling af te ronden, dient u de cheque te betalen aan',
'L_EMAIL_CUST_SENDCHEQUE'           => 'Stuur uw cheque naar dit adres',
'L_EMAIL_CUST_CASH'                 => 'Gelieve te betalen bij levering',
'L_EMAIL_CUST_SUMMARY'              => 'Samenvatting van uw bestelling',
'L_EMAIL_CUST_ADDRESS'              => 'Bezorgadres',
'L_EMAIL_CUST_PAYMENT'              => 'Betaalmethode',

'L_EMAIL_SENT_THANKS'               => 'Dank u.',
'L_EMAIL_SENT1'                     => 'Er is een e-mailoverzicht van de bestelling naar u verzonden.',
'L_EMAIL_SENT2'                     => 'Als je e-mail met je besteloverzicht niet in je inbox wordt gevonden of als spam wordt gerapporteerd, voeg dan %s toe aan je lijst met contactpersonen in je e-mailadres.',
'L_EMAIL_ERROR1'                    => 'Er is een fout opgetreden bij het verzenden van uw overzichtsmail.',
'L_EMAIL_ERROR2'                    => 'Er is een fout opgetreden bij het verzenden van de bestelling per e-mail.',

'L_EMAIL_DELIVERYDATE'              => 'Bezorgdatum',
'L_EMAIL_DELIVERYTIME'              => 'Tijd om te bezorgen',

'L_FILE_ORDER'                      => 'Bestellen van',
'L_FILE_NUM'                        => 'Bestelnr.',
'L_FILE_REF'                        => 'Referenties',

'L_FOR'                             => 'voor',
'L_DEL'                             => 'Verwijderen',

'L_MISSING_EMAIL'                   => 'het e-mailadres is niet gedefinieerd.',
'L_MISSING_FIRSTNAME'               => 'De voornaam is niet gedefinieerd.',
'L_MISSING_LASTNAME'                => 'De familienaam is niet gedefinieerd.',
'L_MISSING_ADDRESS'                 => 'Het adres is niet gedefinieerd.',
'L_MISSING_ZIP'                     => 'De postcode is niet gedefinieerd.',
'L_MISSING_TOWN'                    => 'De stad is niet gedefinieerd.',
'L_MISSING_COUNTRY'                 => 'Het land is niet gedefinieerd.',
'L_MISSING_GIFTNAME'                => 'De naam van de ontvanger van het geschenk is niet gedefinieerd.',

'L_MISSING_VALIDATION_CGV'          => 'U moet de algemene verkoopvoorwaarden valideren.',

'L_DELIVERY_ERROR_MIN_DATE'         => 'De leverdatum is onmogelijk',
'L_DELIVERY_ERROR_MIN_TIME'         => 'Kan geen tijd leveren',

# Espace public
'L_PRODUCT_NO'                      => 'Niets te koop hier, geduld.',
'L_PUBLIC_MSG_BASKET_UP'            => 'Uw winkelmand is bijgewerkt',
'L_PUBLIC_BASKET'                   => 'uw mandje',
'L_PUBLIC_BASKET_NIL'               => '0.00',
'L_PUBLIC_TOTAL_BASKET'             => 'Totaal',
'L_PUBLIC_NOPRODUCT'                => 'Jouw mandje is leeg.',
'L_PUBLIC_MANDATORY_FIELD'          => '* = verplichte velden',
'L_PUBLIC_FIRSTNAME'                => 'Voornaam',
'L_PUBLIC_LASTNAME'                 => 'Naam',
'L_PUBLIC_EMAIL'                    => 'E-mail',
'L_PUBLIC_TEL'                      => 'Telefoon',
'L_PUBLIC_ADDRESS'                  => 'Adres',
'L_PUBLIC_ZIP'                      => 'Postcode',
'L_PUBLIC_TOWN'                     => 'stad',
'L_PUBLIC_COUNTRY'                  => 'Land',
'L_PUBLIC_GIFT'                     => 'Is dit een cadeau?',
'L_PUBLIC_GIFTNAME'                 => 'Geef de voor en achternaam op van de persoon die het geschenk ontvangt',
'L_PUBLIC_COMMENT'                  => 'Jouw commentaar',
'L_PUBLIC_VALIDATE_ORDER'           => 'Bevestig de bestelling',
'L_PUBLIC_ADD_BASKET'               => 'Voeg toe aan winkelkar',
'L_PUBLIC_MOD_BASKET'               => 'Winkelwagen bewerken',
'L_PUBLIC_DEL_BASKET'               => 'Verwijderen uit mand',
'L_PUBLIC_BASKET_MINI_TITLE'        => 'Bestellen',
'L_PUBLIC_BASKET_MINI'              => 'Bevelen!',
'L_PUBLIC_NOJS'                     => 'Activeer JavaScript in uw browser om toegang te krijgen tot het bestelformulier.',
'L_PUBLIC_TAX'                      => 'T.T.C.',

'L_COMMANDE_LIBELLE_DEFAUT'         => 'Ik lees en ga akkoord met de algemene verkoopvoorwaarden.',
'L_CONFIG_VALIDATION_COMMANDE'      => 'Bevestiging van de bestelling',
'L_CONFIG_INFO_CGV'                 => 'Om de algemene verkoopinstructies te activeren, moet het onderstaande veld worden aangegeven.',
'L_CONFIG_INFO_CGV_URL'             => 'Dit adres is herschreven door PluXml (de link wordt gebruikt om te verifiëren).',
'L_CONFIG_INFO_CGV_MML'             => 'Het voorvoegsel "##/" wordt toegevoegd aan talen, behalve de standaard van de site',
'L_CONFIG_LIBELLE_CGV'              => 'Formulering van het validatieverzoek',
'L_CONFIG_LANGUE_CGV_SYSTEME'       => 'Gebruik de standaard plugin-zin',
'L_CONFIG_URL_CGV'                  => 'Algemene verkoop-URL',
'L_CGV'                             => 'V.V.W',

# boutonPanier.php
'L_NOTICE_NOADDCART'                => 'Geen voorraad meer',

# panier.js
'L_TOTAL_BASKET'                    => 'Totaal',
'L_TOTAL_BASKET_PORT'               => ' (inclusief verzendkosten)',
'L_TITRE_PANIER'                    => 'Mand',

# panier.php
'L_PRODUCT'                         => 'Product',
'L_UNIT_PRICE'                      => 'Stuksprijs',
'L_NUMBER'                          => 'Aantal',
'L_TOTAL_PRICE'                     => 'Totale prijs',
'L_PUBLIC_DELIVERYDATE'             => 'Selecteer uw bezorgdatum',
'L_PUBLIC_DELIVERYTIME'             => 'Selecteer uw bezorgvenster',
'L_CHOICE_DELIVERYTIME'             => 'Kies een tijdslot',
'L_FORMAT_PIKADAY'                  => 'dddd Do MMMM YYYY',
'L_PREV_PIKADAY'                    => 'Vorige maand',
'L_NEXT_PIKADAY'                    => 'Volgende maand',
'L_WEEK_PIKADAY'                    => 'Zo ma di wo do vr za',
'L_WEEK_PIKADAY_LONG'               => 'zondag maandag dinsdag woensdag donderdag vrijdag zaterdag',
'L_MONTH_PIKADAY'                   => 'Januari februari maart april mei juni juli augustus september oktober november december',
);
