<?php
require_once dirname(__FILE__) . '/../VuePublique.php';
class products extends VuePublique {
 public function traitementVuePublique() {
  $this->fichierAffichageVuePublique = 'products';
 }
 public function titreVuePublique() {
  return $this->plxPlugin->getLang('L_PRODUCTS_HOME_PAGE');
 }
}