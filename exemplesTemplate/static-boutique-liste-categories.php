<?php if (!defined('PLX_ROOT')) exit; ?>
<?php include(dirname(__FILE__) . '/header.php'); ?>
 <main class="main" >
  <div class="container">
   <div class="grid">
    <div class="content col sml-12">
     <article class="article static" role="article" id="static-page-<?php echo $plxShow->staticId(); ?>">
      <h1><?php $plxShow->staticTitle(); ?></h1>
      <?php $plxShow->staticContent(); ?>
     </article>
     <div class="content col sml-12">
      <ul>
<?php
    $plxMyShop = $plxShow->plxMotor->plxPlugins->aPlugins['plxMyShop'];
    $plxMyShop->donneesModeles['plxPlugin'] = $plxMyShop;
    if (isset($plxMyShop->aProds[$plxMyShop->dLang]) && is_array($plxMyShop->aProds[$plxMyShop->dLang])) {
     $name = $plxMyShop->getParam('name_home_'.$plxMyShop->dLang);#MyMultiLingue ou non);
     $name = $name? $name: $plxMyShop->getLang('L_PRODUCTS_HOME_PAGE');# 000 == MyShopHomePage
     $lien = $plxShow->plxMotor->urlRewrite('?'.$plxMyShop->lang.'products/stars');
?>
       <li><a href="<?php echo $lien;?>"><?php echo htmlspecialchars($name);?></a></li>
<?php
     foreach ($plxMyShop->aProds[$plxMyShop->dLang] as $kRubrique => $vRubrique) {
      if ($vRubrique['pcat'] == 1 && $vRubrique['menu'] == 'oui' && $vRubrique['active'] == 1){
       $lien = $plxShow->plxMotor->urlRewrite('?'.$plxMyShop->lang."product$kRubrique/{$vRubrique['url']}");
?>
       <li><a href="<?php echo $lien;?>"><?php echo htmlspecialchars($vRubrique['name']);?></a></li>
<?php
      }
     }
    }
?>
     </ul>
    </div>
   </div>
  </div>
 </main>
<?php include(dirname(__FILE__).'/footer.php'); ?>