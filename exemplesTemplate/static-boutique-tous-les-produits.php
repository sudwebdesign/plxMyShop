<?php if (!defined('PLX_ROOT')) exit; ?>
<?php include(dirname(__FILE__) . '/header.php'); ?>
 <main class="main" >
  <div class="container">
   <div class="grid">
    <div class="content col sml-12">
     <article class="article static" role="article" id="static-page-<?php echo $plxShow->staticId(); ?>">
      <header>
       <h1><?php $plxShow->staticTitle(); ?></h1>
      </header>
      <?php $plxShow->staticContent(); ?>
     </article>
    </div>
<?php
    $plxMyShop = $plxShow->plxMotor->plxPlugins->aPlugins['plxMyShop'];
    $plxMyShop->donneesModeles['plxPlugin'] = $plxMyShop;
    if (isset($plxMyShop->aProds[$plxMyShop->dLang]) && is_array($plxMyShop->aProds[$plxMyShop->dLang])) {
?>
     <script type="text/javascript" src="<?php echo $plxMyShop->plxMotor->racine . PLX_PLUGINS . $plxMyShop->plugName;?>/js/panier.js?v104"></script>
     <script type="text/javascript">
      var error = false;
      var devise = '<?php echo $plxMyShop->getParam('devise');?>';
      var pos_devise = '<?php echo $plxMyShop->getParam('pos_devise');?>';
      var shoppingCart = null;
      var L_FOR = '<?php echo $plxMyShop->getLang('L_FOR'); ?>';
      var L_DEL = '<?php echo $plxMyShop->getLang('L_DEL'); ?>';
      var L_TOTAL = '<?php echo $plxMyShop->getLang('L_TOTAL_BASKET'); ?>';
     </script>
<?php
     if($plxMyShop->getParam('afficheProductsMenu')){# home group '000'
      $name = $plxMyShop->getParam('name_home_'.$plxMyShop->dLang);#MyMultiLingue ou non);
      $name = $name? $name: $plxMyShop->getLang('L_PRODUCTS_HOME_PAGE');# 000 == MyShopHomePage
      $lien = $plxShow->plxMotor->urlRewrite('?'.$plxMyShop->lang.'products/stars');
      $groups['000'] = array('name'=>$name, 'url'=>$lien, 'image'=>$plxMyShop->getParam('image_home_'.$plxMyShop->dLang));
     }
     foreach ($plxMyShop->aProds[$plxMyShop->dLang] as $k => $v) {#normal groups
      if ( $v['menu'] === 'non'
       || $v['menu'] === ''
       || (1 !== $v['active'])
      ) {
       continue;
      }
      $groups[$k] = $v;
      $groups[$k]['url'] = $plxShow->plxMotor->urlRewrite('?'.$plxMyShop->lang."product$k/{$v['url']}");;
     }
     foreach ($groups as $kRubrique => $vRubrique) {
      $plxMyShop->idProduit = $kRubrique;
      $lien = $vRubrique['url'];
?>
    <div class="col sml-12">
       <h2><a href="<?php echo htmlspecialchars($lien);?>"><?php echo htmlspecialchars($vRubrique['name']);?></a></h2>
       <div class="list_products" role="section">
        <header>
         <div class="cat_image">
          <?php $plxMyShop->productImage();?>
         </div>
        </header>
<?php
         foreach($plxMyShop->aProds[$plxMyShop->dLang] as $k => $v) {
          if(!$v['pcat']
           && $v['active']==1
           && $v['readable']==1
           && strstr($v['group'],$kRubrique)
          ) {
           $plxMyShop->donneesModeles['k'] = $k;
           $plxMyShop->modele('espacePublic/boucle/produitRubrique');
          }
         }
?>
       </div>
    </div>
<?php
     }
    }
?>
   </div>
  </div>
 </main>
<?php include(dirname(__FILE__).'/footer.php'); ?>