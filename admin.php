<?php if (!defined('PLX_ROOT')) exit;
/**
 * Edition des produits
 * @package PLX
 * @author  David L, Thomas Ingles
 **/
if(!empty($_POST)) plxToken::validateFormToken($_POST);# Controle du token du formulaire

$zipname = '.'.$plxPlugin->plugName.'_'.@$_SERVER['HTTP_HOST'].'_backup.commands.zip';
$dirComs = PLX_ROOT.$plxPlugin->aConf['racine_commandes'];
$aLangs = ($plxPlugin->aLangs)?$plxPlugin->aLangs:array($plxPlugin->dLang);#multilingue or not

# FallBack currency symbols (one time)
$devises = true;
foreach($aLangs as $lang) {
 if(empty($plxPlugin->getParam('devise_'.$lang))){#check
  $tlang = $plxPlugin->loadLang(PLX_PLUGINS.$plxPlugin->plugName.'/lang/'.$lang.'.php');
  if(isset($tlang['MONEY'])){
   $plxPlugin->setParam('devise_'.$lang, $tlang['MONEY'], 'string');
   $plxPlugin->setParam('position_devise_'.$lang, $tlang['MONEY_POSITION'], 'numeric');#0#after 1#before
   $devises = false;
  }
  unset($tlang);
 }
}
unset($lang);
if(!$devises) $plxPlugin->saveParams();#save

# Efface la sauvegarde si créés il y a + de 15min
if(file_exists($dirComs . $zipname) && (time() - filemtime($dirComs . $zipname)) > 900){#15 * 60s
 unlink($dirComs . $zipname);
}
////////////////SEARCH//MULTILINGUE/////////////////////////////////////////////////////////////////////
if((!isset($_GET['prod']) AND !isset($_GET['mod'])) OR (isset($_GET['mod']) AND $_GET['mod'] == 'cat')){#!='cmd'
 $search_mode = '_'.(isset($_GET['mod'])?'grp':'prd');
 # Multilingue ou non
 foreach($aLangs as $lang) {
  $lgf=($plxPlugin->aLangs)?$lang.'/':'';//folders
  $lng=($plxPlugin->aLangs)?'_'.$lang:'';//post vars

  # Récuperation des paramètres
  if(!empty($_GET['sel']) AND in_array($_GET['sel'], array('all','published', 'draft'))) {
   $_SESSION['sel_get' . $search_mode][$plxPlugin->plugName][$lang]=plxUtils::nullbyteRemove($_GET['sel']);
   $_SESSION['sel_cat' . $search_mode][$plxPlugin->plugName][$lang]='';
  }
  else
   $_SESSION['sel_get' . $search_mode][$plxPlugin->plugName][$lang]=(isset($_SESSION['sel_get' . $search_mode][$plxPlugin->plugName][$lang]) AND !empty($_SESSION['sel_get' . $search_mode][$plxPlugin->plugName][$lang]))?$_SESSION['sel_get' . $search_mode][$plxPlugin->plugName][$lang]:'all';

  # Nombre d'element sélectionnés
  # Récupération du texte à rechercher
  $artTitle = (!empty($_POST['artTitle' . $lng]))?plxUtils::unSlash(trim(urldecode($_POST['artTitle' . $lng]))):'';
  #$artTitle = (); #BAF REZET ON CLICK "MODIF LIST"
  if(!empty($artTitle) AND !isset($_POST['update']) /*AND !empty($_POST['artTitle'])*/) {
   $_SESSION['artTitle' . $search_mode][$plxPlugin->plugName][$lang] = $artTitle;
  }
  elseif(isset($_SESSION['artTitle' . $search_mode][$plxPlugin->plugName][$lang])){
   $artTitle = $_SESSION['artTitle' . $search_mode][$plxPlugin->plugName][$lang];
   if($artTitle == $_SESSION['artTitle' . $search_mode][$plxPlugin->plugName][$lang]){#raz
    unset($_SESSION['artTitle' . $search_mode][$plxPlugin->plugName][$lang]);
    $artTitle = '';
   }else
    $artTitle = $_SESSION['artTitle' . $search_mode][$plxPlugin->plugName][$lang];
  }
  $_GET['artTitle' . $lng] = $artTitle;

  if(!empty($_POST['sel_cat' . $lng])){
   if(isset($_SESSION['sel_cat' . $search_mode][$plxPlugin->plugName][$lang]) AND $_SESSION['sel_cat' . $search_mode][$plxPlugin->plugName][$lang]==$_POST['sel_cat' . $lng]) # annulation du filtre
    $_SESSION['sel_cat' . $search_mode][$plxPlugin->plugName][$lang]='all';
   else # prise en compte du filtre
    $_SESSION['sel_cat' . $search_mode][$plxPlugin->plugName][$lang]=$_POST['sel_cat' . $lng];
  }else
   $_SESSION['sel_cat' . $search_mode][$plxPlugin->plugName][$lang]=(isset($_SESSION['sel_cat' . $search_mode][$plxPlugin->plugName][$lang]) AND !empty($_SESSION['sel_cat' . $search_mode][$plxPlugin->plugName][$lang]))?$_SESSION['sel_cat' . $search_mode][$plxPlugin->plugName][$lang]:'all';

  # Recherche du motif de sélection des articles en fonction des paramètres
  $catIdSel = '';
  $mod='';
  switch ($_SESSION['sel_get' . $search_mode][$plxPlugin->plugName][$lang]) {#tep
  case 'published':
   $catIdSel = 0;#actived (inverse is show = false)
   $mod='';
   break;
  case 'draft':
   $catIdSel = 1;#deactived (inverse is show = false)
   $mod='_?';
   break;
  case 'all':
   $catIdSel = -1;#2
   $mod='_?';
   break;
  }

  # tep : selector
  switch ($_SESSION['sel_cat' . $search_mode][$plxPlugin->plugName][$lang]) {
   case 'all' :
    $grpIdSel = '';break;#str_replace('FILTER', '', $grpIdSel); break;
   case '000' :# Only in prods list mode
    $grpIdSel = !isset($_GET['mod'])?'000':'';break;#str_replace('FILTER', '000', $grpIdSel); break;
   #case 'home':
   # $grpIdSel = str_replace('FILTER', 'home', $grpIdSel); break;
   case preg_match('/^[\d]{3}$/', $_SESSION['sel_cat' . $search_mode][$plxPlugin->plugName][$lang])==1:
    $grpIdSel = $_SESSION['sel_cat' . $search_mode][$plxPlugin->plugName][$lang];#str_replace('FILTER', $_SESSION['sel_cat' . $search_mode][$plxPlugin->plugName][$lang], $grpIdSel);
  }

  # On génère notre motif de recherche
  if(is_numeric($_GET['artTitle'. $lng])) {
   $prodId = str_pad($_GET['artTitle'. $lng],3,'0',STR_PAD_LEFT);
   $motif = '~^'.$prodId.'\.(.*)\.php$~';
  } else {
   $prodId = str_replace('-','.*?',plxUtils::title2url($_GET['artTitle'. $lng]));
   #$motif = '~^[\d]{3}\..*.*\.php$~';#all
   $motif = '~^[\d]{3}\..*'.$prodId.'.*\.php$~';#find url
  }
  if(!isset($_POST['update']) AND $plxPlugin->aProds[$lang]){#!update fix lost data
   $files = array();
   foreach($plxPlugin->aProds[$lang] as $id => $p){
    $fil = $id.'.'.$p['url'].'.php';
    # $catIdSel (-1|1|0) all|draft|published
    $plxPlugin->aProds[$lang][$id]['visible']=!0;#default
    if($catIdSel == $p['active'])#inverse
     $plxPlugin->aProds[$lang][$id]['visible']=!1;#hide
    $GorI = !isset($_GET['mod'])?$p['group']:$id;
    if(!empty($grpIdSel) AND !in_array($grpIdSel, explode(',', $GorI)))#
     $plxPlugin->aProds[$lang][$id]['visible']=!1;#hide
    if(preg_match($motif, $fil))#url
     $files[] = $fil;#trouvé
    elseif(preg_match('~'.$prodId.'~', plxUtils::removeAccents($p['name'],PLX_CHARSET)))#titre @name
     $files[] = $fil;#trouvé
   }
   $aFiles = array();
   foreach($files as $file){
    $aFiles[] = $file[0].$file[1].$file[2];#SI N'EST PAS DS CETTE LISTE UNSET PROD $_POST = array(); (& maybe get?)
   }
   foreach($plxPlugin->aProds[$lang] as $id => $p){
    $isCat = (isset($_GET['mod']) AND $_GET['mod'] == 'cat');
    if(!in_array($id, $aFiles)){
     if((!$isCat AND !$p['pcat']) OR ($isCat AND $p['pcat'])){
      #unset($plxPlugin->aProds[$lang][$id]);
      $plxPlugin->aProds[$lang][$id]['visible']=!1;
     }
    }
   }
  }#no update
 }#Foreach aLangs Multilingue ou non
}
////////////////SEARCH//MULTILINGUE/////////////////////////////////////////////////////////////////////

$actionBarClasses = 'plx'.str_replace('.','-',@PLX_VERSION).(defined('PLX_MYMULTILINGUE')?' multilingue':'');
$mmlFlagImgs = $plxPlugin->mmlFlagImgs();# Images des drapeaux

# On édite les produits
$fakeget = $onglet = $menuAdminPlus = '';
if(!empty($_POST)){
 $redir = false;
 if (!empty($_POST['clean']) AND empty($_POST['selection']) AND !isset($_POST['ok'])){
  $redir = true;
  $fakeget='&mod=cmd';
  if (isset($_POST['delzip']) AND $_POST['delzip']=='o')# nojs fallback
   $plxPlugin->cleanOrZip($zipname);
  elseif ($_POST['clean']=='zip' OR isset($_POST['zip']))#2nf 4 nojs fallback
   $plxPlugin->cleanOrZip(false,true);# zip all cached files
  elseif ($_POST['clean']=='all')
   $plxPlugin->cleanOrZip();
  else
   $plxPlugin->cleanOrZip($_POST['clean']);#one file
 }
 # Suppression / Modification / Télécharger commandes
 elseif(isset($_POST['ok']) AND !empty($_POST['selection']) AND isset($_POST['idCommand'])){
  $redir = true;
  $error = '';
  $success = '';
  $fakeget='&mod=cmd';
  if($_POST['selection'] == 'download'){#save checkeds in one zip
   $plxPlugin->cleanOrZip(false,true, $_POST['idCommand']);# zip all cached files
   header('Location: plugin.php?p='.$plxPlugin->plugName.$fakeget);# . '&e=stop'
   exit;
  }
  $stateColors = array('P' => 'darkgreen', 'A' => 'darkblue', 'C' => 'darkred');
  $states = array();
  $states[$plxPlugin->dLang] = array('P' => $plxPlugin->getLang('L_PAID'), 'A' => $plxPlugin->getLang('L_AWAIT'), 'C' => $plxPlugin->getLang('L_CANCELED'));
  $var = $_POST['selection'];#A P C
  foreach($_POST['idCommand'] AS $command){
   $filename = PLX_ROOT.$plxPlugin->aConf['racine_commandes'].$command;
   if(!is_file($filename)){
    $error .= $filename . 'Not exist' . PHP_EOL;
   }else{
    if($_POST['selection']=='delete'){
     if(!unlink($filename)){
      $error .= @L_DELETE_FILE_ERR . ' : ' . $filename . PHP_EOL;
     }else
      $success = @L_DELETE_SUCCESSFUL;
    }
    elseif(strlen($_POST['selection']) == 1){#Paid, Cancel, Await
     $success = @L_RENAME_FILE_SUCCESSFUL;
     if(is_writable($filename)) {# en/2020-07-08_10-07-08_cash_51_0_A.html
      # Si meme statut on passe a la suivante
      if(substr($command, -6, 1) === $var){
       continue;
      }
      # Langue de la commande
      $glng = substr($command, 0, 2);
      # Charge la bonne langue, si non dispo = langue par defaut
      if(!isset($states[$glng])){
       $tlangs[$glng] = $plxPlugin->loadLang(PLX_PLUGINS.$plxPlugin->plugName.'/lang/'.$glng.'.php');//Fatal error: Call to private method plxPlugin::loadLang() from context '' : (pluxml-5.2)
       if(!isset($tlangs[$glng]['L_PAID'], $tlangs[$glng]['L_AWAIT'], $tlangs[$glng]['L_CANCELED']))
        $states[$glng] = $states[$plxPlugin->dLang];
       else
        $states[$glng] = array('P' => $tlangs[$glng]['L_PAID'], 'A' => $tlangs[$glng]['L_AWAIT'], 'C' => $tlangs[$glng]['L_CANCELED']);
      }
      # Modifie la commande (html)
      $html = file_get_contents($filename);
      $html = str_replace('<body>', '<body>'.PHP_EOL.'<b style="color:'. $stateColors[$var].'"><i>'.$states[$glng][$var].' (' .date('Y.m.d H:i:s'). ')</i></b>'.PHP_EOL, $html);
      if($var!='P')
       $html = str_replace('style="text-decoration: line-through"', '', $html);
      else
       $html = str_replace('<span class="cust_mess">', '<span class="cust_mess" style="text-decoration: line-through">', $html);
      if(!empty($html)) file_put_contents($filename, $html);
      # Renomme le fichier
      $nfn = str_replace(array('_A.html', '_P.html', '_C.html', '.html'), '' ,$filename);
      $nfn .= '_'.$_POST['selection'].'.html';
      $result = rename($filename, $nfn);
      //$success = '<br />'.$filename.' 2 '.$nfn;
     }else{
      $error .= @L_RENAME_FILE_ERR . ' : ' . $filename . PHP_EOL;
     }
    }
   }
  }

  if(!empty($error)){
   plxMsg::Error(nl2br($error));
  }else{
   plxMsg::Info($success);
  }
 }
 elseif((isset($_POST['ok']) AND !empty($_POST['selection'])) OR (isset($_POST['update']) OR isset($_POST['prod']))){#Prods & cats (origin)
  $redir = true;
  $plxPlugin->editProducts($_POST, true);
  if (isset($_POST['prod']) && !empty($_POST['prod'])){
   $plxPlugin->editProduct($_POST);
   $fakeget='&prod='.$_POST['prod'];
  } else {
   $plxPlugin->editProducts($_POST);
   $fakeget=(isset($_GET['mod']) && !empty($_GET['mod'])?'&mod='.$_GET['mod']:'');
  }
 }
 if($redir){
  header('Location: plugin.php?p='.$plxPlugin->plugName.$fakeget);
  exit;
 }
}
if(!empty($_GET)){
 if(isset($_GET['html']) AND !empty($_GET['html'])){//download eml by readfile #v1.0.0
  $htmlfile = $dirComs.plxEncrypt::decryptId($_GET['html']);#on décrypte
  #on verif si le fichier existe et On lance le téléchargement
  if(file_exists($htmlfile)) {
   ob_clean();//on nettoie le output buffer
   header('Content-Description: File Transfer');
   header('Content-Type: application/download');
   header('Content-Disposition: attachment; filename='.basename($htmlfile));
   header('Content-Transfer-Encoding: binary');
   header('Expires: 0');
   header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
   header('Pragma: no-cache');
   header('Content-Length: '.filesize($htmlfile));
   readfile($htmlfile);
   exit;
  }
 }
}

#Supprimer une commande
if (isset($_GET['kill']) && !empty($_GET['kill']) && is_file($dirComs.$_GET['kill'])){
 if(unlink($dirComs.$_GET['kill']))
  plxMsg::Info(L_DELETE_SUCCESSFUL);
 else
  plxMsg::Error(@L_DELETE_FILE_ERR . ' : ' . $dirComs.$_GET['kill']);
 header('Location: plugin.php?p='.$plxPlugin->plugName.'&mod=cmd');
}

#Shift to edit Prod & Cat : editionProduitAdmin
if ((isset($_GET['prod']) && !empty($_GET['prod'])) || (isset($_POST['prod']) && !empty($_POST['prod'])))
 include(dirname(__FILE__).'/template/editionProduitAdmin.php');
else {#prods, cats & coms list ?>
<style>.inline-form.action-bar{display:none;}</style>
<script type="text/javaScript">function checkBox(obj){obj.value=(obj.checked==true)?'1':'0';}</script>
<?php
#IDEA : SET TO (IN)ACTIVE
 $status = array( '' =>L_FOR_SELECTION/*, 'delete' =>L_DELETE*/);
 if (!isset($_GET['mod'])){
  $onglet = 'produits';
  $titre = $plxPlugin->getLang('L_PRODUCTS_PAGE_TITLE');
 }elseif('cat' === $_GET['mod']){
  $onglet = 'categories';
  $titre = $plxPlugin->getLang('L_CATS_PAGE_TITLE');
 }elseif('cmd' === $_GET['mod']){
  $onglet = 'commandes';
  $titre = $plxPlugin->getLang('LIST_ORDERS');
  $status = array_merge($status, array('A' => $plxPlugin->getLang('L_SET_AWAIT'), 'C' => $plxPlugin->getLang('L_SET_CANCELED'), 'P' => $plxPlugin->getLang('L_SET_PAID')));
  $status['download'] = $plxPlugin->getLang('L_DOWNLOAD');
 }
 $status['delete'] = L_DELETE;
 if($plxPlugin->aLangs){# Onglets MyMultiLingue
  $tabHeaders = '';# Créer les onglets et images des drapeaux
  foreach($aLangs as $lang){
   $datab = $titab = $mixab = '';
   if($lang==$_SESSION['default_lang']){#o
   //~ if($lang==$plxPlugin->lang){#no (maybe $plxPlugin->dLang)
    $titab .= L_CONFIG_BASE_DEFAULT_LANG . (isset($_GET['mod']) && $_GET['mod']=='cat'? ' &amp; ' . $plxPlugin->getLang('L_NEW_CATEGORY'): (!isset($_GET['mod'])? ' &amp; ' . $plxPlugin->getLang('L_NEW_PRODUCT'): ''));
    $datab .= ' data-default_lang';
    $mixab .= ' &amp; ';
   }
   if($lang==$plxPlugin->dLang){#?' data-user_lang':''
    $titab .= $mixab.L_USER_LANG;
    $datab .= ' data-user_lang';
   }
   $tabHeaders .= '     <li id="tabHeader_'.$lang.'"'.($lang==$plxPlugin->dLang?' class="active"':'').' title="'.$titab.'"'.$datab.'>'.strtoupper($lang).'</li>'.PHP_EOL;
  }
 }
 if($onglet=='commandes'){#(isset($_GET['mod']) && $_GET['mod']=='cmd')
  $url = $plxAdmin->racine.$plxAdmin->path_url.(isset($_GET['e'])?'&e=stop':'');
  $statesCom = array('A' => $plxPlugin->getLang('L_AWAIT'), 'C' => $plxPlugin->getLang('L_CANCELED'), 'P' => $plxPlugin->getLang('L_PAID'));
  $fresh=$freshb=$imgzip=$delzip='';# init some var
  $hf=(count(scandir($dirComs)) == 2);//is dir empty #have file
  if(file_exists($dirComs . $zipname)){#goto zip 4 download, &confirm del zip (after 3s) & add button 4 noJs fallBack 'and clear after) !!! if zip is present on server, display download & dialog boxes all the time, sorry about that, click on refresh to stop this effect
   //~ $imgzip = '<a href="' . $dirComs . $zipname . '" title="⚠ '.$plxPlugin->getLang('L_ZIP_SERVER').' ('.date('Y-m-d H:i',filemtime($dirComs . $zipname)).')">'
   //~ .'☯</a> ';
   $imgzip = '<a href="'. $url . '&amp;html='.plxEncrypt::encryptId($zipname) . '" title="⚠ '.$plxPlugin->getLang('L_ZIP_SERVER').' ('.date('Y-m-d H:i',filemtime($dirComs . $zipname)).')">'
   .'☯</a> ';
   $freshb = '<button class="delete green" type="submit" name="delzip" value="o" onClick="cleanOrZip(\''. $zipname.'\');return false;" title="'.$plxPlugin->getLang('L_CACHE_ZIPDEL').'">⛈</button> ';#2 delzip NoJs & js
   if(!isset($_GET['e'])){
    //~ $fresh = '<meta HTTP-EQUIV="REFRESH" content="0; url=' . $dirComs . $zipname . '">';#go to download
    $fresh = '<meta HTTP-EQUIV="REFRESH" content="0; url=' . $url . '&amp;html='.plxEncrypt::encryptId($zipname) . '">';#go to download
    $delzip = '/* After download */
    function cleanZip(){if (confirm("'.$plxPlugin->getLang('L_CACHE_ZIPDEL').'")) cleanOrZip("'. $zipname.'");}
    window.setTimeout(cleanZip, 3000);';
   }
  }
  ob_start();
  echo $fresh;#meta refresh to zip #dowload
?>
   <a href="<?php //echo $e ?>" title="<?php $plxPlugin->lang('L_REFRESH') ?>">&infin;</a>
   <button class="download blue<?php echo($hf?' hide':'')?>" type="submit" name="zip" value="o" onClick="cleanOrZip('zip');" title="<?php $plxPlugin->lang('L_CACHE_ZIP'); ?>">⛑</button>
   <button
    class="update red<?php echo($hf?' hide':'')?>" name="update" type="submit"
    onClick='if(confirm("<?php
    $plxPlugin->lang('L_CACHE_CONFIRM_DEL') ?>") && "<?php
    $plxPlugin->lang('L_CLEAN_CACHE') ?>"==prompt("<?php
    $plxPlugin->lang('L_CACHE_CONFIRM_DEL_2') ?>\n<?php
    $plxPlugin->lang('L_CACHE_CONFIRM_DEL_3') ?>\n<?php
    $plxPlugin->lang('L_CLEAN_CACHE') ?>"))cleanOrZip();return false;'
    title="<?php $plxPlugin->lang('L_CLEAN_CACHE'); ?>">🔥</button>
 <?php
   echo $freshb . $imgzip;
   $menuAdminPlus = ob_get_clean();
  }
?>
<form action="" method="post" id="form_<?php echo $plxPlugin->plugName ?>">
 <div class="action-bar action <?php echo $actionBarClasses?>">
  <h2 id="page-title" class="page-title h4"><?php echo $titre?></h2>
<?php $plxPlugin->adminMenu($onglet); echo $menuAdminPlus; ?>
 </div>
 <div class="in-action-bar selector <?php echo $actionBarClasses?>">
  <?php echo plxToken::getTokenPostMethod() ?>
  <?php plxUtils::printSelect('selection', $status, '', false, '', 'id_selection') ?>
  <input class="button submit" type="submit" name="ok" value="<?php echo L_OK ?>" onclick="return confirmAction(this.form, 'id_selection', 'delete', 'id<?php echo (isset($_GET['mod']) && $_GET['mod']=='cmd'?'Command':'Product'); ?>[]', '<?php $plxPlugin->lang('L_CONFIRM_DELETE') ?>')" />
<?php if ($onglet!='commandes'){ ?>
  <input class="button update" type="submit" name="update" value="<?php $plxPlugin->lang('L_ADMIN_MODIFY') ?> <?php echo (isset($_GET['mod']) && $_GET['mod']=='cat'?$plxPlugin->getLang('L_GROUPS'):$plxPlugin->getLang('L_PRODUCTS')); ?>" />
<?php } ?>
 </div>
 <div id="tabContainer" data-storetabs="myShopAdmin-<?php echo plxUtils::title2filename($plxPlugin->plxMotor->racine) ?>">
<?php if($plxPlugin->aLangs){# Onglets MyMultiLingue ?>
  <div class="tabs col sml-12">
   <ul>
<?php echo $tabHeaders ?>
   </ul>
  </div>
<?php }#fi Onglets MyMultiLingue ?>
  <div class="tabscontent">
<!-- Content en multilingue -->
<?php
foreach($aLangs as $lang) {
 $lgf=($plxPlugin->aLangs)?$lang.'/':'';#folders
 $lgu=($plxPlugin->aLangs AND (isset($_SESSION['default_lang']) AND $lang!=$_SESSION['default_lang']))?$lang.'/':'';#url (view) : old idea ($plxPlugin->dLang != $lang)
 $lng=($plxPlugin->aLangs)?'_'.$lang:'';#post vars

 # Génération de notre tableau des catégories
 $aFilterCat['all'] = $plxPlugin->getLang('L_ALL_GROUPS');
 //~ $aFilterCat['home'] = L_HOMEPAGE;
 //~ $aFilterCat['000'] = L_UNCLASSIFIED;
 if(!isset($_GET['mod']))# Only in prods list mode
  $aFilterCat['000'] = $plxPlugin->getLang('L_PRODUCTS_HOME_PAGE');
 if($plxPlugin->aProds[$lang]) {
  foreach($plxPlugin->aProds[$lang] as $k=>$v) {
   if($v['pcat']==1){
    $aCat[$k] = plxUtils::strCheck($v['name']);
    $aFilterCat[$k] = plxUtils::strCheck($v['name']);
   }
  }
  if(isset($aCat)) $aAllCat[$plxPlugin->getLang('L_GROUPS')] = $aCat;#First idea to fix if nothing groups
 }
 $aAllCat[$plxPlugin->getLang('L_SPECIFIC_GROUPS_TABLE')]['000'] = $plxPlugin->getLang('L_PRODUCTS_HOME_PAGE');
 $aAllCat[$plxPlugin->getLang('L_SPECIFIC_GROUPS_TABLE')][''] = $plxPlugin->getLang('L_ALL_GROUPS_TABLE');
?>
   <div class="tabpage<?php echo ($lang==$plxPlugin->dLang?' active':''); ?>" id="tabpage<?php echo $lng ?>">
<?php
   if($onglet!='commandes'){
    $where = (isset($_GET['mod'])? '&amp;mod='.$_GET['mod']: '');
    $dLang = $plxPlugin->dLang;# shift 4 funk : nbElements()
    $plxPlugin->dLang = $lang;# prods & groups (cats) lists
?>
    <div class="in-action-bar sel-get <?php echo $actionBarClasses . ($dLang!=$lang?' hide':'')?>">
     <span>
      <?php echo ($plxPlugin->aLangs?$mmlFlagImgs[$lang]:'') ?>
      <a <?php echo ($_SESSION['sel_get' . $search_mode][$plxPlugin->plugName][$lang]=='all')?'class="selected" ':'' ?>href="?p=<?php echo $plxPlugin->plugName.$where ?>&amp;sel=all&amp;page=1"><?php echo L_ALL ?></a><?php echo '&nbsp;('.$plxPlugin->nbElements('all', $onglet).')' ?>
      <a <?php echo ($_SESSION['sel_get' . $search_mode][$plxPlugin->plugName][$lang]=='published')?'class="selected" ':'' ?>href="?p=<?php echo $plxPlugin->plugName.$where ?>&amp;sel=published&amp;page=1"><?php echo L_ALL_PUBLISHED ?></a><?php echo '&nbsp;('.$plxPlugin->nbElements('published', $onglet).')' ?>
      <a <?php echo ($_SESSION['sel_get' . $search_mode][$plxPlugin->plugName][$lang]=='draft')?'class="selected" ':'' ?>href="?p=<?php echo $plxPlugin->plugName.$where ?>&amp;sel=draft&amp;page=1"><?php echo L_ALL_DRAFTS ?></a><?php echo '&nbsp;('.$plxPlugin->nbElements('draft', $onglet).')' ?>
     </span>
    </div>
<?php
  $plxPlugin->dLang = $dLang;# Restore : shift 4 funk : nbElements()
 }#Fi onglet!=commandes

////////////////SEARCH//////////////////////////////////////////////////////////////////////////////////
if(isset($search_mode)){#!='cmd'
   $flag = ($plxPlugin->aLangs?' '.$mmlFlagImgs[$lang]:'');
?>
    <div>
     <div class="col sml-6">
      <?php plxUtils::printSelect('sel_cat' . $lng, $aFilterCat, $_SESSION['sel_cat' . $search_mode][$plxPlugin->plugName][$lang]) ?>
      <button class="<?php echo $_SESSION['sel_cat' . $search_mode][$plxPlugin->plugName][$lang]!='all'?' select':'' ?>" type="submit" value="<?php echo L_ARTICLES_FILTER_BUTTON ?>"><?php echo L_ARTICLES_FILTER_BUTTON.$flag ?></button>
     </div>
     <div class="col sml-6 text-right">
      <input id="index-search<?php echo $lng ?>" placeholder="<?php echo $plxPlugin->lang('L_SEARCH_PLACEHOLDER') ?>" type="text" name="artTitle<?php echo $lng ?>" value="<?php echo plxUtils::strCheck($_GET['artTitle' . $lng]) ?>" />
      <button class="<?php echo (!empty($_GET['artTitle' . $lng])?'select':'') ?>" type="submit" value="<?php echo L_SEARCH ?>"><?php echo L_SEARCH.$flag?></button>
     </div>
    </div>
<?php
}
////////////////SEARCH//////////////////////////////////////////////////////////////////////////////////
?>
   <div class="scrollable-table col sml-12">
    <table id="myShop-table<?php echo $lng ?>" class="table full-width AdminGroupProdLists liste<?php echo (isset($_GET['mod']) && $_GET['mod']=='cat'?"Categories":"Produits");?>Admin display responsive no-wrap" width="100%">
     <?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminTable'));# Hook Plugins plxMyShopAdminTable ?>
     <thead>
      <tr>
<?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminTheadTrBegin'));# Hook Plugins plxMyShopAdminTheadTrBegin ?>
<?php if ($onglet != 'commandes'): ?>
       <th><input type="checkbox" onclick="checkAll(this.form, 'idProduct[]')" /></th>
       <th title="<?php $plxPlugin->lang('L_PRODUCTS_ID') ?>">#<?php echo $plxPlugin->aLangs?'&nbsp;'.$mmlFlagImgs[$lang]:'' ?></th>
       <th class="text-center">&nbsp;⛶&nbsp;</th>
       <th><?php $plxPlugin->lang('L_PRODUCTS_TITLE') ?></th>
       <th><?php $plxPlugin->lang('L_PRODUCTS_URL') ?></th>
<?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminTheadTr'));# Hook Plugins plxMyShopAdminTheadTr ?>
       <th><?php $plxPlugin->lang('L_PRODUIT_ACTIF') ?></th>
       <th><?php $plxPlugin->lang('L_PRODUCTS_ORDER') ?></th>
<?php if ($onglet == 'categories'){ ?>
       <th><?php $plxPlugin->lang('L_PRODUCTS_MENU')?></th>
<?php } else { ?>
       <th><?php $plxPlugin->lang('L_LIST_GROUPS') ?></th>
       <th class="nombre"><?php $plxPlugin->lang('L_PRODUCTS_STOCK')?></th>
       <th class="nombre"><?php $plxPlugin->lang('L_PRODUCTS_WEIGHT')?></th>
       <th class="nombre"><?php $plxPlugin->lang('L_PRODUCTS_PRICE')?></th>
<?php } ?>
       <th><?php $plxPlugin->lang('L_PRODUCTS_ACTION') ?></th>
<?php else:#commandes ?>
       <th data-sorting="false" data-search="false"><input type="checkbox" onclick="checkAll(this.form, 'idCommand[]')" /></th>
       <th><?php $plxPlugin->lang('L_DATE') ?></th>
       <th><?php $plxPlugin->lang('L_PAIEMENT') ?></th>
       <th><?php $plxPlugin->lang('L_STATUS') ?></th>
       <th class="nombre"><?php $plxPlugin->lang('L_MONTANT') ?></th>
       <th><?php $plxPlugin->lang('L_ACTIONS') ?></th>
<?php endif; ?>
<?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminTheadTrEnd'));# Hook Plugins plxMyShopAdminTheadTrEnd ?>
      </tr>
     </thead>
     <tbody>
<?php
 $num = 0;# Initialisation de l'ordre
 if($plxPlugin->aProds[$lang]){# Si on a des produits
  foreach($plxPlugin->aProds[$lang] as $k=>$v){# Pour chaque produit
   if((isset($_GET['mod']) && $_GET['mod']=='cat' && $v['pcat']!=1)||(isset($_GET['mod']) && $_GET['mod']=='cmd'))continue;
   if(!isset($_GET['mod']) && $v['pcat']==1)continue;
   if(!$v['visible']) continue;
   $url=$v['url'];

   $ordre = ++$num;
   $selected = $v['pcat']==1 ? ' checked="checked"' : '';
   $valued = $v['pcat']==1 ? '1' : '0';
   $noaddcartImg = ($v['pcat']!=1 ? '<img class="noaddcartImg" src="'.PLX_PLUGINS.$plxPlugin->plugName.'/images/'.(empty($v['noaddcart'])?'full':'empty').'.png" />' : '');
   $noaddcartTit = (empty($v['noaddcart'])?'':PHP_EOL.htmlspecialchars($plxPlugin->getLang('L_PRODUCTS_BASKET_BUTTON')));
   $image = $v["image"];

   #Product group(s) @categorie
   if (empty($valued)){#$v['pcat']==0
    $aCats = array();
    $catIds = explode(',', $v['group']);
    if(sizeof($catIds)>0) {
     foreach($catIds as $catId) {
      $selected = ($catId==$_SESSION['sel_cat' . $search_mode][$plxPlugin->plugName][$plxPlugin->dLang] ? ' selected="selected"' : '');
      //~ if($catId=='draft') $draft = ' - <strong>'.L_DRAFT.'</strong>';
      //~ elseif($catId=='home') $aCats['home'] = '<option value="home"'.$selected.'>'.L_HOMEPAGE.'</option>';
      //~ elseif($catId=='000') $aCats['000'] = '<option value="000"'.$selected.'>'.L_UNCLASSIFIED.'</option>';
      if($catId=='000') $aCats['000'] = '<option value="000"'.$selected.'>'.$plxPlugin->getLang('L_PRODUCTS_HOME_PAGE').'</option>';
      elseif(isset($plxPlugin->aProds[$lang][$catId])) $aCats[$catId] = '<option value="'.$catId.'"'.$selected.'>'.plxUtils::strCheck($plxPlugin->aProds[$lang][$catId]['name']).'</option>';
     }
    }
   }

?>
     <tr class="line-<?php echo ($num%2)?>">
<?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminTableTbodyTrBegin'));# Hook Plugins plxMyShopAdminTbodyTrBegin ?>
      <td><input type="checkbox" name="idProduct[]" value="<?php echo $k?>" /><input type="hidden" name="productNum[]" value="<?php echo $k?>" /></td>
      <td><a href="plugin.php?p=<?php echo $plxPlugin->plugName?>&amp;prod=<?php echo $k?>" title="<?php echo $plxPlugin->getLang('L_PRODUCTS_SRC_TITLE').$noaddcartTit?>"><?php echo $k.$noaddcartImg?></a>
       <input type="hidden" name="<?php echo $k?>_pcat<?php echo $lng?>" value="<?php echo $valued?>"<?php echo $selected?> onclick="checkBox(this);" />
      </td>
<?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminTableTbodyTr'));# Hook Plugins plxMyShopAdminTbodyTr ?>
      <td class="text-center"><a href="plugin.php?p=<?php echo $plxPlugin->plugName.'&amp;prod='.$k?>" title="<?php $plxPlugin->lang('L_PRODUCTS_SRC_TITLE').$noaddcartTit?>"><img class="product_image" src="<?php echo ($image!=''?PLX_ROOT.$plxPlugin->cheminImages.$image:$plxPlugin::$imageNo)?>"/></a></td>
      <td><?php plxUtils::printInput($k.'_name'.$lng, plxUtils::strCheck($v['name']), 'text', '20-255');?></td>
      <td><?php plxUtils::printInput($k.'_url'.$lng, $v['url'], 'text', '12-255');?></td>
      <td><?php plxUtils::printSelect($k.'_active'.$lng, array('0'=>L_NO,'1'=>L_YES), $v['active']);?></td>
      <td><?php plxUtils::printInput($k.'_ordre'.$lng, $ordre, 'text', '2-3');?></td>
<?php if ($v['pcat']==1){ ?>
      <td><?php plxUtils::printSelect($k.'_menu'.$lng, array('oui'=>L_DISPLAY,'non'=>L_HIDE), $v['menu']);?></td>
<?php } else {?>
      <td><?php
      if(sizeof($aCats)>1) {
       echo '<select name="sel_cat2" class="ddcat" onchange="this.form.sel_cat.value=this.value;this.form.submit()">';
       echo implode('', $aCats);
       echo '</select>';
      }
      else echo strip_tags(implode('', $aCats));?></td>

      <td class="nombre"><?php echo ($v['iteminstock'] > 0)? $plxPlugin->smartStock($v['iteminstock']) : '<i title="'.$plxPlugin->getLang('L_PRODUCTS_STOCK_NO').'">'.L_NO.'</i>'?></td>
      <td class="nombre"><?php echo ($v['poidg'] > 0)? $plxPlugin->smartWeight($v['poidg']) : ''?></td>

      <td class="nombre"><?php echo ($v['pricettc'] > 0)? $plxPlugin->pos_devise($v['pricettc'], $lang): '';?></td>
<?php } ?>
<?php if(!plxUtils::checkSite($v['url'])){#a ?>
      <td><a href="plugin.php?p=<?php echo $plxPlugin->plugName?>&amp;prod=<?php echo $k?>" title="<?php echo $plxPlugin->getLang('L_PRODUCTS_SRC_TITLE').$noaddcartTit?>"><?php echo $plxPlugin->getLang('L_PRODUCTS_SRC')?></a><?php
       if(@$v['active']){?>&nbsp;-&nbsp;<a href="<?php echo $plxAdmin->urlRewrite('?' . $lgu . 'product'.intval($k).'/'.$url)?>" title="<?php echo sprintf($plxPlugin->getLang('L_VIEW_ONLINE'), plxUtils::strCheck($v['name']))?>"><?php echo L_VIEW?></a><?php } ?>
      </td>
<?php } else{ ?>
      </td>
      <td>c <a href="<?php echo $plxAdmin->urlRewrite('?' . $lgu .'product'. intval($k).'/'.$url)?>" title="<?php echo plxUtils::strCheck($v['name'])?>"><?php echo L_VIEW?></a></td><!-- C KOI -->
<?php } ?>
<?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminTableTbodyTrEnd'));# Hook Plugins plxMyShopAdminTbodyTrEnd ?>
     </tr>
<?php
  }# hcaerof
  $a = array_keys($plxPlugin->aProds[$lang]);# On récupère le dernier identifiant
  rsort($a);
 } else {#Aucun produits
  $a['0'] = 0;
 }

 $new_id = $a['0']+1;
 $new_productid = str_pad($new_id, 3, '0', STR_PAD_LEFT);
 #new Prod/Cat only appear in selected lang in plxMyMultilingue flags
 if ($onglet!='commandes' && (!$plxPlugin->aLangs OR $lang==$_SESSION['default_lang'])){
?>
    <tr id="newitem-<?php echo $new_id ?>" class="new">
<?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminTbodyTrNewBegin'));# Hook Plugins plxMyShopAdminTbodyTrNewBegin ?>
     <td colspan="3" style="text-align:left"><a id="addmore" href="javascript:;">➕</a> <?php
      plxUtils::printInput('productNum[]', $new_productid, 'hidden', '1-4');
      $isCatMode = ($onglet == 'categories');#isset($_GET['mod']) && $_GET['mod']=='cat'?'1':'0';
      plxUtils::printInput($new_productid.'_pcat'.$lng, $isCatMode, 'hidden', '1-1');
      $plxPlugin->lang('L_NEW_'.($isCatMode?'CATEGORY':'PRODUCT'));
      ?></td>
     <td><?php
      plxUtils::printInput($new_productid.'_name'.$lng, '', 'text', '20-255', false, 'newname');
      plxUtils::printInput($new_productid.'_template'.$lng, $plxPlugin->getParam('template'), 'hidden');
      ?></td>
     <td><?php plxUtils::printInput($new_productid.'_url'.$lng, '', 'text', '12-255');?></td>
<?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminTbodyTrNew'));# Hook Plugins plxMyShopAdminTbodyTrNew ?>
     <td><?php plxUtils::printSelect($new_productid.'_active'.$lng, array('0'=>L_NO,'1'=>L_YES))?></td>
     <td><?php plxUtils::printInput($new_productid.'_ordre'.$lng, ++$num, 'text', '2-3');?></td>
<?php if ($isCatMode){ ?>
     <td><?php plxUtils::printSelect($new_productid.'_menu'.$lng, array('oui'=>L_DISPLAY,'non'=>L_HIDE));?></td>
     <td>&nbsp;</td>
<?php } else {?>
     <td colspan="3">&nbsp;</td>
<?php } ?>
<?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminTbodyTrNewEnd'));# Hook Plugins plxMyShopAdminTbodyTrNewEnd ?>
    </tr>
<script type="text/javascript">
const idActBase = '_active<?php echo $lng ?>';
var idn = <?php echo $new_id ?>;
var newname = document.getElementById('id_<?php echo $new_productid.'_name'.$lng ?>');
var original = document.getElementById('newitem-<?php echo $new_id ?>');
function duplicateNew() {
 var clone = original.cloneNode(true); // "deep" clone
 var bd = (idn + '').padStart(3, '0');
 clone.id = 'newitem-' + ++idn; // there can only be one element with an ID
 var id = (idn + '').padStart(3, '0');
 var isActive = document.getElementById('id_'+bd+idActBase).options[1].selected;
 //clear all .newname.onfocus
 var newnames = document.getElementsByClassName('newname');
 for(var i=0;i<newnames.length;i++)
  newnames[i].removeEventListener('focus', goduplicate, true);//.onfocus=function(){};
 // Increase all cloned input names & id's
 for(var t=0;t<clone.children.length;t++){
  for(var f=0;f<clone.children[t].children.length;f++){
   if(clone.children[t].children[f].value == bd)
    clone.children[t].children[f].value = id;//productNum[]
   if(clone.children[t].children[f].name){
    clone.children[t].children[f].id = clone.children[t].children[f].id.replace(bd,id);
    clone.children[t].children[f].name = clone.children[t].children[f].name.replace(bd,id);
    if(clone.children[t].children[f].name.search('name') > 0)//e.type == 'focus' &&
     clone.children[t].children[f].addEventListener('focus', goduplicate, true);
    if(clone.children[t].children[f].name.search('ordre') > 0)//increase
     clone.children[t].children[f].value = ++clone.children[t].children[f].value;
    if(clone.children[t].children[f].name.search('active') > 0){//Fix is active
     if(isActive){
      clone.children[t].children[f].options[0].selected = false;
      clone.children[t].children[f].options[1].selected = 'selected';
     }
    }
   }else{
    clone.children[t].children[f].id = 'addmore' + idn;
    clone.children[t].children[f].addEventListener('click', duplicateNew, true);//.onclick = duplicateNew;
   }
  }
 }
 original.parentNode.appendChild(clone);
 original = document.getElementById(clone.id);//important to adjust next id's & names
 return false;
}
function goduplicate(e){
 e.target.removeEventListener('focus', goduplicate, true);//.onfocus=function(){};
 duplicateNew();//Only one time
}
newname.addEventListener('focus', goduplicate, true);//.onfocus = goduplicate; + remove listener
document.getElementById('addmore').addEventListener('click', duplicateNew, true);//.onclick = duplicateNew;
</script>
<?php
 }
 if($onglet=='commandes'){#if(isset($_GET['mod']) && $_GET['mod']=='cmd')
  # multilingue or not
  $filescommande = array();
  $dh = opendir($dirComs.$lgf);
  while (false !== ($filename = readdir($dh))){
   if (is_file($dirComs.$lgf.$filename) && $filename!='.' && $filename!='..' && $filename!='index.html'){
    $filescommande[] = $filename;
   }
  }
  unset($dh);
  rsort($filescommande);
  foreach($filescommande as $key => $val){//fix deprecated each() in php 7.2 ::: while (list ($key, $val) = each ($filescommande) ){
   $namearray=explode('_',rtrim($val, '.html'));
   $date=implode('/',explode('-',$namearray[0]));
   $namearray[5] = isset($namearray[5])? $namearray[5]: 'A';#Awaiting, Paid or Cancelled v1.0.0 (compatibility with ancients file name)
   $namearray[5] = isset($statesCom[$namearray[5]])? $statesCom[$namearray[5]]: $plxPlugin->getLang('L_UNKNOWN');

   $srcLink = ' - <a title="'.$plxPlugin->getLang('L_DOWNLOAD') . ' ' . $namearray[2] . ' (html)" target="_blank" style="color:unset;"
href="'.$url.'&amp;html='.plxEncrypt::encryptId($dirComs.$lgf.$val).'"><sup><sub><i>HTML</i></sub></sup></a>';

?>
    <tr>
<?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminTbodyTrCmdBegin'));# Hook Plugins plxMyShopAdminTbodyTrCmdBegin ?>
     <td class="checkbox"><input type="checkbox" name="idCommand[]" value="<?php echo $lgf.$val?>" /></td>
     <td id="dateTime"><a
       target="_blank"
       href="<?php echo $dirComs.$lgf.$val?>"
       data-featherlight-target="<?php echo $dirComs.$lgf.$val?>"
       data-featherlight="iframe"
       data-featherlight-iframe-allowfullscreen="true"
       title="<?php echo plxUtils::strCheck($plxPlugin->getLang('L_ADMIN_ORDER_VIEW'))?>"
      ><?php echo $date?> - <?php echo str_replace('-',':',$namearray[1])?></a><?php echo $srcLink?></td>
     <td><?php echo $namearray[2]?></td>
     <td><?php echo $namearray[5]?></td>
     <td class="nombre"><?php echo $plxPlugin->pos_devise(floatval($namearray[3])+floatval($namearray[4]), $lang)?></td>
     <td><a
        onclick="return (confirm('<?php echo $plxPlugin->getLang('L_ADMIN_CONFIRM_DELETE')?>'))? true: false;"
        href="plugin.php?p=<?php echo $plxPlugin->plugName?>&amp;mod=cmd&amp;kill=<?php echo $lgf.$val?>"><?php echo $plxPlugin->getLang('L_ADMIN_ORDER_DELETE')?></a>
     - <a
        target="_blank" href="<?php echo $dirComs.$lgf.$val?>"
        data-featherlight-target="<?php echo $dirComs.$lgf.$val?>"
        data-featherlight="iframe"
        data-featherlight-iframe-allowfullscreen="true"><?php echo $plxPlugin->getLang('L_ADMIN_ORDER_VIEW')?></a></td>
<?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminTbodyTrCmdEnd'));# Hook Plugins plxMyShopAdminTbodyTrCmdEnd ?>
    </tr>
<?php
  }
 }//fi command
?>
     <?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminTableEnd'));# Hook Plugins plxMyShopAdminTableEnd ?>
    </tbody>
   </table>
   <?php eval($plxAdmin->plxPlugins->callHook($plxPlugin->plugName.'AdminTabPageEnd'));# Hook Plugins plxMyShopAdminTabPageEnd ?>
   </div><!-- fi scrollable-table  -->
  </div><!-- fi tabpage -->
<?php
}#fi foreach aLangs
?>
<!-- Fin du content en multilingue -->
  </div><!-- tabscontent scrolable table -->
 </div><!-- fi tabContainer -->
<?php if($plxPlugin->aLangs){#mml ?>
 <script type="text/javascript" src="<?php echo PLX_PLUGINS.$plxPlugin->plugName.'/js/tabs.js?v='.$plxPlugin::V ?>"></script>
<?php
}#fi multilingue loop

if($onglet=='commandes'){?>
 <input type="hidden" name="clean" id="clean" value="all" />
<script type="text/javascript" style="display:none">
function cleanOrZip(file){
 if(file)document.getElementById('clean').value=file;
 document.getElementById('form_<?php echo $plxPlugin->plugName ?>').submit();
}
</script>
<script type="text/javascript" style="display:none">
 <?php echo $delzip ?>
</script>
<?php
}
?>
</form>
<script style="display:none">
//On affiche les .hide et modifie la balise title
setTimeout(function(){
 let a = document.querySelectorAll('.hide');
 for (i=0; i<a.length; i++)
  a[i].classList.remove('hide');
}, 222);
if(document.title){
 let t = document.getElementById('page-title');
 document.title = ' <?php echo $plxPlugin->plugName ?> - ' + (t.textContent?t.textContent:t.innerText) + ' - ' + document.title;
}
</script>
<?php
}# fi esle : prods, cats & coms list
if($onglet=='commandes'){
 include('lib/datatables.js.php');
}
#$plxPlugin->tips();# Contribute