# plxMyShop - PluXml plugin
## Greffe une boutique à votre PluXml.
+ Compatible avec plxMyMultiLingue et traduit en 11 langues
+ Gère Groupes, produits ainsi que des commandes reçu par mél.
+ Les plugins peuvent utiliser les "Hook" disponibles afin d'augmenter ses capacités...


#### Projet original par David Lhoumaud https://www.ckdevelop.org

#### Repris par Thomas Ingles http://sudwebdesign.free.fr

[Description en Français](lang/fr/description.md)

[Description en Occitan](develop/lang/oc/description.md)

* [Pour éviter ça](https://forum.pluxml.org/discussion/comment/60812/#Comment_60812) le nom du dossier une fois désarchivé doit être **plxMyShop** (respect de la casse des lettres et sans rien après, ni master ou 1.0.1)
