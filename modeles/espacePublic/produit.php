<?php if (!defined('PLX_ROOT')) exit;
/*
Si vous réutilisez ce fichier dans votre thème, nous vous conseillons de noter la version actuelle de plxMyShop
version :
*/
$plxPlugin = $d['plxPlugin'];
$produit = $plxPlugin->aProds[$plxPlugin->dLang][$plxPlugin->productNumber()];
$baseHook = ucfirst(rtrim(basename(__FILE__),'.php'));
# Hook Plugins plxMyShopProduit
if (eval($this->plxMotor->plxPlugins->callHook($plxPlugin->plugName . $baseHook))) return;
#fil d'ariane : breadcrumb
$i=0;
foreach($plxPlugin->productGroupTitle() as $key => $value) {
 echo ($i>0?', ':'').'<a href="'.$plxPlugin->productRUrl($key).'">'.$value.'</a>';
 $i=1;
}
echo '&nbsp;&rsaquo;&nbsp;'; $plxPlugin->productTitle();
?>
<section class="product_content">
 <header>
<?php $plxPlugin->plxShowLienPanierTop() ?>
  <div class="image_product">
<?php
  $plxPlugin->productImage();
  $plxPlugin->donneesModeles['k'] = $plxPlugin->productNumber();
  $plxPlugin->modele('espacePublic/boucle/boutonPanier');
?>
  </div>
<?php if ($produit['pricettc'] > 0) { ?>
   <span class="product_pricettc"><?php echo $plxPlugin->pos_devise($plxPlugin->productPriceTTC());?></span>
<?php } ?>
<?php if ($produit['poidg'] > 0) { ?>
   <span class="product_poidg"><?php echo $plxPlugin->productPoidG();?>&nbsp;kg</span>
<?php } ?>
 </header>
 <article>
  <?php $plxPlugin->plxShowProductChapo(); ?>
  <?php $plxPlugin->plxShowProductContent(); ?>
 </article>
</section>