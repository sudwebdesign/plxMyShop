<?php if (!defined('PLX_ROOT')) exit;
/*
Si vous réutilisez ce fichier dans votre thème, nous vous conseillons de noter la version actuelle de plxMyShop
version :
*/
$plxPlugin = $d['plxPlugin'];
$baseHook = ucfirst(rtrim(basename(__FILE__),'.php'));
# Hook Plugins plxMyShopProducts
if (eval($this->plxMotor->plxPlugins->callHook($plxPlugin->plugName . $baseHook))) return;
?>
<section class="list_products stars">
 <header>
<?php $plxPlugin->plxShowLienPanierTop() ?>
  <div class="cat_image">

<?php
   $imgUrl = PLX_ROOT.$plxPlugin->getParam('image_home'.'_'.$this->dLang);
   $imgUrl = is_file($imgUrl)?$imgUrl:$plxPlugin::$imageNo;
?>
   <img class="product_image_cat" src="<?=$imgUrl?>" alt="" />
<?php #$this->productImage('', 'product_image group home') #todo ?>
  </div>
 </header>
 <article>
  <?php $plxPlugin->plxShowProductChapo(); ?>
  <?php $plxPlugin->plxShowProductContent(); ?>
 </article>
 <?php #product lists
  $have = 0;
  if (isset($plxPlugin->aProds[$plxPlugin->dLang])) {
   foreach($plxPlugin->aProds[$plxPlugin->dLang] as $k => $v) {
    if ( preg_match('~000~', $v['group'])
     && $v['pcat']!=1
     && $v['active']==1
     #&& $v['readable']==1
    ) {
     $have = $k;
     $plxPlugin->donneesModeles['k'] = $k;
     $plxPlugin->modele('espacePublic/boucle/produitRubrique');
    }
   }
  }
  if(!$have) $plxPlugin->lang('L_PRODUCT_NO');
 ?>
</section>