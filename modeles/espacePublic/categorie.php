<?php if (!defined('PLX_ROOT')) exit;
/*
Si vous réutilisez ce fichier dans votre thème, nous vous conseillons de noter la version actuelle de plxMyShop
version :
*/
$plxPlugin = $d['plxPlugin'];
$baseHook = ucfirst(rtrim(basename(__FILE__),'.php'));
# Hook Plugins plxMyShopCategorie
if (eval($this->plxMotor->plxPlugins->callHook($plxPlugin->plugName . $baseHook))) return;
?>
<section class="list_products">
 <header>
<?php $plxPlugin->plxShowLienPanierTop() ?>
  <div class="group_image">
   <?php $plxPlugin->productImage('', 'product_image_group');?>
  </div>
 </header>
 <article>
  <?php $plxPlugin->plxShowProductChapo(); ?>
  <?php $plxPlugin->plxShowProductContent(); ?>
 </article>
 <?php
  $have = 0;
  if (isset($plxPlugin->aProds[$plxPlugin->default_lang])) {
   foreach($plxPlugin->aProds[$plxPlugin->default_lang] as $k => $v) {
    if ( preg_match('~'.$plxPlugin->productNumber().'~', $v['group'])
     && $v['pcat']!=1
     && $v['active']==1
     #&& $v['readable']==1
    ) {
     $have = $k;
     $plxPlugin->donneesModeles['k'] = $k;
     $plxPlugin->modele('espacePublic/boucle/produitRubrique');
    }
   }
  }
  if(!$have) $plxPlugin->lang('L_PRODUCT_NO');
 ?>
</section>