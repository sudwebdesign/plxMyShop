<?php if (!defined('PLX_ROOT')) exit;
/*
Si vous réutilisez ce fichier dans votre thème, nous vous conseillons de noter la version actuelle de plxMyShop
version :
*/
if(isset($this->aProds[$this->default_lang][$d['k']])){#si le produit existe
 $v = $this->aProds[$this->default_lang][$d['k']];
 $save_id = $this->idProduit;#
 $this->idProduit = $d['k'];# Pour productImage(), productUrl(), plxShowProductChapo #tep
?>
<div id="prod<?php echo intval($d['k']); ?>" class="lproduct_content" align="center" title="<?php $this->productTitle(); ?>">
 <header>
  <h3 class="product_name"><a href="<?php $this->productUrl(); ?>" ><?php echo plxUtils::strCut($v['name']); ?></a></h3>
  <a class="toolTipTop" href="<?php $this->productUrl()?>"><?php
   $this->productImage('', 'product_image');
   ?><span class="infobTipTop">
     <p><strong><?php echo $v['name']; ?></strong></p>
     <?php $this->plxShowProductChapo(); ?></span></a>
   <span class="lproduct_pricettc"><?php
    echo floatval($v['pricettc']) > 0? $this->pos_devise($v['pricettc']):'&nbsp;';
   ?></span>
   <span class="lproduct_poidg"><?php
    echo (floatval($v['poidg']) > 0 && $this->getParam('shipping_colissimo'))? '&nbsp;'.$this->lang('L_FOR').' '.$v['poidg'].'&nbsp;kg': '&nbsp;';
   ?></span>
 </header>
 <?php $this->modele('espacePublic/boucle/boutonPanier'); ?>
</div>
<?php
 $this->idProduit = $save_id;# Sait-on jamais
}