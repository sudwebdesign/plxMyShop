<?php if (!defined('PLX_ROOT')) exit;
/*
Si vous réutilisez ce fichier dans votre thème, nous vous conseillons de noter la version actuelle de plxMyShop
version :
*/
$plxPlugin = $d['plxPlugin'];
$minPnr = 1;
$maxPnr = $plxPlugin->aProds[$plxPlugin->dLang][$d['k']]['iteminstock'] != '' ? 'max="'.$plxPlugin->aProds[$plxPlugin->dLang][$d['k']]['iteminstock'].'" ' : '';
$prodsPnr = 1;
$txtPnrBtn = htmlspecialchars($plxPlugin->getLang('L_PUBLIC_ADD_BASKET'));
$classPnrBtn = 'blue';
$shortcode = $plxPlugin->shortcodeactif;#0 if not shortcode, or + if is it. #fix btn add 2 cart duplicate id's
$baseHook = ucfirst(rtrim(basename(__FILE__),'.php'));

# Hook Plugins plxMyShopBoutonPanier #stopable
if (eval($this->plxMotor->plxPlugins->callHook($plxPlugin->plugName . $baseHook))) return;

if (isset($_SESSION[$plxPlugin->plugName]['prods'][$plxPlugin->dLang][$d['k']])){
 if ($_SESSION[$plxPlugin->plugName]['prods'][$plxPlugin->dLang][$d['k']]<1) {
  if(is_array($_SESSION[$plxPlugin->plugName]['prods'][$plxPlugin->dLang])){
   $_SESSION[$plxPlugin->plugName]['ncart'][$plxPlugin->dLang] -= $_SESSION[$plxPlugin->plugName]['prods'][$plxPlugin->dLang][$d['k']];
   unset($_SESSION[$plxPlugin->plugName]['prods'][$plxPlugin->dLang][$d['k']]);
  }else{# is_not_array #unset
    unset($_SESSION[$plxPlugin->plugName]['ncart'],$_SESSION[$plxPlugin->plugName]['prods']);#remove old old session
  }
 }else{
  $minPnr = 0;
  $prodsPnr = $_SESSION[$plxPlugin->plugName]['prods'][$plxPlugin->dLang][$d['k']];
  $txtPnrBtn = htmlspecialchars($plxPlugin->getLang('L_PUBLIC_DEL_BASKET'));
  $classPnrBtn = 'red';
 }
}
$nbProdtype = 'number';#(count($d['pileModeles']) === 1)?'hidden':'number'; //dansShortcode = hidden ????
$prod = $plxPlugin->aProds[$plxPlugin->dLang][$d['k']];

# Hook Plugins plxMyShopBoutonPanierTop
eval($this->plxMotor->plxPlugins->callHook($plxPlugin->plugName . $baseHook . 'Top'));

if(!$prod['pcat']){# if categorie no
 if(empty($prod['noaddcart'])){#Idée param (activer les produits gratuits (prix == 0)
?>
<form action="#prod<?php echo intval($d['k']); ?>" method="POST" id="FormAddProd<?php echo $d['k'].$shortcode; ?>" class="formulaireAjoutProduit" onsubmit="chngNbProd('<?php echo $d['k'].$shortcode; ?>',true);">
 <?php eval($this->plxMotor->plxPlugins->callHook($plxPlugin->plugName . $baseHook . 'Form'));# Hook Plugins plxMyShopBoutonPanierForm ?>
 <input type="hidden" name="idP" value="<?php echo htmlspecialchars($d['k']);?>">
 <input type="<?php echo $nbProdtype; ?>" name="nb" value="<?php echo $prodsPnr; ?>" min="<?php echo $minPnr;?>" <?php echo $maxPnr; ?>id="nbProd<?php echo $d['k'].$shortcode; ?>" onchange="chngNbProd('<?php echo $d['k'].$shortcode; ?>',false);" data-o="<?php echo $prodsPnr; ?>" />
 <br /><input class="<?php echo $classPnrBtn; ?>" type="submit" id="addProd<?php echo $d['k'].$shortcode; ?>" name="ajouterProduit" value="<?php echo $txtPnrBtn; ?>" />
</form>
<?php } else { ?>
<div class="lnotice_noaddcart"><?php echo (empty($prod['notice_noaddcart'])? $plxPlugin->getLang('L_NOTICE_NOADDCART'): $plxPlugin->aProds[$plxPlugin->dLang][$d['k']]['notice_noaddcart']);?></div>
<?php
 }
}