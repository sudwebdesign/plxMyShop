if (window.localStorage){
 function lsTest(){
  var test = 'test';
  try {
   localStorage.setItem(test, test);
   localStorage.removeItem(test);
   return true;
  } catch(e) {
   console.log(e + ' (error localStorage!)');
   return false;
  }
 }

 if(ShowMyForm && lsTest() === true){
  const bouton_effacer = document.getElementById('bouton_effacer');
  const bouton_sauvegarder = document.getElementById('bouton_sauvegarder');
  const alerte_sauvegarder = document.getElementById('alerte_sauvegarder');
  const locale_sauvegarde = 'Shop_Deliver_Adress';
  function stock(){
   bouton_effacer.style.display = '';
   bouton_sauvegarder.style.display = 'none';
   var temp = {
    firstname:firstnameCart.value,
    lastname:lastnameCart.value,
    email:emailCart.value,
    tel:telCart.value,
    adress:adressCart.value,
    postcode:postcodeCart.value,
    city:cityCart.value,
    country:countryCart.value,
   };
   localStorage.setItem(locale_sauvegarde, JSON.stringify(temp));
   alerte_sauvegarder.innerHTML = l_address_saved;
   alerte_sauvegarder.style.display = 'block';
   setTimeout(function(){
   alerte_sauvegarder.style.display = 'none'; }, 3000);
  }
  function clear(){
   bouton_effacer.style.display = 'none';
   bouton_sauvegarder.style.display = '';
   localStorage.removeItem(locale_sauvegarde);
   alerte_sauvegarder.innerHTML = l_address_deleted;
   alerte_sauvegarder.style.display = 'block';
   setTimeout(function(){
   alerte_sauvegarder.style.display = 'none'; }, 3000);
  }
  function raz(){
   clear();
   firstnameCart.value='';
   lastnameCart.value='';
   mailCart.value='';
   telCart.value='';
   adressCart.value='';
   postcodeCart.value='';
   cityCart.value='';
   countryCart.value='';
  }
  function detail(event){
   if (event.target.id != 'id_deliverydate' && event.target.id != 'nomCadeau')//not #datepicker & #nomCadeau
    if (event.target.type == 'text' || event.target.type == 'email'){
     bouton_effacer.style.display = 'none';
     bouton_sauvegarder.style.display = '';
    }
  }
  var gm = JSON.parse(localStorage.getItem(locale_sauvegarde));
  if (gm != null){
   firstnameCart.value = gm['firstname'];
   lastnameCart.value = gm['lastname'];
   emailCart.value = gm['email'];
   telCart.value = gm['tel'];
   adressCart.value = gm['adress'];
   postcodeCart.value = gm['postcode'];
   cityCart.value = gm['city'];
   countryCart.value = gm['country'];
  }
  //var bouton_un = document.getElementById('bouton_sauvegarder');
  let input = document.createElement('input');
  input.setAttribute('name','SaveAdress');
  input.setAttribute('value',l_save_my_address);
  input.setAttribute('type','button');
  input.addEventListener('click',stock, false);
  bouton_sauvegarder.appendChild(input);
  //var bouton_deux = document.getElementById('bouton_effacer');
  input = document.createElement('input');
  input.setAttribute('name','ClearAdress');
  input.setAttribute('value',l_delete_my_address);
  input.setAttribute('type','button');
  input.addEventListener('click',clear, false);
  bouton_effacer.appendChild(input);

  const bouton_raz = document.getElementById('bouton_raz');
  input = document.createElement('input');
  input.setAttribute('name','RAZAdresse');
  input.setAttribute('value',l_reset_address);
  input.setAttribute('type','button');
  input.addEventListener('click',raz, false);
  bouton_raz.appendChild(input);

  const form_client = document.getElementById('formcart');
  form_client.addEventListener('change',detail, false);
  form_client.addEventListener('input',detail, false);
  form_client.addEventListener('blur',detail, false);

  if (gm != null)
   bouton_sauvegarder.style.display = 'none';
  else
   bouton_effacer.style.display = 'none';
 }
}