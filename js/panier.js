function onCheckClick(Id, callback) {
 var e = document.getElementById(Id);
 e.onclick = function (item) {
  return function () {
   callback(item);
  };
 }(e);
};
onCheckClick('choixCadeau', function (e){
 if(e.checked)
  document.getElementById('conteneurNomCadeau').style.display = 'initial';
 else
  document.getElementById('conteneurNomCadeau').style.display = '';
});
if(ShowMyForm){
 formCart=document.getElementById('formcart');
 formcart.style.display='initial';
 //Helpers : need in error & local storage
 firstnameCart=document.getElementById('firstname');//
 lastnameCart=document.getElementById('lastname');//
 emailCart=document.getElementById('email');//
 telCart=document.getElementById('tel');//
 adressCart=document.getElementById('adress');//
 postcodeCart=document.getElementById('postcode');//
 cityCart=document.getElementById('city');//
 countryCart=document.getElementById('country');//
 choixCadeau=document.getElementById('choixCadeau');
 nomCadeau=document.getElementById('nomCadeau');
 msgCart=document.getElementById('msgCart');//
 if(delivery){
  deliverydate=document.getElementById('id_deliverydate');//
  delivery_date=document.getElementById('id_delivery_date');//
  delivery_interval=document.getElementById('id_delivery_interval');//
 }
 recalculer = document.getElementById('recalculer')
 recalculer.style.display='none';;
 nbs = document.getElementsByClassName('nb');
 for(i=0; i < nbs.length; i++){
  nbs[i].addEventListener('change',showBtnCalcul, false);
  nbs[i].addEventListener('input',showBtnCalcul, false);
  nbs[i].addEventListener('blur',showBtnCalcul, false);
 }
 function showBtnCalcul(event){
  recalculer.style.display = '';
  for(i=0; i < nbs.length; i++){
   nbs[i].removeEventListener('change',showBtnCalcul, false);
   nbs[i].removeEventListener('input',showBtnCalcul, false);
   nbs[i].removeEventListener('blur',showBtnCalcul, false);
  }
 }
}