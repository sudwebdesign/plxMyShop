container = false;//global
storetabs = 'tab';//global
window.onload=function() {
 // get tab container
 container = document.getElementById('tabContainer');
 let store = container.getAttribute('data-storetabs');
 storetabs = store?store:storetabs;
 // set current tab
 let navitem = container.querySelector('.tabs ul li.active');
 //get which tab we are on
 let ident = navitem.id.split('_')[1];
 //local storage mover option
 let prevIdent = memoTab(1);

 if (prevIdent && (ident != prevIdent)){
  navitem.classList.remove('active');//remove active
  let page = document.getElementById('tabpage_' + ident)
  page.classList.remove('active');//remove active
  page.style.display = 'none';
  let memo = document.getElementById('tabpage_' + prevIdent)
  memo.classList.add('active');//and add
  memo.style.display = 'block';
  navitem = document.getElementById('tabHeader_' + prevIdent);//get memorized navitem
  navitem.classList.add('active');//add active
  ident = prevIdent;
 }
 //store which tab we are on
 navitem.parentNode.setAttribute('data-current',ident);
 //set current tab with class of activetabheader
 navitem.classList.add('tabActiveHeader');
 //hide two tab contents we don't need
 toggleTables('none');
 //this adds click event to tabs
 let tabs = container.querySelectorAll('.tabs ul li');
 for (var i = 0; i < tabs.length; i++) {
  tabs[i].onclick=displayPage;
 }
 if(btn = document.getElementById('toggleTablesBtn'))
  btn.style = '';//Show : remove display:none
}
// on click of one of tabs
function displayPage() {
 let current = this.parentNode.getAttribute('data-current');
 document.getElementById('tabHeader_' + current).classList.remove('tabActiveHeader');
 let pages = container.querySelectorAll('.tabpage');
 for (var i = 0; i < pages.length; i++) {
  pages[i].classList.remove('active');
  pages[i].style.display='none';
 }
 let ident = this.id.split('_')[1];
 //add class of activetabheader to new active tab and show contents
 this.classList.add('tabActiveHeader');
 this.parentNode.setAttribute('data-current',ident);
 memoTab(0,ident);//localstorage.setItem with lang
 let page = document.getElementById('tabpage_' + ident)
 page.style.display='block';
 page.classList.add('active');
 let togAll = document.getElementById('toggleTablesBtn');
 if(togAll){//restore (hide/show all btn) (in lists only)
  togAll.setAttribute('data-toggle','block');
  togAll.firstChild.textContent='⛖';
 }
}
function memoTab(g,l){
 try {
  if(g) return localStorage.getItem(storetabs);//get lang
  return localStorage.setItem(storetabs, l);//set lang
 } catch(e) {
  return false;
 }
}
function toggleTables(d){
 d = d?d:'none';
 let pages = container.querySelectorAll('.tabpage');
 for (let i = 0; i < pages.length; i++) {
  //~ if(pages[i].className.search('active')>-1){//have active
  if(pages[i].classList.contains('active')){
   pages[i].style.display='block';
  }else{
   pages[i].classList.remove('active');
   pages[i].style.display=d;
  }
 }
}